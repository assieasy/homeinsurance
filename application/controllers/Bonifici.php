<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bonifici extends MY_Controller {
	 /**
     * @var Titoli_model
     */ 
	protected $TITMOD;

	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		if(!$this->controllo_privacy()){
			header('Location: /privacy');
		}
		$this->load->model("Titoli_model");
		$this->TITMOD = new Titoli_model($this->assi);
	}
	public function index()
	{
		$this->bonifico_scadenze();
	}
	public function bonifico($id_titolo_selected=0)
	{
		$this->bonifico_scadenze($id_titolo_selected);
	}

	public function bonifico_scadenze($id_titolo_selected=0)
	{
		$strSCadenze = isset($_POST['scadenze_selezionate'])?$_POST['scadenze_selezionate']:"";
		$strSCadenze = str_replace(",",";",$strSCadenze);
		$scadenze_selezionate = array();
		if($strSCadenze!=""){
			$scadenze_selezionate = explode(";",$strSCadenze);
		}
		if($id_titolo_selected>0){
			if(!in_array($id_titolo_selected,$scadenze_selezionate)){
				$scadenze_selezionate[]=$id_titolo_selected;
			}
		}
		$tipo_elenco = $this->get_tipo_elenco();

		$page_config=array();
		$page_config['titolo']="scadenze";
		$page_data = array();

		$p=[];
		$p['scadenze_selezionate']=$scadenze_selezionate;
		$scadenze_con_bonifico = $this->find_scadenze_con_bonifico($p);
		
		$page_data['titoli']= $scadenze_con_bonifico;
		$page_data['bonifici']= $this->componi_bonifici($scadenze_con_bonifico);
	
		$this->show("user_bonifici",$page_config,$page_data);

	}

	protected function find_scadenze_con_bonifico($params=array())
	{
		$all_config_obj = $this->datiSito['all_config_obj'];
		$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
		$vedi_premio_auto_rin = isset($hiPar->vedi_premio_auto_rin)?$hiPar->vedi_premio_auto_rin:0;  
		$vedi_premio_auto_rin = ($vedi_premio_auto_rin==1);
	
		$scadenze_selezionate = isset($params['scadenze_selezionate'])?$params['scadenze_selezionate']:array();
		$p=array();
		foreach($params as $k=>$v){
			$p[$k] = $v;
		}
		$p['SHOW_CANALI_PAGAMENTO']=1;
		$elenco_tmp = $this->TITMOD->get_scadenze($p);
		$elenco = array();
		foreach ($elenco_tmp as $scad) {
			$scad->BONIFICO_ABILITATO=0;	
			foreach ($scad->canali_pagamento as $CANALE) {
				if($CANALE->COD_TIPO_CANALE=="BONIFICO"){
					if( $scad->IS_SCADENZA_ANNUALITA==1 
						&& $scad->TIPO_RAMO=="A" 
						&& !$vedi_premio_auto_rin){
						//SCADENZA ANNUALE AUTO DOVE NON MOSTRO IMPORTI
						$scad->BONIFICO_ABILITATO=0;
					}
					else{
						$scad->BONIFICO_ABILITATO=1;
					}
				}
			}
			$elenco[]=$scad;
		}
		
		$scadenze_con_bonifico = array();
		foreach ($elenco as $scad) {
			if(in_array($scad->ID_TITOLO, $scadenze_selezionate)){
				$scad->SELECTED=1;
			}
			else{
				$scad->SELECTED=0;
			}
			if($scad->BONIFICO_ABILITATO==1){
				$scadenze_con_bonifico[]=$scad;
			}
		}
		
		return $scadenze_con_bonifico;
	}	
	
	protected function componi_bonifici($scadenze)
	{
		
		$OUT_BONIFICI = array();
		foreach ($scadenze as $scad) {
			if($scad->SELECTED){
				//CERCO I CANALI POSSIBILI PER QUESTA SCADENZA
				$canali = isset($scad->canali_pagamento)?$scad->canali_pagamento:array();
				if(!empty($canali)){
					$id_canale_ok=0;
					$canale_new=null;
					foreach ($canali as $canale) {
						$canale_new=($canale_new==null)?$canale:$canale_new;
						$sTMP = 'CANALE'.$canale->ID_CANALE;
						if(isset($OUT_BONIFICI[$sTMP])){
							$id_canale_ok=$canale->ID_CANALE;
							break;
						}
					}
					
					if($id_canale_ok>0){
						$OUT_BONIFICI['CANALE'.$id_canale_ok]->scadenze[]=$scad;
					}
					else{
						$BONIFICO_NEW = new stdClass();
						$BONIFICO_NEW->canale=$canale_new;
						$BONIFICO_NEW->scadenze=array($scad);
						$OUT_BONIFICI['CANALE'.$canale_new->ID_CANALE]=$BONIFICO_NEW;
					}
				}
			}
		}
		

		foreach ($OUT_BONIFICI as $key => &$BON) {
			$polizze="";
			$Nominativo = "";
			$premio=0;
			foreach ($BON->scadenze as $scad) {
				$Nominativo = ($Nominativo==""?$scad->NOMINATIVO:$Nominativo);

				$polizze .= ($polizze=="")?"":",";
				$polizze .= $scad->NUMERO_POLIZZA;
				$premio += $scad->PREMIO_TOTALE;
			}
			$BON->PREMIO_TOTALE = $premio;
			$BON->CAUSALE = $Nominativo ." ".$polizze;
		}
		return $OUT_BONIFICI;
	
	}
	
}
