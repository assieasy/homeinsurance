<?php

require_once "Assi_rest_model.php";

class Data_assi_model extends Assi_rest_model
{
    protected $base_params=array();
    protected $msg_auenticazione_errata = "";
    function __construct()
    {
        parent::__construct();
        $this->token = "";
    }
    public function get_msg_auenticazione_errata()
    {
       return $this->msg_auenticazione_errata ;
    }

    public function utente()
    {
        $anag = "";
        $ret = $this->get_utente();
        $this->msg_auenticazione_errata = "";
        if (!empty($ret)) {
            if (isset($ret->success) && $ret->success) {
                $anag = $ret->data;
            }
            if(empty($anag)){
                $this->msg_auenticazione_errata = isset($ret->error)?$ret->error:"";
            }            
        }
        return $anag;
    }

    public function get_utente()
    {
        $params = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "autenticazione/";
        $ret = $this->CallAPI("POST", $url, "",$dati);
        return $ret;
    }

    public function mail_a_cliente($params)
	{
        $params['PROGRAMMA']=isset($params['PROGRAMMA'])?$params['PROGRAMMA']:"";
        
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "anagrafica/";
        $ret = $this->CallAPI("POST", $url,"send_mail",$dati);
        return $ret;
    }
    
    public function get_configurazioni_sito($params = array())
    {
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "homeinsurance/";
        $CONF = $this->CallAPI("POST", $url,"configurazioni",$dati);
        $retConf = new stdClass();
        $retConf->all_config_obj = new stdClass();
        foreach($CONF as $nome_contesto =>$valore_contesto){
            $obj = new stdClass();
            foreach($valore_contesto as $nome_parametro =>$valore_parametro){
                $obj->{strtolower($nome_parametro)}=$valore_parametro;
            }
            $retConf->all_config_obj->{strtolower($nome_contesto)}=$obj;
        }
       
      
        // COSTRUISCO CONFIFURAZIONI "PARTICOLARI"
        $retConf->assieasy_url = $this->assieasy_url;
        //TESTI
        $o = isset($retConf->all_config_obj->homeins_parametri)?$retConf->all_config_obj->homeins_parametri:new stdClass();
        $retConf->titolo = isset($o->titolo)?$o->titolo:"Home Insurance";
        $retConf->sottotitolo = isset($o->sottotitolo)?$o->sottotitolo:"";
        //SITO
        $retConf->stile = strtolower(isset($o->stile)?$o->stile:"default");
        $retConf->favicon = isset($o->link_favicon)?$o->link_favicon:"/sito/custom/favicon/favicon000.ico";
        
        $retConf->logo = isset($o->link_logo)?$o->link_logo:"/sito/assets/img/logo-small.png";

        switch($retConf->stile){
            case "test":
                $retConf->base_css = "test.css";
                break;
            case "blue":
                $retConf->base_css = "blue.css";
                break;
            case "red":
                $retConf->base_css = "red.css";
                break;
            case "green":
                $retConf->base_css = "green.css";
                break;
            case "darkgray":
            case "dark-gray":
            case "dark_gray":
                $retConf->base_css = "darkgray.css";
                break;
            default:
                $retConf->base_css = "default.css";
                break;
        }
        
        //A2HS
        $retConf->a2hs = strtolower(isset($o->a2hs_stile)?$o->a2hs_stile:"");
        $retConf->webmanifest ="";
        switch($retConf->a2hs){
            case "red":
                $retConf->a2hs = "red";
                $retConf->webmanifest ="red.webmanifest";
            break;
            case "darkgray":
            case "dark-gray":
            case "dark_gray":
                $retConf->a2hs = "darkgray";
                $retConf->webmanifest ="darkgray.webmanifest";
                break;    
            case "default":
                $retConf->a2hs = "default";
                $retConf->webmanifest ="default.webmanifest";
            break;
            default:
                $retConf->a2hs = "";
            break;
        }
        
        return $retConf;
    }

    
    

    public function login($params)
	{
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "autenticazione/";
        $ret = $this->CallAPI("POST", $url,"login",$dati);
        return $ret;
    }
    
    public function mailcambiopassword($params)
	{
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "autenticazione/";
        $ret = $this->CallAPI("POST", $url,"ask_reset_password",$dati);
        return $ret;
	}

    public function execcambiopassword($params){
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "autenticazione/";
		$ret = $this->CallAPI("POST", $url,"change_password_token",$dati);
        return $ret;
    }

    public function updatepassword($params){
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "autenticazione/";
		$ret = $this->CallAPI("POST", $url,"change_password",$dati);
        return $ret;
    }
    
    public function ask_for_new_user($params)
	{
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "autenticazione/";
        $ret = $this->CallAPI("POST", $url,"ask_for_new_user",$dati);
        return $ret;
    }
    
    public function get_polizze($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "polizze/";
        $ret = $this->CallAPI("POST", $url, "get", $dati);
        return $ret;
    }
    public function get_anagrafica($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "anagrafica/";
        $ret = $this->CallAPI("POST", $url, "get", $dati);
        return $ret;
    }
    public function get_contatti_utili($params)
	{
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "anagrafica/";
        $ret = $this->CallAPI("POST", $url,"contatti_utili",$dati);
        return $ret;
    }

    public function get_anagrafica_privacy($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "anagrafica/";
        $ret = $this->CallAPI("POST", $url, "privacy", $dati);
        return $ret;
    }
    public function put_anagrafica_privacy($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "anagrafica/";
        $ret = $this->CallAPI("JSON", $url, "put_privacy", $dati);
        return $ret;
    }
    public function update_anagrafica_hi($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "anagrafica/";
        $ret = $this->CallAPI("JSON", $url, "update_anagrafica_hi", $dati);
        return $ret;
    }
    public function get_documenti($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "documenti/";
        $ret = $this->CallAPI("POST", $url, "get", $dati);
        return $ret;
    }

    public function get_titoli($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "titoli/";
        $ret = $this->CallAPI("POST", $url, "get", $dati);
        return $ret;
    }
    /******************* SINISTRI  ******************/
    public function get_sinistri($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "sinistri/";
        $ret = $this->CallAPI("POST", $url, "get", $dati);
        return $ret;
    }
    public function put_sinistro($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "sinistri/";
        $ret = $this->CallAPI("JSON", $url, "put", $dati);
        return $ret;
    }
    public function put_sinistro_soggetto($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "sinistri/";
        $ret = $this->CallAPI("JSON", $url, "put_soggetto", $dati);
        return $ret;
    }

    public function put_sinistro_allegato($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "sinistri/";
        $ret = $this->CallAPI("JSON", $url, "put_allegato", $dati);
        return $ret;
    }
    public function stampa_sinistro($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "sinistri/";
        $ret = $this->CallAPI("JSON", $url, "to_pdf", $dati);
        return $ret;
    }
    
    
    /********* PREVENTIVI / REGOLAZIONE ********/
    public function get_regolazione($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "preventivi/";
        $ret = $this->CallAPI("POST", $url, "get", $dati);
        return $ret;
    }
    public function delete_preventivo($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "preventivi/";
        $ret = $this->CallAPI("JSON", $url, "delete", $dati);
        return $ret;
    }
    public function put_preventivo($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "preventivi/";
        $ret = $this->CallAPI("JSON", $url, "put", $dati);
        return $ret;
    }
    public function put_regolazione($params = array())
    {
        return $this->put_preventivo($params);
    }
    public function put_preventivo_testa($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "preventivi/";
        $ret = $this->CallAPI("JSON", $url, "put_testa", $dati);
        return $ret;
    }
    public function cambia_stato_preventivo($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "preventivi/";
        $ret = $this->CallAPI("JSON", $url, "cambia_stato", $dati);
        return $ret;
    }
    public function stampa_regolazione($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "preventivi/";
        $ret = $this->CallAPI("JSON", $url, "to_pdf", $dati);
        return $ret;
    }
    
    public function get_messaggi($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "messaggi/";
        $ret = $this->CallAPI("POST", $url, "get", $dati);
        return $ret;
    }
    public function put_messaggio($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "messaggi/";
        $ret = $this->CallAPI("JSON", $url, "put_messaggio", $dati);
        return $ret;
    }
    
    // ********** TABELLE
    public function get_tab_sistema($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "tabelle/";
        $ret = $this->CallAPI("POST", $url, "get_tab_sistema", $dati);
        return $ret;
    }
    public function get_tab_nodo($params = array())
    {
        $dati = array();
        $dati = $this->add_params($params,$this->base_params,true);
        $url = $this->rets_url . "tabelle/";
        $ret = $this->CallAPI("POST", $url, "get_tab_nodo", $dati);
        return $ret;
    }

}
