<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-polizze",
        "fa_icon"=>"id-card",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > Polizze</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);

    $gestiscoAnnullate = in_array("polizze.annullate",$funzioni_abilitate);
    $vedi_annullate = isset($page_data['vedi_annullate'])?$page_data['vedi_annullate']:0;
   
    ?>
    <form id="form-filtri" name="form-filtri" method="POST">
    <div class="row pt-1">
        <?if($gestiscoAnnullate) {?>
            <div class="px-5">
                <div class="form-group border p-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" 
                            name="chkSOLO_POLIZZE_VIVE" id="chkSOLO_POLIZZE_VIVE"
                            <?=($vedi_annullate>0?"":"checked")?>
                            >
                        <label class="form-check-label" for="chkSOLO_POLIZZE_VIVE">
                            Solo Contratti Attivi
                        </label>
                    </div>
                </div>
                <input type="hidden" value="<?=$vedi_annullate?>" name="VEDI_POLIZZE_ANNULLATE" id="VEDI_POLIZZE_ANNULLATE" >
                    
            </div>
        <?}?>  
        <div class="ml-auto">
            <div class="btn-group card-dettaglio mr-4" role="toolbar"  aria-label="switch-visione">
                <a role="button" class="btn btn-loading" href="/polizze/polizze?TIPO_ELENCO=TABELLA"><i class="fa fa-list" aria-hidden="true"></i></a>
                <a role="button" class="btn disabled btn-loading" href="/polizze/polizze?TIPO_ELENCO=CARD"><i class="fa fa-table" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    </form>
    <?  $aCapo = true;
        $contaRecord=0;
        $quanti_per_riga = 2;
        $i = count($page_data['polizze']);
        if($i<$quanti_per_riga){
            $quanti_per_riga = $i;
        }
        /*if($i % 4 == 0){
            $quanti_per_riga = 4;
        }*/
        foreach($page_data['polizze'] as $key => $pol) {
            if($aCapo){
                if($contaRecord>0){
                    //chiusura div row precedente
                    ?>
                    </div><!-- chiusura div row precedente -->
                    <?
                }
                ?>
                    <div class="row"> <!--div row -->
                <?
            }
            $contaRecord++;
            $aCapo = (($contaRecord % $quanti_per_riga)==0);

            $dtScadenza = new DateTime($pol->DATA_SCADENZA_CONTRATTO);
            $dtEff = new DateTime($pol->DATA_EFFETTO_ORI);
            ?>
            <div class="col-md-<?=(12/$quanti_per_riga)?> d-flex align-items-stretch pt-2">
                <div class="card card-dettaglio" >
                    <div class="card-header">
                        <a class="align-self-end pr-3 btn-loading" href="/polizze/polizza/<?=$pol->ID_POLIZZA?>" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <strong>
                            Polizza N. 
                            <a href="/polizze/polizza/<?=$pol->ID_POLIZZA?>" class="btn-loading""><u><?=$pol->NUMERO_POLIZZA?></u></a>
                        </strong>
                       
                    </div>
                    <div class="card-body">
                        <dl class="row small">
                        <?
                            $valori=array();
                            $valori[]=array("label"=>"Compagnia","valore"=>$pol->DESC_COMPAGNIA);
                            $valori[]=array("label"=>"Contraente","valore"=>$pol->NOMINATIVO);
                            $sTMP = isset($pol->BENEFICIARIO_CAUZIONI)?$pol->BENEFICIARIO_CAUZIONI:"";
                            if($sTMP!=""){
                                $valori[]=array("label"=>"Beneficiario","valore"=>$sTMP);
                            }
                            if($pol->TARGA!=""){
                                $valori[]=array("label"=>"Targa","valore"=>$pol->TARGA);
                            }
                            $valori[]=array("label"=>"Ramo","valore"=>$pol->DESC_RAMO);
                            $valori[]=array("label"=>"Prodotto","valore"=>$pol->DESC_PRODOTTO);
                            $valori[]=array("label"=>"Frazionamento","valore"=>$pol->FRAZIONAMENTO);
                            $valori[]=array("label"=>"Data Decorrenza","valore"=>$dtEff->format('d/m/Y'));
                            $valori[]=array("label"=>"Data Scadenza","valore"=>$dtScadenza->format('d/m/Y'));
                            $valori[]=array("label"=>"Stato","valore"=>$pol->DESC_STATO_POLIZZA);
                            foreach($valori as $valore){
                                ?>
                                <dt class="col-4"><?=$valore["label"]?>:</dt>
                                <dd class="col-8"><?=$valore["valore"]?></dd>
                                <?
                            }
                        ?>
                        </dl>

                        <? 
                        $arretrati = (isset($pol->scadenze)?$pol->scadenze:array());
                        if(!empty($arretrati) &&  in_array("scadenze.msg",$funzioni_abilitate)){
                        ?>
                            <span class="row alert alert-info alert-info-custom">Polizza in scadenza</span>
                        <?
                        }
                        ?>
                    </div>
                </div>
            </div> 
                      
    <?  }
    
    
    if($contaRecord>0){
        //chiusura div row precedente
        ?>
        </div>
        <?
    }?>
</div>


<script>
/*************** funzioni init ***********/
pageinitfunctions = pageinitfunctions?pageinitfunctions:[];
/*************** funzioni init ***********/
pageinitfunctions.push({name:'soloViveChange'});
function soloViveChange(){
   
    $('#chkSOLO_POLIZZE_VIVE').change(function() {
        console.log("fff");
        $('#chkSOLO_POLIZZE_VIVE').val($(this).is(':checked'));
        $('#VEDI_POLIZZE_ANNULLATE').val($(this).is(':checked')?0:1);
        var frm = '#form-filtri';
        $(frm).attr("action","/polizze/polizze");
        $(frm).attr("target","");
        start_spinner();
        $(frm).submit();
        return false;
    });
}

</script>