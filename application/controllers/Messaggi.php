<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messaggi extends MY_Controller {
	/**
     * @var Messaggi_model
     */
	protected $MSGMOD;
	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		$this->load->model("Messaggi_model");
		$this->MSGMOD = new Messaggi_model($this->assi);
	}
	public function index()
	{
		$this->messaggio();
	}

	public function polizza($id_polizza=0)
	{
		$params = array();
		$progressivo_messaggio = isset($_POST['PROGRESSIVO_MESSAGGIO'])?$_POST['PROGRESSIVO_MESSAGGIO']:0;
		$params['ID_POLIZZA'] = $id_polizza;
		$params['ID_TITOLO'] = 0;
		$params['ID_SINISTRO'] = 0;
		
		$this->messaggio($params);
	}

	public function titolo($id_titolo=0)
	{
		$params = array();
		$params['ID_POLIZZA'] = 0;
		$params['ID_TITOLO'] = $id_titolo;
		$params['ID_SINISTRO'] = 0;
		
		$this->messaggio($params);
	}

	public function sinistro($id_sinistro=0)
	{
		$params = array();
		$params['ID_POLIZZA'] = 0;
		$params['ID_TITOLO'] = 0;
		$params['ID_SINISTRO'] = $id_sinistro;
		
		$this->messaggio($params);
	}

	public function agenzia()
	{
		$params = array();
		$params['ID_POLIZZA'] = 0;
		$params['ID_TITOLO'] = 0;
		$params['ID_SINISTRO'] = 0;
		
		$this->messaggio($params);
	}

	public function invia(){
		$params = array();
		foreach ($_POST as $key => $value) {
			$params[$key]=$value;
		}
		$params['files']=array();
		if(isset($_FILES["FILE_ALLEGATO"])){
			$params['files'][]= $_FILES["FILE_ALLEGATO"];
		}

		$this->messaggio($params);
	}

	protected function messaggio($params_in = array())
	{
		$azione = isset($params_in['AZIONE'])?$params_in['AZIONE']:"";
		$errori = array();
		$progressivo_messaggio = isset($params_in['PROGRESSIVO_MESSAGGIO'])?$params_in['PROGRESSIVO_MESSAGGIO']:0;

		switch($azione){
			case "SALVA":
				$retmsg = $this->write_messaggio($params_in);
				$errori = isset($retmsg['errori'])?$retmsg['errori']:$errori;
				$progressivo_messaggio = isset($retmsg['PROGRESSIVO_MESSAGGIO'])?$retmsg['PROGRESSIVO_MESSAGGIO']:0;
				
			break;
		}

		

		$page_config=array();
		$page_config['titolo']="Messaggi";
		$page_data = array();
		
		$page_data['errori']=$errori;
		$page_data['PROGRESSIVO_MESSAGGIO']=$progressivo_messaggio;

		//TEST SU SINISTRO
		$myId =isset($params_in['ID_SINISTRO'])?$params_in['ID_SINISTRO']:0;
		if($myId>0){
			$this->load->model("Sinistri_model");
			$SMOD = new Sinistri_model($this->assi);
			$p=array();
			$p['ID_SINISTRO']=$myId;			
			$page_data['DATI_SINISTRO'] = $SMOD->get_sinistro($p);
		}
		//TEST SU TITOLO
		$myId =isset($params_in['ID_TITOLO'])?$params_in['ID_TITOLO']:0;
		if($myId>0){
			$this->load->model("Titoli_model");
			$TMOD = new Titoli_model($this->assi);
			$p=array();
			$p['ID_TITOLO']=$myId;			
			$page_data['DATI_TITOLO'] = $TMOD->get_titolo($p);
		}
		//TEST SU POLIZZA
		$myId =isset($params_in['ID_POLIZZA'])?$params_in['ID_POLIZZA']:0;
		if($myId>0){
			$this->load->model("Polizze_model");
			$PMOD = new Polizze_model($this->assi);
			$p=array();
			$p['ID_POLIZZA']=$myId;			
			$page_data['DATI_POLIZZA'] = $PMOD->get_polizza($p);
		}


		$p=array();
		$p['sorts']=array();
		$p['sorts'][]=array("column"=>"DATA_RICEZIONE","order"=>"DESC");

		//filtro ultimi 20 messaggi
		$p['limit']=20;
		//filtro i messaggi mandati nell'ultimo mese...
		$datafiltro = date("Y-m-01", strtotime("-1 months"));
		$p['filters']=array();
		$p['filters'][]=array("column"=>"DATA_RICEZIONE","value"=>$datafiltro,"type"=>"date","comparison"=>"gt");

		$ultimi_messaggi_inviati = $this->MSGMOD->get_messaggi($p);
		$page_data['ultimi_messaggi_inviati'] = $ultimi_messaggi_inviati ;

		$this->show("user_messaggio",$page_config,$page_data);
	}

	protected function write_messaggio($params_in=array()){
		$messaggio = $this->post_to_messaggio($params_in);
		if(empty($messaggio["errori"])){
			$ret = $this->MSGMOD->put_messaggio($messaggio);
			if( (isset($ret->success)?$ret->success:FALSE) ){
				$retData = $ret->data;
				$messaggio['PROGRESSIVO_MESSAGGIO'] = $retData->PROGRESSIVO_MESSAGGIO;
			}
			else{
				$sTMP = isset($ret->error)?$ret->error:"Errore nella consegna del messaggio";
				$messaggio["errori"][]=$sTMP;
			}
		}

		return $messaggio;
	}
	protected function post_to_messaggio($params_in=array()){
		if(empty($params_in)) {
			return array();
		}
		
		$retVal = array();
		foreach($params_in as $key=>$value){
			$retVal[$key]=$value;
		}
		$retVal["TITOLO"]=isset($retVal["TITOLO"])?$retVal["TITOLO"]:"";
		$retVal["MESSAGGIO"]=isset($retVal["MESSAGGIO"])?$retVal["MESSAGGIO"]:"";
		$retVal["REFERENTE"]=isset($retVal["REFERENTE"])?$retVal["REFERENTE"]:"";
		$retVal["errori"]=array();

		if($retVal["MESSAGGIO"]==""){
			$retVal["errori"][]="Manca il testo del messaggio";
		}
		if($retVal["TITOLO"]==""){
			$retVal["errori"][]="Manca il titolo del messaggio";
		}
		
		
		return $retVal;
	}
	
}
