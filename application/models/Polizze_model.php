<?php

require_once "Base_rest_model.php";

class Polizze_model extends Base_rest_model
{
    public function get_polizze($params=array()){
        $params = $this->add_filtri_polizza($params);
		$ret = $this->CH->get_polizze($params);
		$elenco = $this->CH->extract_data($ret);
        return $elenco;
    }
    public function get_polizze_vive($params=array()){
        $params['SOLO_VIVE']=1;
        $params = $this->add_filtri_polizza($params);
        $ret = $this->CH->get_polizze($params);
		$elenco = $this->CH->extract_data($ret);
        return $elenco;
    }
    public function get_polizza($params=array()){
        $params = $this->add_filtri_polizza($params);
        $ret = $this->CH->get_polizze($params);
     	$elenco = $this->CH->extract_first_data($ret);
        return $elenco;
    }

    protected function add_filtri_polizza($params=array()){
        $this->load->model("Titoli_model");
        $TM = new Titoli_model();

        $p = isset($params['filtri_scadenze'])?$params['filtri_scadenze']:array();
        $tmp =  $TM->get_filtro_scadenze();
        foreach($tmp as $key=>$value){
            $p[$key] = isset($p[$key] )?$p[$key] :$value;
        }
        $params['filtri_scadenze'] = $p;

        $p = isset($params['filtri_incassi'])?$params['filtri_incassi']:array();
        $tmp =  $TM->get_filtro_incassi();
        foreach($tmp as $key=>$value){
            $p[$key] = isset($p[$key] )?$p[$key] :$value;
        }
        $params['filtri_incassi'] = $p;

        return $params;
    }
    
    
		
}
