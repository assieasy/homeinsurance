<?php

require_once "Base_rest_model.php";

class Tabelle_model extends Base_rest_model
{
    const SIS_TAB_PROFESSIONI = 10006;
    const SIS_TAB_TIPO_SOCIETA = 10007;
    
    public function get_professioni($params=array()){
        $params["TABELLA"] = self::SIS_TAB_PROFESSIONI;
        return $this->get_tab_sistema($params);
    }
    public function get_professione($params=array()){
        $params["TABELLA"] = self::SIS_TAB_PROFESSIONI;
        return $this->get_elemento_sistema($params);
    }
    public function get_tipi_societa($params=array()){
        $params["TABELLA"] = self::SIS_TAB_TIPO_SOCIETA;
        return $this->get_tab_sistema($params);
    }
    public function get_tipo_societa($params=array()){
        $params["TABELLA"] = self::SIS_TAB_TIPO_SOCIETA;
        return $this->get_elemento_sistema($params);
    }

    public function get_tab_sistema($params=array()){
        $params["sorts"] = isset($params["sorts"])?$params["sorts"]:$this->get_default_sort();
        $ret = $this->CH->get_tab_sistema($params);
        $elenco = $this->CH->extract_data($ret);
        return $elenco;
    }
    public function get_elemento_sistema($params=array()){
        $params["sorts"] = isset($params["sorts"])?$params["sorts"]:$this->get_default_sort();
        $ret = $this->CH->get_tab_sistema($params);
        $elemento = $this->CH->extract_first_data($ret);
        return $elemento;
    }
    public function get_tab_nodo($params=array()){
        $params["sorts"] = isset($params["sorts"])?$params["sorts"]:$this->get_default_sort();
        $ret = $this->CH->get_tab_nodo($params);
        $elenco = $this->CH->extract_data($ret);
        return $elenco;
    }
    public function get_elemento_nodo($params=array()){
        $params["sorts"] = isset($params["sorts"])?$params["sorts"]:$this->get_default_sort();
        $ret = $this->CH->get_tab_nodo($params);
        $elemento = $this->CH->extract_first_data($ret);
        return $elenco;
    }
    public function get_default_sort(){
        $params=array();
        $params[]=array("column"=>"DESCRIZIONE","order"=>"ASC");
        return $params;
    }
    
    
}
