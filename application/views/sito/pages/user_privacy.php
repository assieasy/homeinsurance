<?

$anagrafica_privacy = isset($page_data['anagrafica_privacy'])?$page_data['anagrafica_privacy']:array();

$elenco_privacy = isset($anagrafica_privacy->elenco_privacy)?$anagrafica_privacy->elenco_privacy:array();

$all_config_obj = $sito['all_config_obj'];
$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();

?>
<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-account",
        "fa_icon"=>"user-shield",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > Privacy</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
    <div class="row pt-1">
        <div class="col-md-12">
            <div class="card" >
                <div class="card-body">
                <form id="form-privacy" method="POST">
                <input type="hidden" name="AZIONE" id="inputAZIONE"   value="">
                <? 
                
                $link_privacy = "";
                $sTMP= isset($hiPar->link_privacy)?$hiPar->link_privacy:"";
                if($sTMP){
                    $link_privacy="<a href='$sTMP' onclick='segnaPresaVisione()' target='_blank' >Privacy Policy</a>";
                }
                ?>
                <?if($link_privacy!=""){ ?>
                    <div class="card">
                        <div class="card-header">
                            <input id="checkboxPresaVisione" type="checkbox">
                            <label for="checkboxPresaVisione"> Ho preso visione della &nbsp;<?=$link_privacy ?></label>
                            <div id="alertPresaVisione" class="alert alert-warning alert-dismissible collapse" role="alert">
                                <strong>Attenzione!</strong> conferma di aver preso visione della privacy.
                            </div>
                        </div>
                    </div>
                <?}?>       
                <?
                foreach($elenco_privacy as $PRI){    
                    $si_checked="";
                    $no_checked="";
                    $descrizione_estesa=str_replace(PHP_EOL,"<br>",$PRI->DESCRIZIONE_ESTESA);
                    if($PRI->DATA_CONSENSO!=""){
                        if($PRI->CONSENSO>0){
                            $si_checked="checked";
                        }
                        else{
                            $no_checked="checked";
                        }
                    }
                    ?>
                    <div class="form-row pt-3">
                        <div class="card w-100" id="cardPrivacy<?=$PRI->NUMERO_PRIVACY?>">
                            <div class="card-header ">
                                <?=$PRI->DESCRIZIONE?>
                                <?if($PRI->NUMERO_PRIVACY==0 && $no_checked!=""){?>
                                    <span class="alert alert-danger">
                                    (Consenso obbligatorio per la navigazione nel sito)
                                    </span>
                                <?}?>
                                
                            </div>
                            <div  class="card-body small">
                                <?=$descrizione_estesa?>
                            </div>
                            <div  class="card-footer">
                                <div class="form-check form-check-group">
                                    <input name="PRIVACY_<?=$PRI->NUMERO_PRIVACY?>" 
                                        type="radio" 
                                        value=1
                                        id="PRIVACY_SI_<?=$PRI->NUMERO_PRIVACY?>" <?=$si_checked?>>
                                    <label for="PRIVACY_SI_<?=$PRI->NUMERO_PRIVACY?>">Acconsento</label>
                                    <input name="PRIVACY_<?=$PRI->NUMERO_PRIVACY?>" 
                                        type="radio" 
                                        value=0
                                        id="PRIVACY_NO_<?=$PRI->NUMERO_PRIVACY?>" <?=$no_checked?>>
                                    <label for="PRIVACY_NO_<?=$PRI->NUMERO_PRIVACY?>">Declino</label>
                                   
                                </div>
                                <div id="alertPrivacy<?=$PRI->NUMERO_PRIVACY?>" class="alert alert-warning alert-dismissible collapse" role="alert">
                                    <strong>Attenzione!</strong> Selezionare il consenso.
                                </div>
                            </div>
                        </div>
                    </div>
                <?}?>
               
                <div class="form-row pt-2">
                    <div class="col-12 justify-content-center">
                        <button type="button" class="btn btn-primary" href="#" onclick="salvaPrivacy('SALVA')" >Conferma</button>
                    </div>   
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

function segnaPresaVisione(){
    var presaVisione = $('#form-privacy #checkboxPresaVisione');
    if(presaVisione){
        presaVisione.prop( "checked", true );
    }
}

function salvaPrivacy( azione){
  var ok=true;
  var obj = $('#form-privacy #checkboxPresaVisione');
  if(obj.length){
     ok =obj.is(":checked");
  }

  var alertObj = $("#form-privacy #alertPresaVisione");
  if(!ok){
    if(alertObj.length){
        alertObj.collapse('show');
        $(document).scrollTop( alertObj.offset().top );
    }
    else{
        console.log("Link_Privacy non impostato!");
    }
    return false;
  }
  if(alertObj.length){
    alertObj.collapse('hide');
  }
  
  var privacySI;
  var privacyNO;
  <? foreach($elenco_privacy as $PRI){ ?>
    obj = $('#form-privacy #PRIVACY_SI_<?=$PRI->NUMERO_PRIVACY?>');
    privacySI=obj.is(":checked");
    obj = $('#form-privacy #PRIVACY_NO_<?=$PRI->NUMERO_PRIVACY?>');
    privacyNO=obj.is(":checked");
    if(!privacySI&&!privacyNO){
        $("#alertPrivacy<?=$PRI->NUMERO_PRIVACY?>").collapse('show');
        $(document).scrollTop( $("#cardPrivacy<?=$PRI->NUMERO_PRIVACY?>").offset().top );
        return;
    }
    $("#alertPrivacy<?=$PRI->NUMERO_PRIVACY?>").collapse('hide');
  <?}?>

  $('form-privacy').attr("action","/privacy/privacy");
  $('#form-privacy #inputAZIONE').val(azione);
  $('#form-privacy').attr("target","");
  $('#form-privacy').submit();
  return false;
}


</script>