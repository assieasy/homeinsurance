$( document ).ready(function() {
    // pageinitfunctions e' un array di funzioni da eseguire
    // ogni funzione e' un oggetto : {name:'nomefunzione',params: [arraydiparametri]}
    // name 
    if(pageinitfunctions){
        pageinitfunctions.forEach(element => {
            var fName=element.name;
            var fnparams = element.params;
            var fn = window[fName];
            if (typeof fn  === "function") {
                fn.apply(null, fnparams);
            }
            
        });
    }

    //gli oggetti di classe "btn-loading" fanno partire lo spinner al click
    $(".btn-loading").on("click", function(e) {
        var secondi = $(this).attr("loadingSecondi");
        start_spinner(secondi);
      });
});


function start_spinner(secondi){
    // e.preventDefault();
    $("#modal-spinner").modal({
        backdrop: "static", //remove ability to close modal with click
        keyboard: false, //remove option to close with keyboard
        show: true //Display loader!
    });
    if(!secondi || !$.isNumeric(secondi)){secondi=30;}
    setTimeout(function() {
        stop_spinner();
    }, secondi*1000);
}
function stop_spinner(esegui){
    //e.preventDefault();
    $("#modal-spinner").modal("hide");
    if(esegui){
        $("#modal-spinner").modal("hide");
    }
    else{
        //se troppo rapido non riesco a nasconderlo,percui riprovo dopo 1 secondo
        var secondi = 1;
        setTimeout(function() {
            stop_spinner(true);
        },secondi*1000);
    }
}