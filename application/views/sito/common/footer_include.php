        <!-- Bootstrap core JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->

        <? 
        if(isset($page_data['contatti_utili'])){
            $CONTATTI_UTILI  = $page_data['contatti_utili'];
            $CONTATTI_TMP = array();
            foreach($CONTATTI_UTILI as $MSG){
                    if($MSG->CONTESTO == Anagrafica_model::CONTATTI_ULTILI_FOOTER_CUSTOM){
                    $CONTATTI_TMP[]= $MSG;
                    }
            }
            if(!empty($CONTATTI_TMP)){
                foreach($CONTATTI_TMP as $MSG){
                        echo $MSG->DESCRIZIONE;
                }
            }
        }
        ?>