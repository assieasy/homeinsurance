<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="<?=(isset($sito['descrizione'])?$sito['descrizione']:"") ?>" />
        <meta name="author" content="<?=(isset($sito['autore'])?$sito['autore']:"") ?>" />
        <title>
            <?
                $sTMP = (isset($sito['titolo'])?$sito['titolo']:"Home Insurance");
                //se ci fosse del testo html...estraggo solo il testo
                $sTMP = strip_tags($sTMP);
            ?>
            <?=$sTMP?>
        </title>
        <link rel="icon" type="image/x-icon" href="<?=(isset($sito['favicon'])?$sito['favicon']:"favicon.ico")?>">
        
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
       <!-- <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />-->
        <!-- Third party plugin CSS-->
     <!--   <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />-->
        <!-- Core theme CSS (includes Bootstrap)-->

        <? $ver="2.0.3" ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        
        <link href="/sito/css/styles.css?v=<?=$ver?>>" rel="stylesheet" />
        <link href="/sito/css/<?=(isset($sito['base_css'])?$sito['base_css']:"styles.css") ?>?v=<?=$ver?>" rel="stylesheet" />
        
        <?
        if(isset($css_files)){
            
            foreach($css_files as $cssf){
                echo '<link href="/sito/css/'.$cssf.'?v=<?=$ver?>"  rel="stylesheet"  />';
            } 
        }
        ?>
        <?
        if($sito["a2hs"]!=""){
            switch($sito["a2hs"]){
                default:
                   ?> 
                    <link rel="manifest" href="/sito/assets/manifest/<?=$sito["webmanifest"]?>">   
                   <?
                break;
            }
        }
        ?>

        <script>var pageinitfunctions=[];</script>
    </head>
    <body id="page-top">