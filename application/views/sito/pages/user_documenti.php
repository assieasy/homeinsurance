<div class="container-fluid h-100 maincontainer pb-4">
    <div class="row p-4">
        <div class="col-12">
            <?  if(!empty($page_data['documenti'])){?>
            <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">Tipo documento</th>
                <th scope="col">Nome File</th>
                </tr>
            </thead>
            <tbody>
            <?php

                
                    foreach($page_data['documenti'] as $key => $doc){
                
                        echo '<tr>';
                        echo "<td>".$doc->DESC_TIPO_DOCUMENTO."</td>";
                        echo '<td><a target="_blank" href="'.$sito['assieasy_url'].$doc->URL_DOCUMENTO.'" >'.$doc->NOME_FILE.'</a></td>';
                        echo '</tr>';

                    } 
                
            ?>
            </tbody>
            </table>
            <?
            }else{
                        echo '<div class="alert alert-info alert-info-custom" role="alert">
                        Nessun documento disponibile 
                        </div>';
                }
            ?>
        </div>
    </div>
</div>