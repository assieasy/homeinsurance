
<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-sinistri",
        "fa_icon"=>"user-injured",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > Sinistri</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
    <div class="row pt-1">
        <div class="ml-auto">
            <div class="btn-group card-dettaglio mr-4" role="toolbar"  aria-label="switch-visione">
                <a role="button" class="btn disabled btn-loading" href="/sinistri/sinistri?TIPO_ELENCO=TABELLA"><i class="fa fa-list" aria-hidden="true"></i></a>
                <a role="button" class="btn btn-loading" href="/sinistri/sinistri?TIPO_ELENCO=CARD"><i class="fa fa-table" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="row p-4">
        <div class="table-responsive col-12">
            <table class="table table-sm table-hover small">
            <thead>
                <tr>
                <th scope="col">Data Evento</th>
                <th scope="col">Stato</th>
                <th scope="col">Numero</th>
                <th scope="col">Polizza</th>
                <th scope="col">Tipo Evento</th>
                </tr>
            </thead>
            <tbody>
            <? foreach($page_data['sinistri'] as $key => $sin){
                        $dtSin = new DateTime($sin->DATA_SINISTRO);
                        ?>
                        <tr>   
                            <td><?=$dtSin->format('d/m/Y')?></td>
                            <td><?=(isset($sin->DESC_PARTICOLARITA)?$sin->DESC_PARTICOLARITA:$sin->STATO_SINISTRO_DESC)?></td>
                            <td><a class="btn-loading" href="/sinistri/sinistro/<?=$sin->ID_SINISTRO?>" nowrap><i class="fa fa-user-injured mr-1" aria-hidden="true"></i><?=$sin->NUMERO_SINISTRO?></a></td>
                            <td><a class="btn-loading" href="/polizze/polizza/<?=$sin->ID_POLIZZA?>" nowrap ><i class="fa fa-id-card mr-1" aria-hidden="true"></i><?=$sin->NUMERO_POLIZZA?></a></td>
                            <td><?=$sin->DESC_GARANZIA_SINISTRO?></td>
                        </tr>

            <?  }?>
            </tbody>
            </table>
        </div>
    </div>
</div>