<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Titoli extends MY_Controller {
	 /**
     * @var Titoli_model
     */ 
	protected $TITMOD;

	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		if(!$this->controllo_privacy()){
			header('Location: /privacy');
		}
		$this->load->model("Titoli_model");
		$this->TITMOD = new Titoli_model($this->assi);
	}
	public function index()
	{
		$this->scadenze();
	}

	public function scadenze()
	{
		//$tipo_elenco = isset($_POST['TIPO_ELENCO'])?$_POST['TIPO_ELENCO']:"";
		$tipo_elenco = $this->get_tipo_elenco();

		$page_config=array();
		$page_config['titolo']="scadenze";
		$page_data = array();
		$page_data['titoli']= $this->find_scadenze();
		

		if($tipo_elenco=="TABELLA"){
			$this->show("user_titoli",$page_config,$page_data);
		}
		else{
			$this->show("user_titoli_card",$page_config,$page_data);
		}
	}
	public function titoli()
	{
		$tipo_elenco = $this->get_tipo_elenco();
		//$tipo_elenco = isset($_POST['TIPO_ELENCO'])?$_POST['TIPO_ELENCO']:"";
		
		$page_config=array();
		$page_config['titolo']="Titoli";
		$page_data = array();
		$page_data['titoli']= $this->find_titoli();
		if($tipo_elenco=="TABELLA"){
			$this->show("user_titoli",$page_config,$page_data);
		}
		else{
			$this->show("user_titoli_card",$page_config,$page_data);
		}
	}

	public function pagamenti()
	{
		$tipo_elenco = $this->get_tipo_elenco();
		
		//$tipo_elenco = isset($_POST['TIPO_ELENCO'])?$_POST['TIPO_ELENCO']:"";
		
		$page_config=array();
		$page_config['titolo']="Titoli";
		$page_data = array();
		$page_data['pagina_origine'] = "pagamenti";

		$p=array();
		//$p['SHOW_SCADENZE']=1;
		$p['sorts']=array();
		$p['sorts'][]=array("column"=>"DATA_EFFETTO","order"=>"DESC");
		$p['sorts'][]=array("column"=>"ID_POLIZZA","order"=>"DESC");
		$page_data['titoli']= $this->find_pagamenti($p);
		if($tipo_elenco=="TABELLA"){
			$this->show("user_titoli",$page_config,$page_data);
		}
		else{
			$this->show("user_titoli_card",$page_config,$page_data);
		}
	}

	public function titolo($id_titolo=0)
	{
		$id_titolo = isset($_POST['ID_TITOLO'])?$_POST['ID_TITOLO']:$id_titolo;
		
		if($id_titolo==0){
			$this->titoli();
		}
		$this->load->model("Documenti_model");
		$DOCMOD = new Documenti_model($this->assi);
		/*$this->load->model("Polizze_model");
		$POLMOD = new Polizze_model($this->assi);*/
		$page_config=array();
		$page_config['titolo']="Scadenza";

		$page_data = array();
		
		
		$p=array();
		$p['ID_TITOLO']=$id_titolo;
		$p['SHOW_GARANZIE']=1;
		$p['SHOW_SCADENZE']=1;
		$p['SHOW_POLIZZA']=1;
		$p['SHOW_CANALI_PAGAMENTO']=1;
		$tit = $this->TITMOD->get_titolo($p);
		//echo $this->TITMOD->get_last_response();die();
		if(empty($tit)){
			$this->titoli();
			return;
		}
		if(!empty($tit)){
			/*$p=array();
			$p['ID_POLIZZA']= $tit->ID_POLIZZA;
			$pol = $POLMOD->get_polizza($p);
			$tit->polizza = $pol;*/
			
			$p=array();
			$p['ID_TITOLO']= $tit->ID_TITOLO;
			$docs = $DOCMOD->get_documenti($p);
			$tit->documenti = $docs;

			
			
			if(isset($tit->canali_pagamento)){
				foreach ($tit->canali_pagamento as $canale) {
					if($canale->COD_TIPO_CANALE=="BONIFICO"){
						$canale->CAUSALE = $this->bonifico_build_causale($tit);
						$tit->canale_bonifico = $canale;
						break;
					}
				}
				/*foreach ($tit->canali_pagamento as $canale) {
					if($canale->COD_TIPO_CANALE=="NEXI"){
						$canale->CAUSALE = $this->bonifico_build_causale($tit);
						$tit->canale_nexi = $canale;
						break;
					}
				}*/
			}

			//echo json_encode($tit);die();
			
		}
		
		$page_data['scadenza']= $tit;
		$this->show("user_titolo",$page_config,$page_data);
	}

	protected function find_titoli($params=array())
	{
		$p=array();
		foreach($params as $k=>$v){
			$p[$k] = $v;
		}
		$miei_titoli = $this->TITMOD->get_titoli($p);
	
		return $miei_titoli;
	}
	protected function find_scadenze($params=array())
	{
		$p=array();
		foreach($params as $k=>$v){
			$p[$k] = $v;
		}
		//$this->TITMOD->get_scadenze($p);
		
		return $this->TITMOD->get_scadenze($p);
	}	
	protected function find_pagamenti($params=array())
	{
		$p=array();
		foreach($params as $k=>$v){
			$p[$k] = $v;
		}
		return $this->TITMOD->get_ultimi_incassi($p);
	}


	public function bonifico($id_titolo=0)
	{
		$id_titolo = isset($_POST['ID_TITOLO'])?$_POST['ID_TITOLO']:$id_titolo;
		if($id_titolo==0){
			$this->titoli();
		}
		$this->load->model("Polizze_model");
		$POLMOD = new Polizze_model($this->assi);
		$page_config=array();
		$page_config['titolo']="Bonifico";

		$page_data = array();

		$id_canale_pagamento=0;
		$p=array();
		$p['ID_TITOLO']=$id_titolo;
		//$p['SHOW_GARANZIE']=1;
		//$p['SHOW_SCADENZE']=1;
		//$p['SHOW_POLIZZA']=1;
		$p['SHOW_CANALI_PAGAMENTO']=1;
		$tit = $this->TITMOD->get_titolo($p);
		//echo $this->TITMOD->get_last_response();die();
		if(empty($tit)){
			$this->titoli();
			return;
		}
		if(!empty($tit)){
			if(isset($tit->canali_pagamento)){
				foreach ($tit->canali_pagamento as $canale) {
					if($canale->COD_TIPO_CANALE=="BONIFICO"){
						$id_canale_pagamento = $canale->ID_CANALE;
						$canale->CAUSALE = $this->bonifico_build_causale($tit);
						$tit->canale_bonifico = $canale;
						break;
					}
				}
			}

			//echo json_encode($tit);die();
		}
		
		$page_data['scadenza']= $tit;

		$page_data['scadenze_simili']= $this->scadenze_su_stesso_canale_pag($id_canale_pagamento, $tit);
		
		$this->show("user_bonifico",$page_config,$page_data);
	}

	protected function bonifico_build_causale($tit){

		$causale = $tit->NUMERO_POLIZZA." ".$tit->NOMINATIVO;
		return $causale;
	}

	public function nexi($id_titolo=0)
	{
		$this->titolo($id_titolo);
	}
	protected function scadenze_su_stesso_canale_pag($id_canale , $tit){
		$scadenze = array();
		
		$p=array();
		$p['SHOW_CANALI_PAGAMENTO']=1;
		$scad_tmp = $this->find_scadenze($p);

		
		$all_config_obj = $this->datiSito['all_config_obj'];
		$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
		$vedi_premio_auto_rin = isset($hiPar->vedi_premio_auto_rin)?$hiPar->vedi_premio_auto_rin:0;  
		$vedi_premio_auto_rin = ($vedi_premio_auto_rin==1);
	
		foreach ($scad_tmp as $scad) {
			$isSimile = ($scad->ID_TITOLO != $tit->ID_TITOLO);
			if($isSimile){
				if( $scad->STATO_TITOLO==1 
				&& $scad->IS_SCADENZA_ANNUALITA==1 
				&& $scad->TIPO_RAMO=="A" 
				&& !$vedi_premio_auto_rin){
					//SCADENZA ANNUALE AUTO DOVE NON MOSTRO IMPORTI
					$isSimile = FALSE;
				}
			}
			if($isSimile){	
				$isSimile = FALSE;
				foreach ($scad->canali_pagamento as $canale) {
					if($canale->ID_CANALE==$id_canale){
						$isSimile = TRUE;
						break;
					}
				}
			}
			if($isSimile){
				$scadenze[]=$scad;
			}
		}
			
		return $scadenze;
	}
	
}
