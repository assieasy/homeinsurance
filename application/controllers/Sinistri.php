<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sinistri extends MY_Controller {
	protected $SMOD;
	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		if(!$this->controllo_privacy()){
			header('Location: /privacy');
		}
		$this->load->model("Sinistri_model");
		$this->SMOD = new Sinistri_model($this->assi);
	}
	public function index()
	{
		$this->sinistri();
	}


	public function sinistri()
	{
		$tipo_elenco = $this->get_tipo_elenco();

		$page_config=array(); 
		$page_config['titolo']="Sinistri";

		$page_data = array();

		$p=array();
		$p['limit']=50;//ULTIMI 50
		$p['sorts']=array();
		$p['sorts'][]=array("column"=>"DATA_SINISTRO","order"=>"DESC");
		$p['sorts'][]=array("column"=>"NUMERO_SINISTRO","order"=>"DESC");
		$elenco = $this->SMOD->get_sinistri($p);
		$page_data['sinistri']=  $elenco;
		if($tipo_elenco=="TABELLA"){
			$this->show("user_sinistri",$page_config,$page_data);
		}
		else{
			$this->show("user_sinistri_card",$page_config,$page_data);
		}
	}
	public function sinistro($id_sinistro=0)
	{	
		$id_sinistro = $id_sinistro>0?$id_sinistro:0;
		$id_sinistro = isset($_POST['ID_SINISTRO'])?$_POST['ID_SINISTRO']:$id_sinistro;
		$azione = isset($_POST['AZIONE'])?$_POST['AZIONE']:"";
		$errori_sinistro = array();
		switch($azione){
			case "SALVA":
				$ret = $this->write_sinistro();
				if($id_sinistro==0){
					$id_sinistro = $ret['ID_SINISTRO'];
				}
				if(isset($ret['errori'])){
					foreach($ret['errori'] as $key => $msg){
						$errori_sinistro[$key]=$msg;
					}
				}
			break;
			case "STAMPA":
				$this->stampa($id_sinistro);
				die();
			break;
		}

		
		$page_config=array();
		$page_config['titolo']="Sinistro";
		$page_config['js_files']=array();
		$page_config['js_files'][]="jquery.tabledit/jquery.tabledit.mod.js";
		$page_config['css_files']=array();
		
		$page_data = array();
		$page_data['const']= $this->SMOD->costanti();
		if($id_sinistro==0){
			$SIN= $this->SMOD->get_sinistro_vuoto();
			foreach($SIN  as $key => $val){
				if(isset($_POST[$key])){
					$SIN->{$key} = $_POST[$key]; 
				}
			}

		}
		else{
			$p=array();
			$p['ID_SINISTRO']= $id_sinistro;
			$p['SHOW_PARTITE']= 1;
			$p['SHOW_STORICO']= 1;
			$SIN= $this->SMOD->get_sinistro($p);
			$this->load->model("Documenti_model");
			$DOCMOD = new Documenti_model($this->assi);
			$p=array();
			$p['ID_SINISTRO']= $SIN->ID_SINISTRO;
			$docs = $DOCMOD->get_documenti($p);
			$SIN->documenti = $docs;
		}
		
		foreach($errori_sinistro as $key => $msg){
			$SIN->messages[$key]=$msg;
		}
		
		$page_data['sinistro']= $SIN;
		$this->show("user_sinistro",$page_config,$page_data);
	}

	protected function write_sinistro(){
		$sinistro = $this->post_to_sinistro();
		if(empty($sinistro["errori"])){
			$ret = $this->SMOD->put_sinistro($sinistro);
			$retData= array();
			if($ret->success){
				$retData = $ret->data;
				$sinistro['ID_SINISTRO'] = $retData->ID_SINISTRO;
				$this->write_sinistro_soggetti($sinistro);

				if(isset($_FILES["FILE_ALLEGATO"])){
					$this->upload_sinistro_allegato($sinistro,$_FILES["FILE_ALLEGATO"]);
				}
			}
		}
		return $sinistro;
	}
	protected function write_sinistro_soggetti($sinistro){
		$danneggiato = array();
		$genitore = array();
				
		$danneggiato['PROGRESSIVO_SOGGETTO'] = $sinistro['PROGRESSIVO_SOGGETTO_DANNEGGIATO'];
		$danneggiato['NOMINATIVO'] = $sinistro['NOMINATIVO_DANNEGGIATO'];
		$danneggiato['CODICEFISCALE'] = $sinistro['CODICEFISCALE_DANNEGGIATO'];
		$danneggiato['TEL_CASA'] = $sinistro['TEL_CASA_DANNEGGIATO'];
		$danneggiato['EMAIL'] = $sinistro['EMAIL_DANNEGGIATO'];
		switch($sinistro['QUALIFICA_DANNEGGIATO']){
			default:
				$danneggiato['RUOLO_ABBREVIAZIONE'] = $sinistro['QUALIFICA_DANNEGGIATO'];
				break;
		}
		
		if(	$sinistro['NOMINATIVO_GENITORE']!=""){
			$genitore['PROGRESSIVO_SOGGETTO'] = $sinistro['PROGRESSIVO_SOGGETTO_GENITORE'];
			$genitore['NOMINATIVO'] = $sinistro['NOMINATIVO_GENITORE'];
			$genitore['CODICEFISCALE'] = $sinistro['CODICEFISCALE_GENITORE'];
			$genitore['RUOLO_ABBREVIAZIONE'] = Sinistri_model::SIN_RUOLO_GENITORE;
		}
		if(!empty($genitore)){
			$genitore['TEL_CASA'] = $sinistro['TEL_CASA_GENITORE'];
			$genitore['EMAIL'] = $sinistro['EMAIL_GENITORE'];
		}

		$danneggiato['ID_SINISTRO']= $sinistro['ID_SINISTRO'];
		$danneggiato['NOTA'] = $danneggiato['NOMINATIVO']." Tel: ".$danneggiato['TEL_CASA']." Email: ".$danneggiato['EMAIL'];
		$ret = $this->SMOD->put_sinistro_soggetto($danneggiato);
		if(!empty($genitore)){
			$genitore['ID_SINISTRO']= $sinistro['ID_SINISTRO'];
			$genitore['NOTA'] = $genitore['NOMINATIVO']." Tel: ".$genitore['TEL_CASA']." Email: ".$genitore['EMAIL'];
		
			$ret = $this->SMOD->put_sinistro_soggetto($genitore);
		}

		return $sinistro;
	}
	
	protected function upload_sinistro_allegato($sinistro,$file_up){
		if(!empty($file_up)){
			if( is_uploaded_file($file_up['tmp_name'])){
				$f=array();
				$f['ID_SINISTRO']= $sinistro['ID_SINISTRO'];
				$f['FILE_NAME']= $file_up['name'];
				$f['FILE_CONTENT']=base64_encode(file_get_contents($file_up['tmp_name']));
				$f['DESC_TIPO_DOCUMENTO']= "SINISTRO DA ESTERNO";
				$f['TITOLO']= "ScuolaFututa - Documentazione sinistro";
				$ret = $this->SMOD->put_sinistro_allegato($f);
			}
		}

		return $sinistro;
	}

	protected function post_to_sinistro(){
		if(empty($_POST)) {
			return array();
		}
		$sinistro = array();
		foreach($_POST as $key=>$value){
			$sinistro[$key]=$value;
		}

		$sinistro["errori"]=array();
		$sinistro['ID_SINISTRO'] = isset($_POST['ID_SINISTRO'])?$_POST['ID_SINISTRO']:0;
		$sinistro['COD_GARANZIA_SINISTRO'] = isset($_POST['COD_GARANZIA_SINISTRO'])?$_POST['COD_GARANZIA_SINISTRO']: "";
		if($sinistro['COD_GARANZIA_SINISTRO']==""){
			$sinistro["errori"]["COD_GARANZIA_SINISTRO"]="Selezionale la tipologia di evento";
		}
		/*if($sinistro['COD_GARANZIA_SINISTRO']==""){
			$sinistro['COD_GARANZIA_SINISTRO'] = Sinistri_model::SIN_GAR_DANNO;
		}*/
		$sinistro['DATA_SINISTRO'] = isset($_POST['DATA_SINISTRO'])?$_POST['DATA_SINISTRO']:"";// date("d/m/Y");
		if($sinistro['DATA_SINISTRO']==""){
			$sinistro["errori"]["DATA_SINISTRO"]="Indicare la data dell'evento";
		}
		
		$sinistro['ID_POLIZZA'] = isset($_POST['ID_POLIZZA'])?$_POST['ID_POLIZZA']:0;
		if($sinistro['ID_POLIZZA']==0){
			$this->load->model("Polizze_model");
			$PMOD = new Polizze_model($this->assi);
			$polizz_vive= $PMOD->get_polizze_vive();
			if(!empty($polizz_vive)){
				$sinistro['ID_POLIZZA'] = $polizz_vive[0]->ID_POLIZZA;
			}
		}
		if($sinistro['ID_POLIZZA']==0){
			$sinistro["errori"]["POLIZZA"]="Nessuna polizza attiva per comunicazione evento";
		}
		$sinistro['LUOGO_ACCADIMENTO'] = isset($_POST['LUOGO_ACCADIMENTO'])?$_POST['LUOGO_ACCADIMENTO']: "";
		if($sinistro['LUOGO_ACCADIMENTO']==""){
			$sinistro["errori"]["LUOGO_ACCADIMENTO"]="Compilare il luogo dell'accadimento";
		}
		$sinistro['DINAMICA'] = isset($_POST['DINAMICA'])?$_POST['DINAMICA']: "";
		if($sinistro['DINAMICA']==""){
			$sinistro["errori"]["DINAMICA"]="Descrivere la dinamica dell'evento";
		}
		$sinistro['NOMINATIVO_DANNEGGIATO'] = isset($_POST['NOMINATIVO_DANNEGGIATO'])?$_POST['NOMINATIVO_DANNEGGIATO']: "";
		$sinistro['CODICEFISCALE_DANNEGGIATO'] = isset($_POST['CODICEFISCALE_DANNEGGIATO'])?$_POST['CODICEFISCALE_DANNEGGIATO']: "";
		if($sinistro['NOMINATIVO_DANNEGGIATO']=="" || $sinistro['CODICEFISCALE_DANNEGGIATO']=="" ){
			$sinistro["errori"]["DANNEGGIATO"]="Compilare i dati del danneggiato";
		}
		$sinistro['CODICEFISCALE_DANNEGGIATO']= strtoupper($sinistro['CODICEFISCALE_DANNEGGIATO']);
		if( !preg_match("/^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]$/i",$sinistro['CODICEFISCALE_DANNEGGIATO']) ){
			$sinistro["errori"]["DANNEGGIATO"]="Codice fiscale del danneggiato non corretto";
		}

		$sinistro['NOMINATIVO_GENITORE'] = isset($_POST['NOMINATIVO_GENITORE'])?$_POST['NOMINATIVO_GENITORE']: "";
		$genitore_presente = !($sinistro['NOMINATIVO_GENITORE']=="");
		if($genitore_presente){
			$sinistro['CODICEFISCALE_GENITORE'] = isset($_POST['CODICEFISCALE_GENITORE'])?$_POST['CODICEFISCALE_GENITORE']: "";
			$sinistro['CODICEFISCALE_GENITORE']= strtoupper($sinistro['CODICEFISCALE_GENITORE']);
			if(!preg_match("/^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]$/i",$sinistro['CODICEFISCALE_DANNEGGIATO']) ){
				$sinistro["errori"]["GENITORE"]="Codice fiscale del genitore non corretto";
			}
			$sinistro['TEL_CASA_GENITORE'] = isset($_POST['TEL_CASA_GENITORE'])?$_POST['TEL_CASA_GENITORE']: "";
			$sinistro['EMAIL_GENITORE'] = isset($_POST['EMAIL_GENITORE'])?$_POST['EMAIL_GENITORE']: "";
			if($sinistro['TEL_CASA_GENITORE']=="" && $sinistro['TEL_CASA_GENITORE']==""){
				$sinistro["errori"]["GENITORE"]="Inserire almeno un recapito del genitore";
			}
		}

		return $sinistro;
	}

	protected function stampa($id_sinistro=0){
		$SIN = array();
		$SIN['ID_SINISTRO'] = isset($_POST['ID_SINISTRO'])?$_POST['ID_SINISTRO']:$id_sinistro;
		$sFile = "";
		if($SIN['ID_SINISTRO']>0){
			$ret = $this->SMOD->stampa($SIN);
			if($ret){
				$sFile = base64_decode($ret->file_content);
			}
		}
		$out_file_name = "Sinistro.pdf";
		header("Content-type:application/pdf");
		header("Content-Disposition:attachment;filename=$out_file_name");
		echo $sFile;
		die();
	}

}
