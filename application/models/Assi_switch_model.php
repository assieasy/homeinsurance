<?php

class Assi_switch_model extends CI_Model
{
    
    function __construct($chiave_ricerca="")
    {
        parent::__construct();
    }

    function find_config_da_url($chiave_ricerca=""){
        $cliente = array();
        $id_switch = 0;
        if($chiave_ricerca!=""){
            $this->db->where("RSW.REG_ABILITATA",1);
            $this->db->order_by("RSW.ORDER");
            $this->db->from("REQUEST_SWITCHER RSW");
            $myQ = $this->db->get();
            $rr = $myQ->result_array();
            foreach($rr as $rec){
                $my_pattern=$rec['REGEXP'];
                if( preg_match('/^'.$my_pattern.'$/i',$chiave_ricerca)){
                    $id_switch = $rec['ID_SWITCH'];
                }
            }
        }
        
        $this->db->where("SW.ID_SWITCH",$id_switch);
        $this->db->select("CLI.*");
        $this->db->select("SW.SITE_URL");
        $this->db->from("REQUEST_SWITCHER SW");
        $this->db->join("CLIENTI CLI","CLI.ID_CLIENTE=SW.ID_CLIENTE","INNER");
        $myQ = $this->db->get();
        $cliente = $myQ->row_array();
            
        return $cliente ;
    }
    
    
}
