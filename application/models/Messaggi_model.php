<?php

require_once "Base_rest_model.php";

class Messaggi_model extends Base_rest_model
{
    const TIPO_MSG_COMUNICAZIONE = array("DESCRIZIONE_CALENDARIO"=>"MESSAGGIO WEB CLIENTE",
                                        "ABBREVIAZIONE_CALENDARIO"=>"MWC");
    const TIPO_MSG_VARIAZIONE = array("DESCRIZIONE_CALENDARIO"=>"CLIENTE WEB VARIAZIONE ANAGRAFE",
                                        "ABBREVIAZIONE_CALENDARIO"=>"VWC");

    
    public function get_messaggi($params=array()){
        $ret = $this->CH->get_messaggi($params);
		$elenco = $this->CH->extract_data($ret); 
        return $elenco;
    }
    
    /**
     * crea in assieasy un'agenda di tipo TIPO_MSG_COMUNICAZIONE
     */
    public function put_messaggio($params=array()){
        foreach(self::TIPO_MSG_COMUNICAZIONE as $key=>$val){
            $params[$key] = isset($params[$key] )?$params[$key] :$val;
        }
        return $this->send_messaggio($params);
    }
    
    /**
     * crea in assieasy un'agenda di tipo TIPO_MSG_VARIAZIONE
     */
    public function put_variazione($params=array()){
        foreach(self::TIPO_MSG_VARIAZIONE as $key=>$val){
            $params[$key] = isset($params[$key] )?$params[$key] :$val;
        }
        return $this->send_messaggio($params);
    }


    protected function send_messaggio($params=array()){
        $tmp = $this->get_allegati($params);
        $params['attachments']=$tmp;
        $ret = $this->CH->put_messaggio($params);
     	return $ret;
    }

    
    
}
