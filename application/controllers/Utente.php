<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utente extends MY_Controller {
	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		
	}
	public function index()
	{
		$this->show("home_page");
	}
	public function main()
	{
		$page_config=array();
		$page_config['titolo']="Utente";

		$page_data = array();
		$ret = $this->assi->get_polizze();
		$page_data['polizze']=  $this->assi->extract_data($ret);
		$this->show("user_main",$page_config,$page_data);
	}
	public function anagrafica()
	{
		$page_config=array();
		$page_config['titolo']="Utente - Anagrafica";

		$page_data = array();
		$ret = $this->assi->get_anagrafica();
		
		$page_data['anagrafica']=  $this->assi->extract_first_data($ret);
		$this->show("user_anagrafica",$page_config,$page_data);
	}
	public function account()
	{
		$this->load->model("Tabelle_model");
		$TABMOD = new Tabelle_model($this->assi); 
		$page_config=array();
		$page_config['titolo']="Utente - Account";
		//$page_config['js_files']=array("login.js");
		$page_data = array();
		$ret = $this->assi->get_anagrafica();
		
		$ANA =  $this->assi->extract_first_data($ret);
		$page_data['anagrafica']= $ANA;
		if($page_data['anagrafica']->TIPOPERSONA =="PF"){
			$professioni = $TABMOD->get_professioni();
			$page_data['professioni'] = $professioni;
		}
		if($page_data['anagrafica']->TIPOPERSONA =="PG"){
			$tipi_societa = $TABMOD->get_tipi_societa();
			$page_data['tipi_societa'] = $tipi_societa;
		}

		$this->load->model("Documenti_model");
		$DOCMOD = new Documenti_model($this->assi);
		
		//DOCUMENTI
		$p=array();
		$p['ID_ANAGRAFICA']= $ANA->ID_ANAGRAFICA;
		$p['PROVENIENZA']= "ANA";
		$docs = $DOCMOD->get_documenti($p);
		$page_data['documenti_anagrafica'] = $docs;
		$this->show("user_account",$page_config,$page_data);
	}
	
	protected function build_valori_variazioni($dati_in){
		$this->load->model("Tabelle_model");
		$TABMOD = new Tabelle_model($this->assi); 
		
		$dati=array(); 
		$campi =array();
		$campi[]= array("name"=>"TEL_CASA","caption"=>"Telefono Casa","value"=>"","stringvalue"=>"");
		$campi[]= array("name"=>"TEL_CELLULARE","caption"=>"Cellulare","value"=>"","stringvalue"=>"");
		$campi[]= array("name"=>"PROFESSIONE_SIS","caption"=>"Professione","value"=>"","stringvalue"=>"");
		$campi[]= array("name"=>"TIPO_SOCIETA","caption"=>"Tipo Societa'","value"=>"","stringvalue"=>"");
		$variazioni = array();//PER MESSAGGIO TESTUALE
		foreach($campi as $campo){
			$val = isset($dati_in[$campo["name"]])?$dati_in[$campo["name"]]:"";
			if($val!=""){
				$sis_tabella_decode=0;
				switch($campo["name"]){
					case "PROFESSIONE_SIS":
						$sis_tabella_decode = Tabelle_model::SIS_TAB_PROFESSIONI;
						if($val>0){
							$campo["value"]=$val;
						}
						break;
					case "TIPO_SOCIETA":
						$sis_tabella_decode = Tabelle_model::SIS_TAB_TIPO_SOCIETA;
						if($val>0){
							$campo["value"]=$val;
						}
						break;
					default:
						$campo["value"]=$val;
						$campo["stringvalue"]=$val;
						break;
				}

				if($sis_tabella_decode>0){
					if($campo["value"]>0){
						$p=array();
						$p['TABELLA']=$sis_tabella_decode;
						$p['ELEMENTO']=$campo["value"];
						$ELEM = $TABMOD->get_elemento_sistema($p);
						if(!empty($ELEM)){
							$campo["stringvalue"]=$ELEM->DESCRIZIONE;
						}
					}
				}

				if($campo["value"]!=""){
					$variazioni[]=$campo;
				}
			}
		}
		return $variazioni;
	}
	public function registra_variazione(){
						
		$variazioni = $this->build_valori_variazioni($_POST);
		
		if(empty($variazioni)){
			$retData = array();
			$retData['success']=false;
			$retData['message']="Non è stata comunicata nessuna variazione significativa!";
		}
		else{

			//AGGIORNO ANAGRAFICA (SENZA OVERWRITE)
			$this->load->model("Anagrafica_model");
			$AMOD = new Anagrafica_model($this->assi); 
			$p=array();
			foreach($variazioni as $key=>$campo){
				$p[$campo['name']]=$campo['value'];
			}
			$AMOD->update_anagrafica_hi($p);
			
			
			//MESSAGGIO VARIAZIONI
			$messaggio = array();
			$messaggio["TITOLO"]="Variazione dati anagrafici Cliente";

			$sTMP="Il cliente ".$this->utente->NOMINATIVO;
			if(count($variazioni)>1){
				$sTMP.=" comunica le seguenti variazioni anagrafiche:";
			}
			else{
				$sTMP.=" comunica la seguente variazione anagrafiche:";
			}
			foreach($variazioni as $key=>$campo){
				$sTMP.= PHP_EOL.$campo["caption"]." : ".$campo["stringvalue"];
			}

			$messaggio["MESSAGGIO"]=$sTMP;
			
			$this->load->model("Messaggi_model");
			$MSGMOD = new Messaggi_model($this->assi);
			$ret = $MSGMOD->put_variazione($messaggio);
			
			if( (isset($ret->success)?$ret->success:FALSE) ){
				$retData = array();
				$retData['success']=true;
				$retData['message']=$sTMP;
				$retData['data']=$ret->data;
				$retData['message']="La richiesta è stata presa in carico!";
			}
			else{
				$sTMP = isset($ret->error)?$ret->error:"Errore nella consegna del messaggio";
				$retData = array();
				$retData['success']=false;
				$retData['message']=$sTMP;
			}
			
			
		}
		
		
		echo json_encode($retData);
	}

	/**
	 * Riceve old_password , new_password e new_password_repeat in post.
	 * Richiede ad assieasy di aggiornare la password
	 *
	 * @var	array 
	 * @return	json
	 */
	public function updatepassword(){
		$dati=array(); 
		$dati['new_password']=$_POST['new_password'];
		$dati['new_password_repeat']=$_POST['new_password_repeat'];
		$dati['old_password']=$_POST['old_password'];
		if($dati['new_password']!=$dati['new_password_repeat']){
			$ret = array();
			$ret['success']=false;
			$ret['message']="[Nuova password] non coincide con [Ripeti Nuova password]";
		
		}
		else{
			$ret = $this->assi->updatepassword($dati);
		}
		
		echo json_encode($ret);
	}

}
