<? 
/*
mi aspetto $config:
	- border_class ="border-xxxx";
    - fa_icon ="icona font wasome";
    - titolo = titolo da scrivere

*/
$conf = isset($config)?$config:array();
if(isset($conf)){
?>
	<div class="row pt-2">
		<div class="col">
			<div class="card card-title border border-3 border-dettaglio <?=isset($conf["border_class"])?$conf["border_class"]:""?>" >
				<div class="card-text p-1">
					<div class="row">
						<div class="col">
							<h5 class="main-font">
								<?
								if((isset($conf["fa_icon"])?$conf["fa_icon"]:"")!=""){
									?>
									<i class="fa fa-<?=$conf["fa_icon"]?> fa-2x icona mr-2" aria-hidden="true"></i>
									<?
								}
								?>
								<?=isset($conf["titolo"])?$conf["titolo"]:""?>
							</h5>
						</div>
						<div class="ml-auto mr-4">
							<h5 class="main-font">
								<a class="align-self-end btn-loading" href="/"><i class="fa fa-home fa-2x icona" aria-hidden="true"></i></a>
							</h5>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
<?}?>