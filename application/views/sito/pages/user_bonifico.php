    <? 
    $all_config_obj = $sito['all_config_obj'];
    $hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
    $vedi_premio_auto_rin = isset($hiPar->vedi_premio_auto_rin)?$hiPar->vedi_premio_auto_rin:0;  
    $vedi_premio_auto_rin = ($vedi_premio_auto_rin==1);

                                    
    
    $TIT = isset($page_data['scadenza'])?$page_data['scadenza']:array();
    $SCADENZE_SIMILI = isset($page_data['scadenze_simili'])?$page_data['scadenze_simili']:array();
    $nascondi_premio = FALSE;
    if( $TIT->STATO_TITOLO==1 
            && $TIT->IS_SCADENZA_ANNUALITA==1 
            && $TIT->TIPO_RAMO=="A" 
            && !$vedi_premio_auto_rin){
        $nascondi_premio = TRUE;
    }
    //$POLIZZA = isset($TIT->polizza)?$TIT->polizza:array();
    $dtScadenza = new DateTime($TIT->DATA_EFFETTO);
    $dtEff = new DateTime($TIT->DATA_EFFETTO);

    $sLinkPolizza = '<a class="btn-loading" href="/polizze/polizza/'.$TIT->ID_POLIZZA.'"><u>'.$TIT->NUMERO_POLIZZA.'</u></a>';
    $show_contraente = false;
    
    ?>
<div class="container-fluid maincontainer pb-4">
    <?
    if($TIT->STATO_TITOLO==1){
       $link_testata =  '<a class="btn-loading" href="/titoli/scadenze">Scadenze</a> > Scadenza del '.
        '<a class="btn-loading" href="/titoli/titolo/'.$TIT->ID_TITOLO.'"><u>'.$dtEff->format('d/m/Y').'</u></a> > Polizza '.$sLinkPolizza;
    }
    else{
        $link_testata =  '<a class="btn-loading" href="/titoli/pagamenti">Scadenze</a> > Scadenza del '.
        '<a class="btn-loading" href="/titoli/titolo/'.$TIT->ID_TITOLO.'"><u>'.$dtEff->format('d/m/Y').'</u></a> > Polizza '.$sLinkPolizza;
    }

    $config=array(
        "border_class"=>"border-scadenze",
        "fa_icon"=>"calendar-check",
        "titolo"=>$link_testata 
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>

    <?if($TIT->STATO_TITOLO==1 && in_array("scadenze.paga",$funzioni_abilitate) && !$nascondi_premio) { 
        $BONIFICO = isset($TIT->canale_bonifico)?$TIT->canale_bonifico:"";
        if($BONIFICO!=""){?>
            <div class="row py-2">
                <div class="col">
                    <!-- CARD BONIFICO -->
                    <div class="card card-dettaglio " >
                        <div class="card-header">
                            <h5><i class="fa fa-university mr-2" aria-hidden="true"></i>Paga con un Bonifico</h5>
                        </div>
                        <div class="card-body justify-content-center">
                            <dl class="row">
                            <? foreach ($BONIFICO->parametri as $key => $p) {
                                if($p->VALORE!=""){
                                ?>
                                    <dt class="col-3" ><?=$p->DESCRIZIONE?></dt>
                                    <dd class="col-9" >
                                        <? if($p->CHIAVE_PARAMETRO=="IBAN"){ ?>
                                            <span class="copy-target copy-remove-spaces"><?=$p->VALORE?></span>
                                            <button class="btn copy-btn ml-2" title="Copia"><i class="fa fa-copy mr-1" ></i></button>
                                        <?}
                                        else {?>
                                            <span><?=$p->VALORE?></span>
                                        <?}?>
                                        
                                    </dd>
                            <?  } 
                            }?>
                            <?  $causale = $BONIFICO->CAUSALE;
                                $importo = "&euro; ".number_format($TIT->PREMIO_TOTALE,2,",","");
                            ?>
                                <dt class="col-3">Causale</dt>
                                <dd class="col-9"><span class="copy-target"><?=$causale?></span>
                                    <button class="btn copy-btn ml-2" title="Copia"><i class="fa fa-copy mr-1"></i></button>
                                </dd>
                                <dt class="col-3">Importo</dt>
                                <dd class="col-9"><?=$importo?></dd>
                            </dl>
                        </div>
                        
                    </div>
                    <!-- CARD BONIFICO FINE-->
                </div>
            </div>
    <?  } 
    }?>

    <?if(!empty($SCADENZE_SIMILI )){?>
        <div class="row">
        <div class="col">
            <!-- CARD SCADENZE_SIMILI -->
            <div class="card card-dettaglio " >
                <div class="card-header">
                    <h5><i class="fa fa-calendar-check mr-2" aria-hidden="true"></i>Scadenze Simili</h5>
                </div>
                <div>
                    <!--   ************* -->
                    <?  $aCapo = true;
                    $contaRecord=0;
                    $quanti_per_riga = 2;
                    $i = count($SCADENZE_SIMILI);
                    if($i<$quanti_per_riga){
                        $quanti_per_riga = $i;
                    }
                    /*if($i % 4 == 0){
                        $quanti_per_riga = 4;
                    }*/
                    foreach($SCADENZE_SIMILI as $key => $SCAD) {
                        if($aCapo){
                            if($contaRecord>0){
                                //chiusura div row precedente
                                ?>
                                </div><!-- chiusura div row precedente -->
                                <?
                            }
                            ?>
                                <div class="row"> <!--div row -->
                            <?
                        }
                        $contaRecord++;
                        $aCapo = (($contaRecord % $quanti_per_riga)==0);

                        $dtEff = new DateTime($SCAD->DATA_EFFETTO);
                        ?>
                        <div class="col-md-<?=(12/$quanti_per_riga)?> d-flex align-items-stretch pt-2">
                            <div class="card card-dettaglio m-2" >
                                <!-- <div class="card-header">
                                    <a class="btn-loading align-self-end pr-3" href="/titoli/titolo/<?=$SCAD->ID_TITOLO?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <strong>
                                        Scadenza del <a class="btn-loading" href="/titoli/titolo/<?=$SCAD->ID_TITOLO?>"><u><?=$dtEff->format('d/m/Y')?></u></a> 
                                        su polizza N. 
                                        <a class="btn-loading" href="/polizze/polizza/<?=$SCAD->ID_POLIZZA?>"><u><?=$SCAD->NUMERO_POLIZZA?></u></a>
                                    </strong>
                                
                                </div> -->
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div class="col-1" >
                                            <a class="btn-loading align-self-end pr-3" href="/titoli/titolo/<?=$SCAD->ID_TITOLO?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="col-10">
                                            <strong>
                                                Scadenza del <a class="btn-loading" href="/titoli/titolo/<?=$SCAD->ID_TITOLO?>"><u><?=$dtEff->format('d/m/Y')?></u></a> 
                                                su polizza N. 
                                                <a class="btn-loading" href="/polizze/polizza/<?=$SCAD->ID_POLIZZA?>"><u><?=$SCAD->NUMERO_POLIZZA?></u></a>
                                                <br>per un importo di &euro; <?=number_format($SCAD->PREMIO_TOTALE,2,",",".")?>
                                            </strong>
                                        </div>
                                        <!--
                                            <div class="col-1">
                                            <button class="btn btn-carrello" title="Aggiungi al pagamento"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                                        </div>
                                        -->
                                    </div>
                                    <div class="d-flex">
                                        <dl class="row small pt-1">
                                        <?
                                            $valori=array();
                                            $valori[]=array("label"=>"Contraente","valore"=>$SCAD->NOMINATIVO);
                                            $valori[]=array("label"=>"Compagnia","valore"=>$SCAD->DESC_COMPAGNIA);
                                            //$valori[]=array("label"=>"Numero Polizza","valore"=>$SCAD->NUMERO_POLIZZA);
                                            if($SCAD->TARGA!=""){
                                                $valori[]=array("label"=>"Targa","valore"=>$SCAD->TARGA);
                                            }
                                            //$valori[]=array("label"=>"Data Scadenza","valore"=>$dtEff->format('d/m/Y'));
                                            foreach($valori as $valore){
                                                ?>
                                                <dt class="col-4"><?=$valore["label"]?>:</dt>
                                                <dd class="col-8"><?=$valore["valore"]?></dd>
                                                <?
                                            }
                                        ?>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div> 
                                    
                    <?  }
                    
                    
                    if($contaRecord>0){
                        //chiusura div row precedente
                        ?>
                        </div>
                        <?
                    }?>
                    <!--   ************* -->
                </div>
            </div>
        </div>
        </div>
    <?}?>
</div>


<script>
pageinitfunctions = pageinitfunctions?pageinitfunctions:[];
pageinitfunctions.push({name:'abilita_copia'});


function abilita_copia(){
    $('.copy-btn').on("click", function(){
        $tmp = $(this).siblings(".copy-target").first();
        var text = $tmp.text();
        if( $tmp.is( ".copy-remove-spaces" )){
            text = text.replace(/\s+/g, '');
        }

        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(text).select();
        document.execCommand("copy");
        $temp.remove();
    });
}

function abilita_carrello(){
    $('.btn-carrello').on("click", function(){
        console.log($(this));
    });
}

var carrello_scadenze =[];

var scadenza = {
    "id_titolo": <?=$TIT->ID_TITOLO?>,
    "premio": <?=$TIT->PREMIO_TOTALE?>,
    "numero_polizza": "<?=$TIT->NUMERO_POLIZZA?>",
    "contraente": "<?=$TIT->NOMINATIVO?>"
};

carrello_scadenze.push(scadenza);
console.log(carrello_scadenze);


</script>