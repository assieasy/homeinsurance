<? 
    $SINISTRO = isset($page_data['sinistro'])?$page_data['sinistro']:array();
    $dtSinistro = new DateTime($SINISTRO->DATA_SINISTRO);
    $dtDenuncia = new DateTime($SINISTRO->DATA_DENUNCIA);
    $show_importi = in_array("sinistri.liquidato",$funzioni_abilitate);
    if($show_importi){
        //PER IL RAMO AUTO NON VISUALIZZO IMPORTO LIQUIDATO
        $show_importi = ($SINISTRO->TIPO_RAMO!="A");
    }
    ?>
<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-sinistri",
        "fa_icon"=>"user-injured",
        "titolo"=>'<a class="btn-loading" href="/sinistri/sinistri">Sinistri</a> > Sinistro '.$SINISTRO->NUMERO_SINISTRO.' > Polizza <a class="btn-loading" href="/polizze/polizza/'.$SINISTRO->ID_POLIZZA.'">'.$SINISTRO->NUMERO_POLIZZA.'</a>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
    <div class="row pt-3">
        <div class="col-md-6 py-2">
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD SINISTRO -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-user-injured mr-1" aria-hidden="true"></i> Sinistro</h5>
                        </div>
                        <div class="card-body">
                            <dl class="">
                                <? $valori=array();
                                $valori[]=array("label"=>"Numero Sinistro","valore"=>$SINISTRO->NUMERO_SINISTRO);
                                $valori[]=array("label"=>"Numero Polizza","valore"=>$SINISTRO->NUMERO_POLIZZA);
                                $valori[]=array("label"=>"Garanzia Colpita","valore"=>$SINISTRO->DESC_GARANZIA_SINISTRO);
                                $valori[]=array("label"=>"Data Sinistro","valore"=>$dtSinistro->format('d/m/Y'));
                                $valori[]=array("label"=>"Data Denuncia","valore"=>$dtDenuncia->format('d/m/Y'));
                                $valori[]=array("label"=>"Stato","valore"=>$SINISTRO->STATO_SINISTRO_DESC);
                                if($show_importi){
                                    $tmp = isset($SINISTRO->IMPORTO_LIQUIDATO)?$SINISTRO->IMPORTO_LIQUIDATO:0;
                                    if($tmp >0){
                                        $sTMP = "&euro; ".number_format($tmp,2,",",".");
                                        $valori[]=array("label"=>"Importo Liquidato","valore"=>$sTMP);
                                    }
                                }
                                foreach($valori as $valore){
                                    ?>
                                    <dt ><?=$valore["label"]?></dt>
                                    <dd ><?=$valore["valore"]?></dd>
                                    <?
                                }
                                ?>
                            </dl> 
                        </div>
                    </div>
                    <!-- CARD SINISTRO FINE-->
                </div>
            </div>
            <div class="row py-2">
                <div class="col-12">
                    
                </div>
            </div>
        </div>

        <div class="col-md-6 py-2">
             <?if(in_array("messaggi",$funzioni_abilitate) ){ ?>
            <div class="row py-2">
                <div class="col-12">
                    <div class="card card-dettaglio border-0 m-0 p-0" >
                            <div class="card-body justify-content-center text-center">
                            <h5><a href="/messaggi/sinistro/<?=$SINISTRO->ID_SINISTRO?>" class="btn btn-primary btn-loading mt-1" ><i class="fa fa-comment-dots mr-1" aria-hidden="true"></i>Scrivi un messaggio / Invia Documento</a></h5>
                            </div>
                    </div>
                </div>
            </div>
            <?}?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD CONTRAENTE -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-user mr-1" aria-hidden="true"></i> Contraente Polizza</h5>
                        </div>
                        <div class="card-body"> 
                            <dl class="">
                                <? $valori=array();
                                $valori[]=array("label"=>"Nominativo","valore"=>$SINISTRO->NOMINATIVO);
                                $valori[]=array("label"=>"Codice Fiscale","valore"=>$SINISTRO->CODICEFISCALE);
                                $valori[]=array("label"=>"Indirizzo","valore"=>$SINISTRO->INDIRIZZO);
                                $valori[]=array("label"=>"Comune","valore"=>$SINISTRO->COMUNE);
                                $valori[]=array("label"=>"Cap","valore"=>$SINISTRO->CAP);
                                foreach($valori as $valore){
                                    ?>
                                    <dt><?=$valore["label"]?></dt>
                                    <dd><?=$valore["valore"]?></dd>
                                    <?
                                }
                                ?>
                            </dl> 
                        </div>
                    </div>
                    <!-- CARD CONTRAENTE FINE-->
                </div>
            </div>
            <div class="row py-2">
                <div class="col-12">
                <?if(!empty($SINISTRO->documenti)) {?>
                    <div class="row py-2">
                        <div class="col-12">
                            <!-- CARD DOCUMENTI -->
                            <div class="card card-dettaglio " >
                                <div class="card-header">
                                <h5><i class="fa fa-file-alt mr-1" aria-hidden="true"></i> Archivio Documenti</h5>
                                </div>
                                <div class="card-body">
                                    <ul>
                                        <?  foreach($SINISTRO->documenti as $doc) {?>
                                                <li>
                                                    <?=$doc->DESC_TIPO_DOCUMENTO?>:
                                                    <a target="_blank" href="<?=$sito['ASSI_URL_PUBBLICO']."/".$doc->URL_DOCUMENTO."/download"?>" >
                                                    <i class="fa fa-<?=$this->load->view("sito/common/icona_file_fa",array("filename"=>$doc->NOME_FILE),TRUE);?> fa-2x"></i>
                                                    <?=($doc->TITOLO!=""?$doc->TITOLO:$doc->NOME_FILE)?>
                                                    </a>
                                                </li>
                                        <?  }?>
                                    </ul> 
                                </div>
                            </div>
                            <!-- CARD DOCUMENTI FINE-->
                        </div>
                    </div>
                    <?}?>
                </div>
            </div>
        </div>
    </div>    
</div>
