<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-polizze",
        "fa_icon"=>"id-card",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > Polizze</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);

    $gestiscoAnnullate = in_array("polizze.annullate",$funzioni_abilitate);
    $vedi_annullate = isset($page_data['vedi_annullate'])?$page_data['vedi_annullate']:0;
    ?>
    <form id="form-filtri" name="form-filtri" method="POST">
    <div class="row pt-1">
        <?if($gestiscoAnnullate) {?>
        <div class="px-5">
            <div class="form-group border p-2">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" 
                        name="chkSOLO_POLIZZE_VIVE" id="chkSOLO_POLIZZE_VIVE"
                        <?=($vedi_annullate>0?"":"checked")?>
                        >
                    <label class="form-check-label" for="chkSOLO_POLIZZE_VIVE">
                        Solo Contratti Attivi
                    </label>
                </div>
            </div>
            <input type="hidden" value="<?=$vedi_annullate?>" name="VEDI_POLIZZE_ANNULLATE" id="VEDI_POLIZZE_ANNULLATE" >
                
        </div>
        <?}?>      
        <div class="ml-auto">
            <div class="btn-group card-dettaglio mr-4" role="toolbar"  aria-label="switch-visione">
                <a role="button" class="btn disabled btn-loading" href="/polizze/polizze?TIPO_ELENCO=TABELLA"><i class="fa fa-list" aria-hidden="true"></i></a>
                <a role="button" class="btn btn-loading" href="/polizze/polizze?TIPO_ELENCO=CARD"><i class="fa fa-table" aria-hidden="true"></i></a>
             </div>
        </div>
    </div>
    </form>
    <div class="row p-4">
        <div class="table-responsive col-12">
            <table class="table table-sm table-hover small">
            <thead>
                <tr>
                <th scope="col">Polizza</th>
                <th scope="col">Compagnia</th>
                <th scope="col">Prodotto</th>
                <th scope="col">Effetto</th>
                <th scope="col">Scadenza</th>
                <th scope="col">Frazionamento</th>
                <th scope="col">Note</th>
                <!-- <th scope="col">Documenti</th> -->
                <? if($gestiscoAnnullate){ ?>
                    <th scope="col">Stato</th> 
                <? } ?>
                </tr>
            </thead>
            <tbody>
            <?  foreach($page_data['polizze'] as $key => $pol) {
                    $dtScadenza = new DateTime($pol->DATA_SCADENZA_CONTRATTO);
                    $dtEff = new DateTime($pol->DATA_EFFETTO_ORI);
                    ?>
                    <tr>
                    <td nowrap><a href="/polizze/polizza/<?=$pol->ID_POLIZZA?>" class="btn-loading"><i class="fa fa-id-card mr-1" aria-hidden="true"></i><?=$pol->NUMERO_POLIZZA?></a></td>
                    <td><?=$pol->DESC_COMPAGNIA?></td>
                    <td><?=$pol->DESC_PRODOTTO?></td>
                    <td><?=$dtEff->format('d/m/Y')?></td>
                    <td><?=$dtScadenza->format('d/m/Y')?></td>
                    <td><?=$pol->FRAZIONAMENTO?></td>
                    <td><?
                        $sTMP = "";
                        if($sTMP==""){
                            $sTMP = $pol->TARGA;
                            $sTMP = ($sTMP==""?"":"Targa: ".$sTMP);
                        } 
                        if($sTMP==""){
                            $sTMP = isset($pol->BENEFICIARIO_CAUZIONI)?$pol->BENEFICIARIO_CAUZIONI:"";
                            $sTMP = ($sTMP==""?"":"Beneficiario: ".$sTMP);
                        }
                        echo $sTMP;
                    ?></td>
                    <? if($gestiscoAnnullate){ ?>
                        <td><?=$pol->DESC_STATO_POLIZZA?></td>
                    <? } ?>
                    </tr>
                    
            <?  }?>
            </tbody>
            </table>
        </div>
    </div>
</div>


<script>
/*************** funzioni init ***********/
pageinitfunctions = pageinitfunctions?pageinitfunctions:[];
/*************** funzioni init ***********/
pageinitfunctions.push({name:'soloViveChange'});
function soloViveChange(){
   
    $('#chkSOLO_POLIZZE_VIVE').change(function() {
        console.log("fff");
        $('#chkSOLO_POLIZZE_VIVE').val($(this).is(':checked'));
        $('#VEDI_POLIZZE_ANNULLATE').val($(this).is(':checked')?0:1);
        var frm = '#form-filtri';
        $(frm).attr("action","/polizze/polizze");
        $(frm).attr("target","");
        start_spinner();
        $(frm).submit();
        return false;
    });
}

</script>

