<? 
$all_config_obj = $sito['all_config_obj'];
$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
$ANAG_SITO = isset($hiPar->anagrafica_sito)?$hiPar->anagrafica_sito:array(); 
?>
<footer class="bg-light">
    <div class="container-fluid footer-container py-2">
    <?
    if(!empty($ANAG_SITO)){
    ?>
        <div class="row">
            <div class="col-md-6">
                <div class="small text-center text-muted">
                <?=$ANAG_SITO->NOMINATIVO?><br>
                <? 
                    $sTMP = $ANAG_SITO->INDIRIZZO;
                    $sTMP .= ", ".$ANAG_SITO->CAP;
                    $sTMP .= " ".$ANAG_SITO->COMUNE;
                    $sTMP .= " (".$ANAG_SITO->PROVINCIA.")";
                    echo $sTMP;
                ?>
                <br>
                
                <?
                $valori=array();
                $valori[]=array("label"=>"Tel","valore"=>$ANAG_SITO->TEL_CASA);
                $valori[]=array("label"=>"Fax","valore"=>$ANAG_SITO->FAX);
                $valori[]=array("label"=>"mail","valore"=>$ANAG_SITO->EMAIL);
                $valori[]=array("label"=>"PEC","valore"=>$ANAG_SITO->EMAIL_PEC);
                foreach($valori as $valore){    
                    if($valore["valore"]!=""){
                        echo $valore["label"].": ".$valore["valore"]."<br>";
                    }                
                }
                ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="small text-center text-muted">
                    PIVA: <?=$ANAG_SITO->CODICEFISCALE?><br>
                    <? 
                    $sRui = isset($hiPar->rui_sito)?$hiPar->rui_sito:"";
                    if($sRui!=""){
                        echo "Iscrizione al RUI nr: $sRui.<br>";
                        echo $ANAG_SITO->NOMINATIVO . " è sottoposta al controllo dell’IVASS.";
                    }
                    ?>
                    
                </div>
            </div>
        </div>
    <?}?>
    
        <div class="row">
            <div class="col-md-12 small text-center text-muted">
                <? 
                    $links = "";
                    $sTMP= isset($hiPar->link_cookie)?$hiPar->link_cookie:"";
                    $target_cookies = "target='_blank'";
                    if($sTMP==""){
                        $sTMP="/welcome/cookies";
                        $target_cookies="";
                    }
                    if($sTMP){
                        $links.= ($links=="")?"":" - ";
                        $links.="<a href='$sTMP' $target_cookies >Cookie Policy</a>";
                    }
                    $sTMP= isset($hiPar->link_privacy)?$hiPar->link_privacy:"";
                    if($sTMP){
                        $links.= ($links=="")?"":" - ";
                        $links.=" <a href='$sTMP' target='_blank' >Privacy Policy</a>";
                    }
                    $sTMP= isset($hiPar->link_reclami)?$hiPar->link_reclami:"";
                    if($sTMP){
                        $links.= ($links=="")?"":" - ";
                        $links.=" <a href='$sTMP' target='_blank' >Reclami</a>";
                    }

                    if(in_array("contatti.preview",$funzioni_abilitate)){ 
                        $sTMP="/utils/contatti_preview";
                         if($sTMP){
                            $links.= ($links=="")?"":" - ";
                            $links.=" <a href='$sTMP' >Suggerimenti Estetici</a>";
                        }
                    }
                    echo $links;
                ?>
            </div>
        </div>
    </div>
</footer>