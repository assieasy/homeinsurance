<? 
?>

<div class="container-fluid maincontainer pb-4">
    <div class="row pt-5">
        <div class="col-md-12">
            <div class="card border border-3 border-primary" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-11">
                            <h5 class="main-font">
                            <i class="fa fa-book fa-2x" aria-hidden="true"></i> Istruzioni e procedure da seguire</h5>
                        </div>
                        <div class="col-md-1 align-self-end">
                            <h5 class="main-font">
                                <a class="align-self-end" href="/"><i class="fa fa-home fa-2x" aria-hidden="true"></i></a>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row pt-3">
        <div class="col-md-6">
            <p>PROCEDURA REGOLAZIONE DEL PREMIO</p>
        </div>
        <div class="col-md-2">
            <a target="_new" href="/sito/assets/guide/ISTRUZIONI PER LA COMUNICAZIONE DI EVENTI DANNOSI.docx" class="btn btn-primary">Scarica</a>
        </div>
    </div>
    <div class="row pt-3">
        <div class="col-md-6">
            <p>PROCEDURA COMUNICAZIONE INFORTUNI/EVENTI DANNOSI</p>
        </div>
        <div class="col-md-2">
            <a target="_new" href="/sito/assets/guide/ISTRUZIONI PER LA COMUNICAZIONE DI EVENTI DANNOSI.docx" class="btn btn-primary">Scarica</a>
        </div>
    </div>
    <div class="row pt-3">
        <div class="col-md-6">
            <p>PROCEDURA PAGAMENTO DEL PREMIO</p>
        </div>
        <div class="col-md-2">
            <a target="_new" href="/sito/assets/guide/ISTRUZIONI PER LA RICHIESTA DI RISARCIMENTO.docx" class="btn btn-primary">Scarica</a>
        </div>
    </div>
</div>
