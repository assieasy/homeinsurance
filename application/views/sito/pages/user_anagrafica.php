
<header class="userhead">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center text-center">
            <div class="col-lg-10 align-self-start">
                <h1 class="text-uppercase text-white font-weight-bold"> <? echo $utente->NOMINATIVO ?></h1>
                <hr class="divider my-4" />
                
                <? 
                
                foreach($page_data['anagrafica'] as $key=>$value){
                    if(!is_object($value) && !is_array($value)){
                        $sTMP = "";
                        $sTMP .= '<div class="mt-5">';
                        $sTMP .= '        <h3 class="h4 mb-2">'.$key.'</h3>';
                        $sTMP .= '        <p class="text-muted mb-0">'.$value.'</p>';
                        $sTMP .= '</div>';
                        echo $sTMP;
                    }
                }
                ?>
                
            </div>
        </div>
    </div>
</header>
<section>
    
</section>
        