<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-sinistri",
        "fa_icon"=>"user-injured",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > Sinistri</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>

    <div class="row pt-1">
        <div class="ml-auto">
            <div class="btn-group card-dettaglio mr-4" role="toolbar"  aria-label="switch-visione">
                <a role="button" class="btn btn-loading" href="/sinistri/sinistri?TIPO_ELENCO=TABELLA"><i class="fa fa-list" aria-hidden="true"></i></a>
                <a role="button" class="btn disabled btn-loading" href="/sinistri/sinistri?TIPO_ELENCO=CARD"><i class="fa fa-table" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    
    <?  $aCapo = true;
        $contaRecord=0;
        $quanti_per_riga = 3;
        $i = count($page_data['sinistri']);
        if($i<$quanti_per_riga){
            $quanti_per_riga = $i;
        }
        /*if($i % 4 == 0){
            $quanti_per_riga = 4;
        }*/
        foreach($page_data['sinistri'] as $key => $sin) {
            if($aCapo){
                if($contaRecord>0){
                    //chiusura div row precedente
                    ?>
                    </div><!-- chiusura div row precedente -->
                    <?
                }
                ?>
                    <div class="row"> <!--div row -->
                <?
            }
            $contaRecord++;
            $aCapo = (($contaRecord % $quanti_per_riga)==0);

            $dtSin = new DateTime($sin->DATA_SINISTRO);
            ?>
            <div class="col-md-<?=(12/$quanti_per_riga)?> d-flex align-items-stretch pt-2">
                <div class="card card-dettaglio" >
                    <div class="card-header">
                        <a class="align-self-end pr-3" href="/sinistri/sinistro/<?=$sin->ID_SINISTRO?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <strong>
                            Sinistro del <a class="btn-loading" href="/sinistri/sinistro/<?=$sin->ID_SINISTRO?>"><u><?=$dtSin->format('d/m/Y')?></u></a>  su Polizza N. 
                            <a class="btn-loading" href="/polizze/polizza/<?=$sin->ID_POLIZZA?>"><u><?=$sin->NUMERO_POLIZZA?></u></a>
                        </strong>
                       
                    </div>
                    <div class="card-body">
                        <dl class="row small">
                        <?
                            $valori=array();
                            $valori[]=array("label"=>"Data Sinistro","valore"=>$dtSin->format('d/m/Y'));
                            $sTMP="<a class=\"btn-loading\" href=\"/sinistri/sinistro/$sin->ID_SINISTRO\">".$sin->NUMERO_SINISTRO."</a>";
                            $valori[]=array("label"=>"Numero Sinistro","valore"=>$sTMP);
                            $sTMP="<a class=\"btn-loading\" href=\"/polizze/polizza/$sin->ID_POLIZZA\">".$sin->NUMERO_POLIZZA."</a>";
                            $valori[]=array("label"=>"Numero Polizza","valore"=>$sTMP);
                            
                            $sTMP=(isset($sin->DESC_PARTICOLARITA)?$sin->DESC_PARTICOLARITA:$sin->STATO_SINISTRO_DESC);                            
                            $valori[]=array("label"=>"Stato","valore"=>$sTMP);
                            $valori[]=array("label"=>"Tipo Evento","valore"=>$sin->DESC_GARANZIA_SINISTRO);
                            foreach($valori as $valore){
                                ?>
                                <dt class="col-4"><?=$valore["label"]?>:</dt>
                                <dd class="col-8"><?=$valore["valore"]?></dd>
                                <?
                            }
                        ?>
                        </dl>
                    </div>
                </div>
            </div> 
                      
    <?  }
    
    
    if($contaRecord>0){
        //chiusura div row precedente
        ?>
        </div>
        <?
    }?>
</div>
