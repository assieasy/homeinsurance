<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Polizze extends MY_Controller {
	/**
	 * @var Polizze_model
	 */
	protected $PMOD;
	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		if(!$this->controllo_privacy()){
			header('Location: /privacy');
		}
		$this->load->model("Polizze_model");
		$this->PMOD = new Polizze_model($this->assi);
	}
	public function index()
	{
		$this->polizze();
	}

	protected function get_filtro_polizze_annullate(){
        $this->load->helper("cookie");
		$vedi_annullate = -1;
		if(isset($_POST['VEDI_POLIZZE_ANNULLATE'])){
			$vedi_annullate =$_POST['VEDI_POLIZZE_ANNULLATE'];
		}
		if(isset($_GET['VEDI_POLIZZE_ANNULLATE'])){
			$vedi_annullate =$_GET['VEDI_POLIZZE_ANNULLATE'];
		}	
		if($vedi_annullate<0){
			$vedi_annullate = get_cookie("VEDI_POLIZZE_ANNULLATE");
		}
        if($vedi_annullate!=1){
			$vedi_annullate = 0;
		}
        set_cookie("VEDI_POLIZZE_ANNULLATE",$vedi_annullate, $this->durata_cookie_secs);
        return $vedi_annullate;
    }
	public function polizze()
	{
		$tipo_elenco = $this->get_tipo_elenco();
		$vedi_annullate = $this->get_filtro_polizze_annullate();
		//$tipo_elenco = isset($_POST['TIPO_ELENCO'])?$_POST['TIPO_ELENCO']:"";
		

		$this->load->model("Documenti_model");
		$DOCMOD = new Documenti_model($this->assi);
		$this->load->model("Titoli_model");
		$TITMOD = new Titoli_model($this->assi);
		$page_config=array();
		$page_config['titolo']="Polizze";

		$page_data = array();
		
		$mie_polizze = array();
		
		$p=array();
		$p['sorts']=array();
		$p['sorts'][]=array("column"=>"DATA_EFFETTO_ORI","order"=>"DESC");
		$p['sorts'][]=array("column"=>"ID_POLIZZA","order"=>"DESC");
		if($this->is_funzione_abilitata("scadenze")){
			$p['sorts_scadenze']=array();
			$p['sorts_scadenze'][]=array("column"=>"DATA_EFFETTO","order"=>"DESC");
			$p['SHOW_SCADENZE']= 1;
		}
		
		if($this->is_funzione_abilitata("polizze.annullate")){
			if($vedi_annullate>0){
				$elenco = $this->PMOD->get_polizze($p);
			}
			else{
				$elenco = $this->PMOD->get_polizze_vive($p);
			}
		}
		else{
			$elenco = $this->PMOD->get_polizze_vive($p);
		}
		
		$mie_polizze = $elenco;
	
		$page_data['polizze']= $mie_polizze;
		$page_data['vedi_annullate'] = $vedi_annullate;
		if($tipo_elenco=="TABELLA"){
			$this->show("user_polizze",$page_config,$page_data);
		}
		else{
			$this->show("user_polizze_card",$page_config,$page_data);
		}
		

		
	}

	public function polizza($id_polizza=0)
	{
		$id_polizza = isset($_POST['ID_POLIZZA'])?$_POST['ID_POLIZZA']:$id_polizza;
		
		
		if($id_polizza==0){
			$this->polizze();
		}
		$this->load->model("Documenti_model");
		$DOCMOD = new Documenti_model($this->assi);
		$this->load->model("Titoli_model");
		$TITMOD = new Titoli_model($this->assi);
		$page_config=array();
		$page_config['titolo']="Polizza";

		$page_data = array();
		
		
		$p=array();
		$p['ID_POLIZZA']=$id_polizza;
		$p['SHOW_GARANZIE']=1;

		if($this->is_funzione_abilitata("pagamenti")){
			$p['SHOW_INCASSI']=1;
			$p['sorts_incassi']=array();
			$p['sorts_incassi'][]=array("column"=>"DATA_EFFETTO","order"=>"DESC");
		}
		
		if($this->is_funzione_abilitata("scadenze")){
			$p['SHOW_SCADENZE']=1;
			$p['sorts_scadenze']=array();
			$p['sorts_scadenze'][]=array("column"=>"DATA_EFFETTO","order"=>"DESC");
		}
		
		if($this->is_funzione_abilitata("sinistri")){
			$p['SHOW_SINISTRI']=1;
			$p['sorts_sinistri']=array();
			$p['sorts_sinistri'][]=array("column"=>"DATA_SINISTRO","order"=>"DESC");
		}

		$pol = $this->PMOD->get_polizza($p);
		if(empty($pol)){
			$this->polizze();
			return;
		}

		if(!empty($pol)){
			//DOCUMENTI
			$p=array();
			$p['ID_POLIZZA']= $pol->ID_POLIZZA;
			$p['SHOW_DOCUMENTI_PRODOTTO']= 1;
			$docs = $DOCMOD->get_documenti($p);
			$pol->documenti = $docs;
			
			if(!empty($pol->incassi)){
				foreach($pol->incassi as $inc){
					if(!isset($pol->ultimo_incasso)){
						$pol->ultimo_incasso = $inc;
					}
					else{
						if($inc->DATA_EFFETTO >= $pol->ultimo_incasso->DATA_EFFETTO) {
							$pol->ultimo_incasso = $inc;
						}
					}
					
				}
			}
			$p=array();
			$p['COD_COMPAGNIA']=$pol->COD_COMPAGNIA;
			$pol->contatti_utili = $this->get_contatti_utili_compagnia($p);
			
		}
		
		$page_data['polizza']= $pol;
		$this->show("user_polizza",$page_config,$page_data);
	}
	protected function get_contatti_utili_compagnia($params = array()){

		$this->load->model("Anagrafica_model");
		$AMOD = new Anagrafica_model($this->assi);
			
		$p=array();
		$p['COD_COMPAGNIA']=isset($params['COD_COMPAGNIA'])?$params['COD_COMPAGNIA']:"";
		$p['CONTESTO']=$AMOD::CONTATTI_ULTILI_CONTESTO_DETTAGLIO;
		$contatti = $AMOD->get_contatti_utili($p);

		return $contatti;
		
	}

}
