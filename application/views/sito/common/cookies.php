<? 
$all_config_obj = $sito['all_config_obj'];
$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();

$link_cookie= isset($hiPar->link_cookie)?$hiPar->link_cookie:"";
$link_privacy= isset($hiPar->link_privacy)?$hiPar->link_privacy:"";
$link_reclami= isset($hiPar->link_reclami)?$hiPar->link_reclami:"";

$ANAG_SITO = isset($hiPar->anagrafica_sito)?$hiPar->anagrafica_sito:array(); 

?>
<!-- cookie warning toast -->
<div class="fixed-bottom p-4">
    <div class="toast bg-dark text-white w-100 mw-100" role="alert" data-autohide="false">
        <div class="toast-body p-4 d-flex flex-column">
            <h4>Politica sui Cookies</h4>
            <p>
            Questo sito fa uso di cookie per migliorare lesperienza di navigazione degli utenti e 
            per raccogliere informazioni sull'utilizzo del sito stesso. 
            <!--
                Utilizziamo sia cookie tecnici sia cookie di parti terze per inviare messaggi promozionali 
            sulla base dei comportamenti degli utenti. 
                -->
           <? if($link_privacy){ ?>                
                Puoi conoscere i dettagli consultando la nostra <a href='<?=$link_privacy?>' target='_blank' >privacy policy</a>. 
           <?}?>            
            Proseguendo nella navigazione si accetta l'uso dei cookie.
            <? if($link_cookie){ ?>                
                <a href='<?=$link_cookie?>' target='_blank' >Maggiori informazioni</a>. 
           <?}?>  
            In caso contrario è possibile abbandonare il sito.
        </p>
            <div class="ml-auto">
                <button type="button" class="btn btn-outline-light mr-3" id="btnDeny">
                    Declina
                </button>
                <button type="button" class="btn btn-light" id="btnAccept">
                    Accetta
                </button>
            </div>
        </div>
    </div>
</div>

<script>
/*************** funzioni init ***********/
pageinitfunctions = pageinitfunctions?pageinitfunctions:[];


pageinitfunctions.push({name:'initCookies'});

    function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {   
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function cookieConsent() {
    if (!getCookie('allowCookies')) {
        $('.toast').toast('show')
    }
}

function initCookies() {
    $('#btnDeny').click(()=>{
        eraseCookie('allowCookies')
        $('.toast').toast('hide')
    });

    $('#btnAccept').click(()=>{
        setCookie('allowCookies','1',7)
        $('.toast').toast('hide')
    });



    // for demo / testing only
    $('#btnReset').click(()=>{
        // clear cookie to show toast after acceptance
        eraseCookie('allowCookies')
        $('.toast').toast('show')
    });
    
    cookieConsent();
}
</script>