<? 
/*
mi aspetto $filename:
	ritorno l'icona da utilizzare

*/
$filename = isset($filename)?$filename:"";
$estensione_file = strtoupper(pathinfo($filename, PATHINFO_EXTENSION));
$icona="file";
switch($estensione_file){
	case "PDF":
		$icona="file-pdf icona-pdf";
		break;
	case "DOC":
	case "DOCX":
			$icona="file-word";
		break;
	case "JPG":
	case "TIF":
	case "GIF":
			$icona="file-image";
		break;
	case "ZIP":
			$icona="file-archive";
		break;
}
echo $icona;