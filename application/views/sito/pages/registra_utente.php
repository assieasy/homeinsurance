<? 
$all_config_obj = $sito['all_config_obj'];
$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();

$logo= isset($hiPar->link_logo)?$hiPar->link_logo:"";  
$titolo= isset($hiPar->titolo)?$hiPar->titolo:"";  
$sottotitolo= isset($hiPar->sottotitolo)?$hiPar->sottotitolo:"";  
$procedi = isset($page_data['procedi'])?$page_data['procedi']:FALSE;
$user_info = isset($page_data['new_user_info'])?$page_data['new_user_info']:array();
$message = isset($page_data['message'])?$page_data['message']:array();
?>

<div class="container-fluid h-100 homepage">
    <div class="d-flex justify-content-center h-90" >
        <div class="d-flex flex-column logincard rounded">
            <div class="logincard-header pt-2">
                <? if($logo){ ?>
                <span class="d-flex justify-content-center align-middle main-font py-2">
                    <img class="img-fluid mr-4" src="<?=$sito['logo']?>" height="60">
                </span>
                <? }else{ ?>
                <h3 class="d-flex justify-content-center align-middle main-font">
                       <?=$titolo?>
                </h3>
                <h5 class="d-flex justify-content-center align-middle main-font">
                       <?=$sottotitolo?>
                </h5>
                <? } ?>
            </div>      
            <? if($procedi){ ?>
            <div class="collapse show" id="container-login">
                <div class="logincard-body px-4">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="email" id="loginForm-username">
                        
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-id-card"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Codice fiscale / Partita iva" id="loginForm-username">
                    </div>
                    <div class="d-flex justify-content-center form-group">
                        <input type="button" value="Registra" class="btn login_btn" onclick="doRegistrazione()">   
                    </div>
                    <div class="row alert alert-info alert-info-custom collapse" role="alert" id="loginForm-msg"></div>
                    <div class="row spinner-border text-primary float-center collapse" id="loginForm-loading" role="status">
                        
                    </div>
                </div>
                
            </div>
            <?}
            else{?>
                <div class="logincard-body px-4">
                    <div class="row alert alert-info alert-info-custom" role="alert" ><?=$message?></div>
                </div>
            <?}?>
            <div class="logincard-footer">
                <div class="d-flex justify-content-center">
                    <a href="/" >Vai a Home Page</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<?=print_r($sito,true);?>
    -->
<script>



function doRegistrazione() {
    var prefisso = "loginForm";
    return false;
    var username = $("#" + prefisso + "-username").val();
    var password = $("#" + prefisso + "-pass").val();

    $("#" + prefisso + "-msg").html("<strong>accesso in corso ..</strong>");
    $("#" + prefisso + "-msg").hide();
    $("#" + prefisso + "-loading").show();

    $.ajax({
        type: "POST",
        url: "/login/autenticazione",
        data: { username: username, password: password },
        success: function (response) {
            var res = eval('(' + response + ')');


            if (res.success) {
                $("#" + prefisso + "-msg").html("<strong>accesso avvenuto</strong>");
                $(window.location).attr('href', '/');
            }
            else {
                $("#" + prefisso + "-msg").html("<strong>Oops!</strong>" + res.message);
                $("#" + prefisso + "-msg").show();
                $("#" + prefisso + "-loading").hide();
            }
        },
        error: function () {

            $("#" + prefisso + "-msg").html("<strong>Errore!</strong>");
            $("#" + prefisso + "-msg").show();
            $("#" + prefisso + "-loading").hide();
        }
    });
}

function doRichiediCambio() {
    var prefisso = "forgotForm";
    var email = $("#" + prefisso + "-email").val();
    $("#" + prefisso + "-msg").hide();
    $("#" + prefisso + "-loading").show();
    $.ajax({
        type: "POST",
        url: "/login/mailcambiopassword",
        data: { email: email },
        success: function (response) {
            var res = eval('(' + response + ')');
            if (res.success) {
                $("#" + prefisso + "-msg").html("<strong>E' stata inviata la mail all'indirizzo richiesto</strong>");
                $("#" + prefisso + "-msg").show();
                $("#" + prefisso + "-loading").hide();
            }
            else {
                $("#" + prefisso + "-msg").html("<strong>Oops!</strong>" + res.message);
                $("#" + prefisso + "-msg").show();
                $("#" + prefisso + "-loading").hide();
            }
        },
        error: function () {
            $("#" + prefisso + "-msg").html("<strong>Errore!</strong>");
            $("#" + prefisso + "-msg").show();
            $("#" + prefisso + "-loading").hide();
        }
    });
}
</script>
