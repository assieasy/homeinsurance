<?
    $all_config_obj = $sito['all_config_obj'];
    $hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
    $vedi_premio_auto_rin = isset($hiPar->vedi_premio_auto_rin)?$hiPar->vedi_premio_auto_rin:0;  
    $vedi_premio_auto_rin = ($vedi_premio_auto_rin==1);
?>
<div class="container-fluid maincontainer pb-4">
    <?
    $pagina_origine=isset($page_data['pagina_origine'])?$page_data['pagina_origine']:"";
    $config=array(
        "border_class"=>"border-scadenze",
        "fa_icon"=>"calendar-check",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > '.($pagina_origine=="pagamenti"?"Storico pagamenti":"Scadenze").'</h5>'
 
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);

    
    ?>
    <div class="row pt-1">
        <div class="ml-auto">
            <div class="btn-group card-dettaglio mr-4" role="toolbar"  aria-label="switch-visione">
                <a role="button" class="btn btn-loading" href="/titoli/<?=$pagina_origine=="pagamenti"?"pagamenti":"scadenze"?>/?TIPO_ELENCO=TABELLA"><i class="fa fa-list" aria-hidden="true"></i></a>
                <a role="button" class="btn disabled btn-loading" href="/titoli/<?=$pagina_origine=="pagamenti"?"pagamenti":"scadenze"?>/?TIPO_ELENCO=CARD"><i class="fa fa-table" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    
    <?  $aCapo = true;
        $contaRecord=0;
        $quanti_per_riga = 2;
        $i = count($page_data['titoli']);
        if($i<$quanti_per_riga){
            $quanti_per_riga = $i;
        }
        /*if($i % 4 == 0){
            $quanti_per_riga = 4;
        }*/
        foreach($page_data['titoli'] as $key => $TIT) {
            if($aCapo){
                if($contaRecord>0){
                    //chiusura div row precedente
                    ?>
                    </div><!-- chiusura div row precedente -->
                    <?
                }
                ?>
                    <div class="row"> <!--div row -->
                <?
            }
            $contaRecord++;
            $aCapo = (($contaRecord % $quanti_per_riga)==0);

            $dtEff = new DateTime($TIT->DATA_EFFETTO);
            ?>
            <div class="col-md-<?=(12/$quanti_per_riga)?> d-flex align-items-stretch pt-2">
                <div class="card card-dettaglio" >
                    <div class="card-header">
                        <a class="btn-loading align-self-end pr-3" href="/titoli/titolo/<?=$TIT->ID_TITOLO?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <strong>
                            Scadenza del <a class="btn-loading" href="/titoli/titolo/<?=$TIT->ID_TITOLO?>"><u><?=$dtEff->format('d/m/Y')?></u></a> 
                             su polizza N. 
                            <a class="btn-loading" href="/polizze/polizza/<?=$TIT->ID_POLIZZA?>"><u><?=$TIT->NUMERO_POLIZZA?></u></a>
                        </strong>
                       
                    </div>
                    <div class="card-body">
                        <dl class="row small">
                        <?
                            $valori=array();
                            $valori[]=array("label"=>"Compagnia","valore"=>$TIT->DESC_COMPAGNIA);
                            $valori[]=array("label"=>"Numero Polizza","valore"=>$TIT->NUMERO_POLIZZA);
                            if($TIT->TARGA!=""){
                                $valori[]=array("label"=>"Targa","valore"=>$TIT->TARGA);
                            }
                            if(in_array("scadenze.premio",$funzioni_abilitate) || $TIT->STATO_TITOLO>2 ) {
                                //SE ARRETRATO FACCIO VEDERE IMPORTO SOLO SE "scadenze.premio" E' ABILITATA
                               /* $sTMP = "&euro; ".number_format($TIT->PREMIO_LORDO_DA_DIREZIONE,2,",",".");
                                $valori[]=array("label"=>"Importo","valore"=>$sTMP);*/
                                if($TIT->IS_SCADENZA_ANNUALITA==1 && $TIT->TIPO_RAMO=="A" && !$vedi_premio_auto_rin){
                                    $sTMP = $TIT->DESC_SCADENZA_ANNUALITA;
                                    $valori[]=array("label"=>"Tipo scadenza","valore"=>$sTMP);
                                }
                                else{
                                    $sTMP = "&euro; ".number_format($TIT->PREMIO_TOTALE,2,",",".");
                                    $valori[]=array("label"=>"Importo","valore"=>$sTMP);
                                }
                               
                            }
                            $valori[]=array("label"=>"Prodotto","valore"=>$TIT->DESC_PRODOTTO);
                            $valori[]=array("label"=>"Data Scadenza","valore"=>$dtEff->format('d/m/Y'));
                            $sTMP = $TIT->DESC_STATO_TITOLO;
                            if($TIT->STATO_TITOLO==10){
                                $sTMP = "Incassato da contabilizzare";
                            }
                            $valori[]=array("label"=>"Stato","valore"=>$sTMP);
                            foreach($valori as $valore){
                                ?>
                                <dt class="col-4"><?=$valore["label"]?>:</dt>
                                <dd class="col-8"><?=$valore["valore"]?></dd>
                                <?
                            }
                        ?>
                        </dl>
                    </div>
                </div>
            </div> 
                      
    <?  }
    
    
    if($contaRecord>0){
        //chiusura div row precedente
        ?>
        </div>
        <?
    }?>
</div>