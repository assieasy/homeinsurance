<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Accedi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="text" id="loginForm-username" name="username" class="form-control validate">
          <label data-error="wrong" data-success="right" for="loginForm-username">Username o Email</label>
        </div>

        <div class="md-form mb-4">
          <i class="fas fa-lock prefix grey-text"></i>
          <input type="password" id="loginForm-pass" name="password" class="form-control validate">
          <label data-error="wrong" data-success="right" for="loginForm-pass">Password</label>
        </div>

        <div class="spinner-border text-primary collapse" id="loginForm-loading" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="alert alert-info alert-info-custom collapse" role="alert" id="loginForm-msg"></div>
      </div>
      
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default" onclick="switchFormModal('modalForgotForm')" >Ho dimenticato la password</button>
        <button class="btn btn-default" onclick="doLoginModal()">Login</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modalForgotForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Accedi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="text" id="forgotForm-email" name="email" class="form-control validate">
          <label data-error="wrong" data-success="right" for="forgotForm-email">Email</label>
        </div>
      <div class="spinner-border text-primary collapse" id="forgotForm-loading" role="status">
        <span class="sr-only">Loading...</span>
      </div>
      <div class="alert alert-info alert-info-custom collapse" role="alert" id="forgotForm-msg"></div>
      
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default"  onclick="switchFormModal('modalLoginForm')">Login</button>
        <button class="btn btn-default" onclick="doRichiediCambioModal()">Richiedi</button>
      </div>
    </div>
  </div>
</div>


<script>
function doLoginModal(){
  var username = $("#loginForm-username").val();
  var password = $("#loginForm-pass").val();
 
  $("#loginForm-msg").hide();
  $("#loginForm-loading").hide();

  $.ajax({
      type: "POST",
      url: "<? echo site_url("login/autenticazione")?>",
      data: {username:username,password:password},
      success: function(response)
      {
        var res = eval('('+response+')');
        if(res.success){
            $(window.location).attr('href', '<? echo site_url("")?>');
        }
        else{
            $("#loginForm-msg").html("<strong>Spiacenti!</strong>"+ res.message);
            $("#loginForm-msg").show();
            $("#loginForm-loading").hide();
        }
      },
      error: function()
      {
        $("#loginForm-msg").html("<strong>Errore!</strong>");
        if($("#loginForm-msg").is(':hidden')) {
          $("#loginForm-msg").toggle();
        }
        $("#loginForm-loading").toggle();
      }
    });
}

function doRichiediCambioModal(){
  var email = $("#forgotForm-email").val();
  $("#forgotForm-loading").toggle();
  if(!$("#forgotForm-msg").is(':hidden')) {
    $("#forgotForm-msg").toggle();
  }
  $.ajax({
      type: "POST",
      url: "<? echo site_url("login/mailcambiopassword")?>",
      data: {email:email},
      success: function(response)
      {
        var res = eval('('+response+')');
        if(res.success){
          $("#forgotForm-msg").html("<strong>E' stata inviata la mail all'indirizzo richiesto</strong>");
            if($("#forgotForm-msg").is(':hidden')) {
              $("#forgotForm-msg").toggle();
            }
            $("#forgotForm-loading").toggle();
        }
        else{
            $("#forgotForm-msg").html("<strong>Spiacenti!</strong>"+ res.message);
            if($("#forgotForm-msg").is(':hidden')) {
              $("#forgotForm-msg").toggle();
            }
            $("#forgotForm-loading").toggle();
        }
      },
      error: function()
      {
        $("#forgotForm-msg").html("<strong>Errore!</strong>");
        if($("#forgotForm-msg").is(':hidden')) {
          $("#forgotForm-msg").toggle();
        }
        $("#forgotForm-loading").toggle();
      }
    });
}

function switchFormModal(newFormId){
  if(!$("#modalForgotForm").is(':hidden')) {
    $("#modalForgotForm").modal('toggle');
  }
  if(!$("#modalLoginForm").is(':hidden')) {
    $("#modalLoginForm").modal('toggle');
  }

  $("#"+newFormId).modal('toggle');

}
</script>