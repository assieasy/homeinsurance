<? 
$all_config_obj = $sito['all_config_obj'];
$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();

$logo= isset($hiPar->link_logo)?$hiPar->link_logo:"";  
$titolo= isset($hiPar->titolo)?$hiPar->titolo:"";  
$sottotitolo= isset($hiPar->sottotitolo)?$hiPar->sottotitolo:"";  

$msg_err = isset($page_data['msg_auenticazione_errata'])?$page_data['msg_auenticazione_errata']:"";
?>

<div class="container-fluid h-100 maincontainer">
    <div class="d-flex justify-content-center h-90" >
        <div class="d-flex flex-column logincard rounded">
            <div class="logincard-header pt-2">
                <? if($logo){ ?>
                <span class="d-flex justify-content-center align-middle main-font py-2">
                    <img class="img-fluid mr-4" src="<?=$sito['logo']?>" height="60">
                </span>
                <? }else{ ?>
                <h3 class="d-flex justify-content-center align-middle main-font">
                       <?=$titolo?>
                </h3>
                <h5 class="d-flex justify-content-center align-middle main-font">
                       <?=$sottotitolo?>
                </h5>
                <? } ?>
            </div>      
            <div class="collapse show" id="container-login">
               <!-- <div class="logincard-header">
                     <h3 class="d-flex justify-content-center main-font">Accedi</h3>
                </div>
                -->
                <div class="logincard-body px-4">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="email o username" id="loginForm-username">
                        
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="password" id="loginForm-pass">
                    </div>
                    <div class="d-flex justify-content-center form-group">
                        <input type="button" value="Entra" class="btn login_btn" onclick="doLogin()">   
                    </div>
                    <?if($msg_err!=""){?>
                        <div class="row alert alert-info alert-info-custom" role="alert" id="loginForm-msg"><?=$msg_err?></div>
                    <?}else{?>
                        <div class="row alert alert-info alert-info-custom collapse" role="alert" id="loginForm-msg"></div>
                    <?}?>
                    
                    <div class="row spinner-border text-primary float-center collapse" id="loginForm-loading" role="status">
                        
                    </div>
                </div>
                <div class="logincard-footer">
                    <div class="d-flex justify-content-center">
                        <a href="#" onclick="switchFormLogin('container-cambio-password')" >Hai dimenticato la password?</a>
                    </div>
                </div>
            </div>
            <div class="collapse" id="container-cambio-password">
                <div class="logincard-header">
                    <h3 class="d-flex justify-content-center">Richiedi Cambio Password</h3>
                </div>
                <div class="logincard-body px-4">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                        </div>
                        <input type="email" class="form-control" placeholder="email" id="forgotForm-email">
                        
                    </div>
                    <div class="d-flex justify-content-center form-group">
                        <input type="button" value="Richiedi" class="btn login_btn" onclick="doRichiediCambio()">   
                    </div>
                    
                    <div class="row alert alert-info alert-info-custom collapse" role="alert" id="forgotForm-msg"></div>
                    <div class="row spinner-border text-primary float-center collapse" id="forgotForm-loading" role="status">
                        
                    </div>
                </div>
                
                <div class="logincard-footer">
                    <div class="d-flex justify-content-center">
                        <a href="#" onclick="switchFormLogin('container-login')">Torna alla login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var pswd = document.getElementById("loginForm-pass");
/* Call 'checkPswd' when the 'Enter' key is released. */
pswd.onkeyup = function (e) {
   if (e.which == 13) doLogin();
};

function switchFormLogin(newFormId) {
    if (newFormId != "container-cambio-password") {
        $("#container-cambio-password").collapse('hide');
    }
    if (newFormId != "container-login") {
        $("#container-login").collapse('hide');
    }
    $("#" + newFormId).collapse('show');

}

function doLogin() {
    var prefisso = "loginForm";
    var username = $("#" + prefisso + "-username").val();
    var password = $("#" + prefisso + "-pass").val();

    $("#" + prefisso + "-msg").html("<strong>accesso in corso ..</strong>");
    $("#" + prefisso + "-msg").hide();
    $("#" + prefisso + "-loading").show();

    $.ajax({
        type: "POST",
        url: "/login/autenticazione",
        data: { username: username, password: password },
        success: function (response) {
            var res = eval('(' + response + ')');


            if (res.success) {
                $("#" + prefisso + "-msg").html("<strong>accesso avvenuto</strong>");
                $(window.location).attr('href', '/');
            }
            else {
                $("#" + prefisso + "-msg").html("<strong>Oops!</strong>" + res.message);
                $("#" + prefisso + "-msg").show();
                $("#" + prefisso + "-loading").hide();
            }
        },
        error: function () {

            $("#" + prefisso + "-msg").html("<strong>Errore!</strong>");
            $("#" + prefisso + "-msg").show();
            $("#" + prefisso + "-loading").hide();
        }
    });
}

function doRichiediCambio() {
    var prefisso = "forgotForm";
    var email = $("#" + prefisso + "-email").val();
    $("#" + prefisso + "-msg").hide();
    $("#" + prefisso + "-loading").show();
    $.ajax({
        type: "POST",
        url: "/login/mailcambiopassword",
        data: { email: email },
        success: function (response) {
            var res = eval('(' + response + ')');
            if (res.success) {
                $("#" + prefisso + "-msg").html("<strong>E' stata inviata la mail all'indirizzo richiesto</strong>");
                $("#" + prefisso + "-msg").show();
                $("#" + prefisso + "-loading").hide();
            }
            else {
                $("#" + prefisso + "-msg").html("<strong>Oops!</strong>" + res.message);
                $("#" + prefisso + "-msg").show();
                $("#" + prefisso + "-loading").hide();
            }
        },
        error: function () {
            $("#" + prefisso + "-msg").html("<strong>Errore!</strong>");
            $("#" + prefisso + "-msg").show();
            $("#" + prefisso + "-loading").hide();
        }
    });
}
</script>
