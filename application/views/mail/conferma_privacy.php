<?

$anagrafica_privacy = isset($page_data['anagrafica_privacy'])?$page_data['anagrafica_privacy']:array();

$elenco_privacy = isset($anagrafica_privacy->elenco_privacy)?$anagrafica_privacy->elenco_privacy:array();

$all_config_obj = $sito['all_config_obj'];
$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
$ANAG_SITO = isset($hiPar->anagrafica_sito)?$hiPar->anagrafica_sito:array(); 


?>

<html>

<body>
    <p>Gentile <?=$anagrafica_privacy->NOMINATIVO?>,
        <br>ti inviamo il riepilogo sulle scelte privacy che hai selezionato
    </p>    
    <? foreach($elenco_privacy as $PRI){    
        $si_checked="";
        $no_checked="";
        $descrizione_estesa=str_replace(PHP_EOL,"<br>",$PRI->DESCRIZIONE_ESTESA);
        if($PRI->DATA_CONSENSO!=""){
            if($PRI->CONSENSO>0){
                $si_checked="checked";
            }
            else{
                $no_checked="checked";
            }
        }
        ?>
        <p>
            <h5><?=$PRI->DESCRIZIONE?>: <?=($si_checked?"Accettata":"Declinata")?></h5> 
            <i><?=$descrizione_estesa?></i>
        </p>
        
       
        
    <?}?>
    <?             
    $link_privacy = "";
    $sTMP= isset($hiPar->link_privacy)?$hiPar->link_privacy:"";
    if($sTMP){
        $link_privacy="<a href='$sTMP' target='_blank' >qui</a>";
    }
    ?>
    <?if($link_privacy!=""){ ?>
        <h5>Ti ricordiamo che puoi visualizzare la nostra Privacy Policy &nbsp;<?=$link_privacy ?>.</h5>
    <?}?> 
   
    <span>Cordiali saluti</span>

    <?if(!empty($ANAG_SITO)){?>
        <p>
        <?=$ANAG_SITO->NOMINATIVO?><br>
        <? 
            $sTMP = $ANAG_SITO->INDIRIZZO;
            $sTMP .= ", ".$ANAG_SITO->CAP;
            $sTMP .= " ".$ANAG_SITO->COMUNE;
            $sTMP .= " (".$ANAG_SITO->PROVINCIA.")";
            echo $sTMP;
        ?><br>
        <?
        $valori=array();
        $valori[]=array("label"=>"Tel","valore"=>$ANAG_SITO->TEL_CASA);
        $valori[]=array("label"=>"Fax","valore"=>$ANAG_SITO->FAX);
        $valori[]=array("label"=>"mail","valore"=>$ANAG_SITO->EMAIL);
        $valori[]=array("label"=>"PEC","valore"=>$ANAG_SITO->EMAIL_PEC);
        foreach($valori as $valore){    
            if($valore["valore"]!=""){
                echo $valore["label"].": ".$valore["valore"]."<br>";
            }                
        }
        ?>
        </p>
    <?}?>

</body>

</html>