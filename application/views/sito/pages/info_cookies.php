<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-account",
        "fa_icon"=>"cookie",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > cookies</h5>'
    );

     echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
    <div class="row pt-3">
        <div class="col">
            <strong>Cookie</strong></p>
            <p>Il
            sito presente utilizza i cookies, piccoli file che vengono memorizzati sul
            computer del navigatore e che consentono di acquisire/conservare informazioni
            sulle preferenze delle visite utilizzate al fine di:</p>



            <ul><li>migliorare
            le funzionalità di questo sito;</li><li>semplificare
            la navigazione attraverso procedure automatizzate (per es. Login, impostazione
            lingua sito)</li><li>analizzare
            l’uso del sito per migliorare la sua usabilità e user experience.</li></ul>



            <p>I <strong>cookie di sessione</strong>
            sono necessari per poter differenziare gli utenti collegati ed evitare che la
            funzionalità richiesta da un visitatore sia utilizzata da un altro visitatore,
            e sono necessari ai fini della sicurezza per cercare di neutralizzare attacchi
            informatici al sito.</p>



            <p>I
            cookie di sessione utilizzati su questo sito non acquisiscono dati personali e
            la loro durata è legata alla sessione di navigazione corso. La chiusura del
            browser cancella i cookie di sessione. Per questi non occorre consenso.</p>



            <p>I <strong>functionality cookie</strong>
            utilizzati da questo sito sono indispensabili all’uso del sito e sono associati
            a delle funzionalità richieste specificatamente da parte del visitatore (per
            esempio il Login in un’area riservata, o l’accettazione del banner di avviso
            sui cookie). Per questi non occorre consenso.<br>
            <br>
            <strong>Disabilitazione dei
            cookie</strong></p>



            <p>Il
            rifiuto dell’accettazione del banner iniziale prevede il blocco
            dell’installazione dei cookie.<br>
            Comunque i cookies sono strettamente collegati al browser impiegato per la
            navigazione e la gestione di questi, come la revoca di cookie preventivamente
            accettati, può avvenire attraverso il browser.<br>
            Potete leggere tutte le spiegazioni per disabilitare i cookie ai seguenti
            link:&nbsp;</p>



            <p><a href="https://support.mozilla.org/it/kb/Attivare%20e%20disattivare%20i%20cookie" target="_blank" rel="noreferrer noopener">Mozilla Firefox</a>&nbsp;–&nbsp;<a href="https://support.microsoft.com/it-it/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank" rel="noreferrer noopener">Microsoft
            Internet Explorer</a>&nbsp;– <a href="https://support.microsoft.com/it-it/help/4027947/windows-delete-cookies" target="_blank" rel="noreferrer noopener">Microsoft Edge</a>
            –&nbsp;<a href="https://support.google.com/chrome/answer/95647?hl=it" target="_blank" rel="noreferrer noopener">Google
            Chrome</a>&nbsp;–&nbsp;<a href="http://help.opera.com/Windows/10.00/it/cookies.html" target="_blank" rel="noreferrer noopener">Opera</a>&nbsp;–&nbsp;<a href="https://support.apple.com/it-it/HT201265" target="_blank" rel="noreferrer noopener">Apple
            Safari</a>&nbsp;
        </div>
    </div>    
</div>
