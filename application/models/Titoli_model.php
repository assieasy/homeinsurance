<?php

require_once "Base_rest_model.php";

class Titoli_model extends Base_rest_model
{
    public function get_titoli($params=array()){
        $params["sorts"] = isset($params["sorts"])?$params["sorts"]:$this->get_default_sort();
        $ret = $this->CH->get_titoli($params);
        $elenco = $this->CH->extract_data($ret);
        return $elenco;
    }
    public function get_scadenze($params=array()){
        $params['STATO_TITOLO']=1;

        $tmp = $this->get_filtro_scadenze();
        foreach($tmp as $key=>$value){
            $params[$key] = isset($params[$key] )?$params[$key] :$value;
        }
        $params["sorts"] = isset($params["sorts"])?$params["sorts"]:$this->get_default_sort();
      
        $ret = $this->CH->get_titoli($params);
		$elenco = $this->CH->extract_data($ret);
        return $elenco;
    }
    public function get_incassi($params=array()){
        $params['STATO_TITOLO']="4,10";
        $params["sorts"] = isset($params["sorts"])?$params["sorts"]:$this->get_default_sort();
        $ret = $this->CH->get_titoli($params);
		$elenco = $this->CH->extract_data($ret);
        return $elenco;
    }
    public function get_ultimi_incassi($params=array()){
        $tmp = $this->get_filtro_incassi();
        foreach($tmp as $key=>$value){
            $params[$key] = isset($params[$key] )?$params[$key] :$value;
        }
        $params["sorts"] = isset($params["sorts"])?$params["sorts"]:$this->get_default_sort();
       
        return $this->get_incassi($params);
    }
    public function get_titolo($params=array()){
        $ret = $this->CH->get_titoli($params);
        $record = $this->CH->extract_first_data($ret);
        return $record;
    }
    public function get_scadenze_scadute($params=array()){
        $elenco=array();
        $tmp = $this->get_filtro_scadenze();
        foreach($tmp as $key=>$value){
            $params[$key] = isset($params[$key] )?$params[$key] :$value;
        }
        $params['DATA_EFFETTO_A'] = date("Y-m-d", strtotime("-1 days")); 
        $scadenze = $this->get_scadenze($params);
        return $elenco;
    }

    public function get_default_sort(){
        $params=array();
        $params[]=array("column"=>"DATA_EFFETTO","order"=>"DESC");
        return $params;
    }
    
    public function get_filtro_scadenze(){
        $params=array();
     
        $gg = (int) get_parametro_hi('min_anzianita_scadenza',-15);
        $gg = ($gg*-1);
        $sTMP = date("Y-m-d", strtotime("$gg days"));
        $params['DATA_EFFETTO_A'] = $sTMP; 
 
        $gg = (int) get_parametro_hi('max_anzianita_scadenza',90);
        $gg = ($gg*-1);
        $sTMP = date("Y-m-d", strtotime("$gg days"));
        $params['DATA_EFFETTO_DA'] = $sTMP; 

        // FILTRO PUBBLICAZIONE SCADENZE
        // SE VIENE IMPOSTATO UN GIORNO DI PUBBLICAZIONE SCADENZE ...
        $gg = (int) get_parametro_hi('gg_pubblicazione_scadenze',0);
        if($gg>0){
            $gg_oggi = date("d");
            if($gg_oggi>=$gg) {
                //SE OGGI E' MAGGIORE DEL GIORNO DI PUBBLICAZIONE
                // CONSIDERO ANCHE LE SCADENZE DEL MESE PROSSIMO
                $sTMP = date("Y-m-t", strtotime("+1 month"));
            }
            else{
                //SE OGGI E' MINORE DEL GIORNO DI PUBBLICAZIONE
                // CONSIDERO SOLO LE SCADENZE DI QUESTO MESE
                $sTMP = date("Y-m-t");
            }
            if($sTMP<$params['DATA_EFFETTO_A']){
                $params['DATA_EFFETTO_A'] = $sTMP;
            }
        }
        // FILTRO PUBBLICAZIONE SCADENZE FINE

        $params['FILTRO_AVVISI'] =1;
        return $params;
    }

    public function get_filtro_incassi(){
        $params=array();
        $params['DATA_EFFETTO_DA'] = (date("Y")-1)."-01-01";
        
        return $params;
    }
    
}
