<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documenti extends MY_Controller {
	protected $DMOD;
	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		$this->load->model("Documenti_model");
		$this->DMOD = new Documenti_model($this->assi);
	}
	public function index()
	{
		$this->documenti();
	}

	public function documenti()
	{
		$page_config=array();
		$page_config['titolo']="Documenti";

		$page_data = array();
		$elenco = $this->DMOD->get_documenti();
		$page_data['documenti']= $elenco;
		$this->show("user_documenti",$page_config,$page_data);
	}
	
}
