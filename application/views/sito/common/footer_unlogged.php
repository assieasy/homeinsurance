<? 
$ANAG_SITO = get_parametro_hi("anagrafica_sito",array());
?>
<footer class="bg-light">
    <div class="container-fluid footer-container py-2">
<?
if(!empty($ANAG_SITO)){
?>
   
        <div class="row">
            <div class="col-md-6">
                <div class="small text-center text-muted">
                <?=$ANAG_SITO->NOMINATIVO?><br>
                <? 
                    $sTMP = $ANAG_SITO->INDIRIZZO;
                    $sTMP .= ", ".$ANAG_SITO->CAP;
                    $sTMP .= " ".$ANAG_SITO->COMUNE;
                    $sTMP .= " (".$ANAG_SITO->PROVINCIA.")";
                    echo $sTMP;
                ?>
                <br>
                
                <?
                $valori=array();
                $valori[]=array("label"=>"Tel","valore"=>$ANAG_SITO->TEL_CASA);
                $valori[]=array("label"=>"Fax","valore"=>$ANAG_SITO->FAX);
                $valori[]=array("label"=>"mail","valore"=>$ANAG_SITO->EMAIL);
                $valori[]=array("label"=>"PEC","valore"=>$ANAG_SITO->EMAIL_PEC);
                foreach($valori as $valore){    
                    if($valore["valore"]!=""){
                        echo $valore["label"].": ".$valore["valore"]."<br>";
                    }                
                }
                ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="small text-center text-muted">
                    PIVA: <?=$ANAG_SITO->CODICEFISCALE?><br>
                    <? 
                    $sRui = get_parametro_hi("rui_sito");
                    if($sRui!=""){
                        echo "Iscrizione al RUI nr: $sRui.<br>";
                        echo $ANAG_SITO->NOMINATIVO . " è sottoposta al controllo dell’IVASS.";
                    }
                    ?>
                    
                </div>
            </div>
        </div>
<?}?>
        <div class="row">
            <div class="col-md-12 small text-center text-muted">
                <? 
                    $links = "";
                    $sTMP= get_parametro_hi("link_cookie");
                    $target_cookies = "target='_blank'";
                    if($sTMP==""){
                        $sTMP="/welcome/cookies";
                        $target_cookies="";
                    }
                 
                    if($sTMP){
                        $links.= ($links=="")?"":" - ";
                        $links.="<a href='$sTMP' $target_cookies >Cookie Policy</a>";
                    }
                    $sTMP= get_parametro_hi("link_privacy");
                    if($sTMP){
                        $links.= ($links=="")?"":" - ";
                        $links.=" <a href='$sTMP' target='_blank' >Privacy Policy</a>";
                    }
                    $sTMP= get_parametro_hi("link_reclami");
                    if($sTMP){
                        $links.= ($links=="")?"":" - ";
                        $links.=" <a href='$sTMP' target='_blank' >Reclami</a>";
                    }

                    echo $links;
                ?>
            </div>
        </div>
    </div>
</footer>