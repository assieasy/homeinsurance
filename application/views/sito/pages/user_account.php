<? $ANAG = $page_data['anagrafica']; ?>
<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-account",
        "fa_icon"=>"user",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > Account</h5>'
    );

     echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
    <div class="row pt-3">
        <div class="col-md-6 py-2">
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD ACCOUNT -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-user mr-1" aria-hidden="true"></i> Account</h5>
                        </div>
                        <div class="card-body">
                            <dl class="">
                                <? $valori=array();
                                $valori[]=array("label"=>"Nome","valore"=>$ANAG->NOMINATIVO);
                                $valori[]=array("label"=>"Codice Fiscale","valore"=>$ANAG->CODICEFISCALE);
                                $valori[]=array("label"=>"Indirizzo","valore"=>$ANAG->INDIRIZZO);
                                $valori[]=array("label"=>"Comune","valore"=>$ANAG->COMUNE);
                                $valori[]=array("label"=>"Cap","valore"=>$ANAG->CAP);
                                $valori[]=array("label"=>"Telefono","valore"=>$ANAG->TEL_CASA);
                                $valori[]=array("label"=>"Cellulare","valore"=>$ANAG->TEL_CELLULARE);
                                if($ANAG->TIPOPERSONA=="PF"){
                                    $valori[]=array("label"=>"Professione","valore"=>$ANAG->DESC_PROFESSIONE);
                                }
                                if($ANAG->TIPOPERSONA=="PG"){
                                    $valori[]=array("label"=>"Tipo Societ&agrave;","valore"=>$ANAG->DESC_TIPO_SOCIETA);
                                }
                                foreach($valori as $valore){
                                    ?>
                                    <dt><?=$valore["label"]?></dt>
                                    <dd><?=$valore["valore"]?>&nbsp;</dd>
                                    <?
                                }
                                ?>
                            </dl> 
                        </div>
                    </div>
                    <!-- CARD ACCOUNT FINE-->
                </div>
            </div>

            <?
            $documenti =isset($page_data['documenti_anagrafica'])?$page_data['documenti_anagrafica']:array();
            if(!empty($documenti)) {?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD DOCUMENTI -->
                    <div class="card card-dettaglio " >
                        <div class="card-header">
                        <h5><i class="fa fa-file-alt mr-1" aria-hidden="true"></i> Archivio Documenti</h5>
                        </div>
                        <div class="card-body">
                            <ul>
                                <?  foreach($documenti as $doc) {?>
                                        <li><?=$doc->DESC_TIPO_DOCUMENTO?>:
                                            <a target="_blank" href="<?=$sito['ASSI_URL_PUBBLICO']."/".$doc->URL_DOCUMENTO."/download"?>" >
                                            <i class="fa fa-<?=$this->load->view("sito/common/icona_file_fa",array("filename"=>$doc->NOME_FILE),TRUE);?> fa-2x"></i>
                                            <?=($doc->TITOLO!=""?$doc->TITOLO:$doc->NOME_FILE)?>
                                            </a>
                                        </li>
                                <?  }?>
                            </ul> 
                        </div>
                    </div>
                    <!-- CARD DOCUMENTI FINE-->
                </div>
            </div>
            <?}?>        
        </div>
        <div class="col-md-6 py-2">
            <div class="row py-2">
                <div class="col-12">
                <?if(in_array("messaggi",$funzioni_abilitate) ){ ?>
                    <!-- CARD VARIAZIONI -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-user-edit mr-1" aria-hidden="true"></i> Comunica Variazioni</h5>
                        </div>
                        <div class="card-body"> 
                            <form id="form-variazioni">        
                                <div class="row">
                                    <div class="input-group form-group col">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                        </div>
                                        <input type="text" name="TEL_CASA" class="form-control form-control-sm" placeholder="telefono" id="form-variazioni-telefono">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-group form-group col">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-mobile"></i></span>
                                        </div>
                                        <input type="text" name="TEL_CELLULARE" class="form-control form-control-sm" placeholder="cellulare" id="form-variazioni-cellulare">
                                    </div>
                                </div>
                                <?if($ANAG->TIPOPERSONA == "PF"){?>
                                    <div class="row">
                                        <div class="input-group form-group col">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-briefcase"></i></span>
                                            </div>
                                            <select name="PROFESSIONE_SIS"  class="form-control form-control-sm" placeholder="professione" id="form-variazioni-professione">
                                                <option value="0" selected>- seleziona una professione -</option>
                                                <?
                                                $arr = isset($page_data['professioni'] )?$page_data['professioni'] :array();
                                                foreach($arr as $professione){
                                                    echo "<option value=\"".$professione->ELEMENTO."\">".$professione->DESCRIZIONE."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?}?>
                                <?if($ANAG->TIPOPERSONA == "PG"){?>
                                    <div class="row">
                                        <div class="input-group form-group col">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-briefcase"></i></span>
                                            </div>
                                            <select name="TIPO_SOCIETA"  class="form-control form-control-sm" placeholder="tipo societa" id="form-variazioni-tipo_societa">
                                                <option value="0" selected>- seleziona il tipo di societa' -</option>
                                                <?
                                                $arr = isset($page_data['tipi_societa'] )?$page_data['tipi_societa'] :array();
                                                foreach($arr  as $professione){
                                                    echo "<option value=\"".$professione->ELEMENTO."\">".$professione->DESCRIZIONE."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?}?>
                            </form>
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary" onclick="doVariazioniAnagrafica()">Comunica Variazioni</button>
                            <div class="row alert alert-info alert-info-custom collapse" role="alert" id="form-variazioni-msg"></div>
                        </div>
                    </div>
                    <!-- CARD VARIAZIONI FINE-->
                    <?}?>
                </div>
            </div>

            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD CAMBIOPWD -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-lock mr-1" aria-hidden="true"></i> Cambio Password</h5>
                        </div>
                        <div class="card-body"> 
                            <form id="changeForm">        
                                <div class="row">
                                    <div class="input-group form-group col">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                        </div>
                                        <input type="password" class="form-control form-control-sm" placeholder="vecchia password" id="changeForm-old-pass">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-group form-group col">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                                        </div>
                                        <input type="password" class="form-control form-control-sm" placeholder="nuova password" id="changeForm-new-pass">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-group form-group col">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-redo"></i></span>
                                        </div>
                                        <input type="password" class="form-control form-control-sm" placeholder="ripeti nuova password" id="changeForm-new-pass2">
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                        <div class="card-footer text-right" >
                            <button class="btn btn-primary" onclick="doCambiaPassword()">Cambia</button>
                            <div class="row alert alert-info alert-info-custom collapse" role="alert" id="changeForm-msg"></div>
                        </div>
                    </div>
                    <!-- CARD CAMBIOPWD FINE-->
                </div>
            </div>
            <div class="row py-2">
                <div class="col-12">
                     <!-- CARD CONSENSI PRIVACY -->
                     <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-user-shield mr-1" aria-hidden="true"></i> Privacy</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <a href="/privacy" class="form-control form-control-sm btn btn-primary btn-loading" >Consulta</a>
                            </div>
                        </div>
                    </div>
                    <!-- CARD CONSENSI PRIVACY FINE-->
                </div>
            </div>
            <div class="row py-2">
                <div class="col-12">
                </div>
            </div>
        </div>
    </div>    
</div>

<script>

function doCambiaPassword() {
    var ok = true;
    var prefisso = "changeForm";
    var old_password = $("#" + prefisso + "-old-pass").val();
    var new_password = $("#" + prefisso + "-new-pass").val();
    var new_password_repeat = $("#" + prefisso + "-new-pass2").val();
    if(ok && old_password==""){
        var o = $("#" + prefisso + "-msg");
        o.html("<strong>Oops!</strong>[Vecchia password] non compilata");
        o.show();
        ok = false;
    }
    if(ok && new_password!=new_password_repeat){
        var o = $("#" + prefisso + "-msg");
        o.html("<strong>Oops!</strong>[Nuova password] non coincide con [Ripeti Nuova password]");
        o.show();
        ok= false;
    }

    if(ok){
        $("#" + prefisso + "-msg").hide();
        start_spinner();
        $.ajax({
            type: "POST",
            url: "/utente/updatepassword",
            data: { old_password: old_password, new_password:new_password, new_password_repeat:new_password_repeat },
            success: function (response) {
                stop_spinner();
                var res = eval('(' + response + ')');
                var o = $("#" + prefisso + "-msg");
                if (res.success) {
                    o.html("<strong>La password è stata modificata</strong>");
                    o.show();
                    $("#" + prefisso).trigger("reset");
                }
                else {
                    o.html("<strong>Oops!</strong>" + res.message);
                    o.show();
                }
            },
            error: function () {
                stop_spinner();
                var o = $("#" + prefisso + "-msg");
                o.html("<strong>Errore!</strong>");
                o.show();
            }
        });
    }
}

function doVariazioniAnagrafica() {
    var prefisso = "form-variazioni";
    var conta_variazioni=0;
    var tmp = $("#" + prefisso + "-telefono").val();
    conta_variazioni = conta_variazioni+(tmp.trim()==""?0:1);
    
    tmp = $("#" + prefisso + "-cellulare").val();
    conta_variazioni = conta_variazioni+(tmp.trim()==""?0:1);
    
    tmp = $("#" + prefisso + "-professione").val();
    if(tmp){
        conta_variazioni = conta_variazioni+(tmp.trim()=="0"?0:1);
    }
    tmp = $("#" + prefisso + "-tipo_societa").val();
    if(tmp){
        conta_variazioni = conta_variazioni+(tmp.trim()=="0"?0:1);
    }
    
    if(conta_variazioni==0){
        var o = $("#" + prefisso + "-msg");
        o.html("<strong>Oops!</strong>Nessuna variazione da comunicare all'Agenzia");
        o.show();
        return false;
    }
    var o = $("#" + prefisso + "-msg");
    o.html("<strong>Grazie per la collaborazione!</strong><br>Le variazioni inviate saranno validate da un operatore di Agenzia.");
    o.show();
     
    start_spinner();
    var dati_form = $("#" + prefisso).serialize();
    $.ajax({
        type: "POST",
        url: "/utente/registra_variazione",
        data: dati_form,
        success: function (response) {
            stop_spinner();
            var res = eval('(' + response + ')');
            if (res.success) {
                var msg = (res.message)?res.message:"Richiesta inoltrata all'Agenzia";
                $("#" + prefisso + "-msg").html("<strong>"+msg+"</strong>");
                $("#" + prefisso + "-msg").show();
                $("#" + prefisso).trigger("reset");
                
            }
            else {
                $("#" + prefisso + "-msg").html("<strong>Oops!</strong>" + res.message);
                $("#" + prefisso + "-msg").show();
            }
        },
        error: function () {
            stop_spinner();
            $("#" + prefisso + "-msg").html("<strong>Errore!</strong>");
            $("#" + prefisso + "-msg").show();
        }
    });
    
}
</script>