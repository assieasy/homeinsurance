    <? 
    $all_config_obj = $sito['all_config_obj'];
    $hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
    $vedi_premio_auto_rin = isset($hiPar->vedi_premio_auto_rin)?$hiPar->vedi_premio_auto_rin:0;  
    $vedi_premio_auto_rin = ($vedi_premio_auto_rin==1);

    $POLIZZA = isset($page_data['polizza'])?$page_data['polizza']:array();
    $dtScadenza = new DateTime($POLIZZA->DATA_SCADENZA_CONTRATTO);
    $dtEff = new DateTime($POLIZZA->DATA_EFFETTO_ORI);
    $show_contraente = true;
    ?>
<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-polizze",
        "fa_icon"=>"id-card",
        "titolo"=>'<a class="btn-loading" href="/polizze/polizze">Polizze</a> > Polizza n.'.$POLIZZA->NUMERO_POLIZZA
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
   
    <div class="row pt-3">
        <div class="col-md-6 py-2">
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD POLIZZA -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-id-card mr-1" aria-hidden="true"></i> Polizza</h5>
                        </div>
                        <div class="card-body">
                            <dl class="">
                                <? $valori=array();
                                $valori[]=array("label"=>"Compagnia","valore"=>$POLIZZA->DESC_COMPAGNIA);
                                $valori[]=array("label"=>"Numero Polizza","valore"=>$POLIZZA->NUMERO_POLIZZA);
                                $sTMP = isset($POLIZZA->BENEFICIARIO_CAUZIONI)?$POLIZZA->BENEFICIARIO_CAUZIONI:"";
                                if($sTMP!=""){
                                    $valori[]=array("label"=>"Beneficiario","valore"=>$sTMP);
                                }
                                if($POLIZZA->TARGA!=""){
                                    $valori[]=array("label"=>"Targa","valore"=>$POLIZZA->TARGA);
                                }

                                $valori[]=array("label"=>"Ramo","valore"=>$POLIZZA->DESC_RAMO);
                                $valori[]=array("label"=>"Prodotto","valore"=>$POLIZZA->DESC_PRODOTTO);
                                $valori[]=array("label"=>"Frazionamento","valore"=>$POLIZZA->FRAZIONAMENTO);
                                $valori[]=array("label"=>"Data Decorrenza","valore"=>$dtEff->format('d/m/Y'));
                                $valori[]=array("label"=>"Data Scadenza","valore"=>$dtScadenza->format('d/m/Y'));
                                $valori[]=array("label"=>"Stato","valore"=>$POLIZZA->DESC_STATO_POLIZZA);
                                foreach($valori as $valore){
                                    ?>
                                    <dt ><?=$valore["label"]?></dt>
                                    <dd ><?=$valore["valore"]?></dd>
                                    <?
                                }
                                ?>
                            </dl> 
                        </div>
                    </div>
                    <!-- CARD POLIZZA FINE-->
                </div>
            </div>
            <? if(in_array("pagamenti",$funzioni_abilitate) && !empty($POLIZZA->incassi)){?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD PAGAMENTI -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-shopping-bag mr-1" aria-hidden="true"></i> Ultimi Pagamenti</h5>
                        </div>
                        <div class="card-body">
                            <ul>
                                <?  foreach($POLIZZA->incassi as $TIT) {
                                        $dtEffSca = new DateTime($TIT->DATA_EFFETTO);
                                        //$premioSca = number_format( $TIT->PREMIO_LORDO_DA_DIREZIONE ,2 , "," , "."); 
                                        $premioSca = number_format( $TIT->PREMIO_TOTALE ,2 , "," , "."); 
                                        $str_goto = '<a class="btn-loading" href="/titoli/titolo/'.$TIT->ID_TITOLO.'"><u>'.$dtEffSca->format('d/m/Y').'</u></a>'; 
                                        ?>
                                        
                                        <li><?=("Pagamento titolo del ".$str_goto." per un importo di &euro;.".$premioSca)?>
                                        <?  if(!empty($TIT->documenti)){ ?>
                                                <ul><?
                                                foreach($TIT->documenti as $doc) {?>
                                                    <li>
                                                        <?=$doc->DESC_TIPO_DOCUMENTO?>:
                                                        <a target="_blank" href="<?=$sito['ASSI_URL_PUBBLICO']."/".$doc->URL_DOCUMENTO."/download"?>" >
                                                        <i class="fa fa-<?=$this->load->view("sito/common/icona_file_fa",array("filename"=>$doc->NOME_FILE),TRUE);?> fa-2x"></i>
                                                        <?=($doc->TITOLO!=""?$doc->TITOLO:$doc->NOME_FILE)?>
                                                        </a>
                                                    </li>
                                                <?  }?>
                                                </ul>
                                        <?  }?>

                                        </li>
                                <?  }?>
                            </ul> 
                        </div>
                    </div>
                    <!-- CARD PAGAMENTI FINE-->
                </div>
            </div>
            <? } else {
                if(in_array("pagamenti",$funzioni_abilitate) && !empty($POLIZZA->ultimo_incasso)){ ?>
                    <div class="row py-2">
                        <div class="col-12">
                            <!-- CARD INCASSO -->
                            <div class="card card-dettaglio" >
                                <div class="card-header">
                                    <h5><i class="fa fa-shopping-bag mr-1" aria-hidden="true"></i> Ultimo Pagamento</h5>
                                </div>
                                <div class="card-body">
                                    <ul>
                                        <?  $TIT = $POLIZZA->ultimo_incasso;
                                            $dtEffSca = new DateTime($TIT->DATA_EFFETTO);
                                            //$premioSca = number_format( $TIT->PREMIO_LORDO_DA_DIREZIONE ,2 , "," , "."); 
                                            $premioSca = number_format( $TIT->PREMIO_TOTALE ,2 , "," , "."); 
                                        ?>
                                        <li><?=($TIT->DESC_STATO_TITOLO." del ".$dtEffSca->format('d/m/Y')." per un importo di &euro;.".$premioSca)?></li>
                                    </ul> 
                                </div>
                            </div>
                            <!-- CARD POLIZZA FINE-->
                        </div>
                    </div>
                <?}   
            }?>
            <?if(!empty($POLIZZA->sinistri) && in_array("sinistri",$funzioni_abilitate)) {?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD SINISTRI -->
                    <div class="card card-dettaglio " >
                        <div class="card-header">
                        <h5><i class="fa fa-user-injured mr-1" aria-hidden="true"></i> Sinistri</h5>
                        </div>
                        <div class="card-body">
                            <ul>
                                <?  foreach($POLIZZA->sinistri as $sin) {
                                    $dtSinistro = new DateTime($sin->DATA_SINISTRO);
                                    ?>
                                        <li>
                                            Sinistro del <?=$dtSinistro->format('d/m/Y')?> numero  <a class="btn-loading" href="/sinistri/sinistro/<?=$sin->ID_SINISTRO?>"><?=$sin->NUMERO_SINISTRO?>
                                        </li>
                                <?  }?>
                            </ul> 
                        </div>
                    </div>
                    <!-- CARD SINISTRI FINE-->
                </div>
            </div>
            <?}?>
            
        </div>

        <div class="col-md-6 py-2">
            <?if(in_array("messaggi",$funzioni_abilitate) ){ ?>
            <div class="row py-2">
                <div class="col-12">
                    <div class="card card-dettaglio border-0 m-0 p-0" >
                            <div class="card-body justify-content-center text-center">
                            <h5><a href="/messaggi/polizza/<?=$POLIZZA->ID_POLIZZA?>" class="btn btn-primary btn-loading mt-1"  ><i class="fa fa-comment-dots mr-1" aria-hidden="true"></i>Scrivi un messaggio / Invia Documento</a></h5>
                            </div>
                    </div>
                </div>
            </div>
            <?}?>
            <?if(!empty($POLIZZA->documenti)) {?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD DOCUMENTI -->
                    <div class="card card-dettaglio " >
                        <div class="card-header">
                        <h5><i class="fa fa-file-alt mr-1" aria-hidden="true"></i> Archivio Documenti</h5>
                        </div>
                        <div class="card-body">
                            <ul>
                                <?  foreach($POLIZZA->documenti as $doc) {?>
                                        <li><?=$doc->DESC_TIPO_DOCUMENTO?>:
                                            <a target="_blank" href="<?=$sito['ASSI_URL_PUBBLICO']."/".$doc->URL_DOCUMENTO."/download"?>" >
                                            <i class="fa fa-<?=$this->load->view("sito/common/icona_file_fa",array("filename"=>$doc->NOME_FILE),TRUE);?> fa-2x"></i>
                                            <?=($doc->TITOLO!=""?$doc->TITOLO:$doc->NOME_FILE)?>
                                            </a>
                                        </li>
                                <?  }?>
                            </ul> 
                        </div>
                    </div>
                    <!-- CARD DOCUMENTI FINE-->
                </div>
            </div>
            <?}?>
            <? if($show_contraente){ ?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD CONTRAENTE -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-user mr-1" aria-hidden="true"></i> Contraente</h5>
                        </div>
                        <div class="card-body"> 
                            <dl class="">
                                <? $valori=array();
                                $valori[]=array("label"=>"Nominativo","valore"=>$POLIZZA->NOMINATIVO);
                                $valori[]=array("label"=>"Codice Fiscale","valore"=>$POLIZZA->CODICEFISCALE);
                                if($POLIZZA->INDIRIZZO!=""){
                                    $valori[]=array("label"=>"Indirizzo","valore"=>$POLIZZA->INDIRIZZO);
                                    $valori[]=array("label"=>"Comune","valore"=>$POLIZZA->COMUNE);
                                    $valori[]=array("label"=>"Cap","valore"=>$POLIZZA->CAP);    
                                }
                                foreach($valori as $valore){
                                    ?>
                                    <dt><?=$valore["label"]?></dt>
                                    <dd><?=$valore["valore"]?></dd>
                                    <?
                                }
                                ?>
                            </dl> 
                        </div>
                    </div>
                    <!-- CARD CONTRAENTE FINE-->
                </div>
            </div>
            <?}?>
            <? if(in_array("scadenze",$funzioni_abilitate) && !empty($POLIZZA->scadenze)){?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD SCADENZE -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-calendar mr-1" aria-hidden="true"></i> Scadenze</h5>
                        </div>
                        <div class="card-body">
                            <ul>
                                <?  foreach($POLIZZA->scadenze as $TIT) {
                                        $dtEffSca = new DateTime($TIT->DATA_EFFETTO);
                                        //$premioSca = number_format( $TIT->PREMIO_LORDO_DA_DIREZIONE ,2 , "," , "."); 
                                        $premioSca = number_format( $TIT->PREMIO_TOTALE ,2 , "," , "."); 
                                        $str_goto = '<a class="btn-loading" href="/titoli/titolo/'.$TIT->ID_TITOLO.'"><u>'.$dtEffSca->format('d/m/Y').'</u></a>'; 
                                        
                                        $suffisso_msg = " per un importo di &euro;.".$premioSca;
                                        if($TIT->IS_SCADENZA_ANNUALITA==1 && $TIT->TIPO_RAMO=="A" && !$vedi_premio_auto_rin){
                                            $suffisso_msg = " ( ".$TIT->DESC_SCADENZA_ANNUALITA." )";
                                        }   
                                       
                                        if(in_array("scadenze.premio",$funzioni_abilitate)){
                                            $str_li = "Scadenza arretrata in data ".$str_goto." ".$suffisso_msg;
                                        }
                                        else{
                                            $str_li = "Scadenza arretrata in data ".$str_goto;
                                        }
                                        ?>
                                        
                                        <li><?=$str_li?></li>
                                <?  }?>
                            </ul> 
                        </div>
                    </div>
                    <!-- CARD SCADENZE FINE-->
                </div>
            </div>
            <? } ?>

            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD GARANZIE -->
                    <div class="card card-dettaglio " >
                        <div class="card-header">
                        <h5><i class="fa fa-book-open mr-1" aria-hidden="true"></i> Garanzie</h5>
                        </div>
                        <div class="card-body">
                            <ul>
                                <?  foreach($POLIZZA->garanzie as $GAR) {?>
                                        <li><?=$GAR->DESC_GARANZIA?></li>
                                <?  }?>
                            </ul> 
                        </div>
                    </div>
                    <!-- CARD GARANZIE FINE-->
                </div>
            </div>
        </div>
    </div>    
    <? if(!empty($POLIZZA->contatti_utili)) { ?>
        <div class="row pt-3">
        <div class="col">
            <div class="card w-100" >
                <div class="card-header">Contatti Utili</div>
                <div class="card-body">
                    <?
                    $aCapo = true;
                    $contaContatti=0;
                    $quanti_per_riga = 3;
                    $i = count($POLIZZA->contatti_utili);
                    if($i<$quanti_per_riga){
                        $quanti_per_riga = $i;
                    }
                    if($i % 4 == 0){
                        $quanti_per_riga = 4;
                    }
                    
                    foreach($POLIZZA->contatti_utili as $C){
                        if($aCapo){
                            if($contaContatti>0){
                                //chiusura div row precedente
                                ?>
                                </div><!-- chiusura div row precedente -->
                                <?
                            }
                            ?>
                                <div class="row"> <!--div row -->
                            <?
                        }
                        $contaContatti++;
                        $aCapo = (($contaContatti % $quanti_per_riga)==0);
                        ?>
                        <div class="col-md-<?=(12/$quanti_per_riga)?> d-flex align-items-stretch pt-2">
                            <div class="card card-dettaglio w-100" >
                                <div class="card-header">
                                    <strong>
                                    <?=$C->TITOLO?>
                                    </strong>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <?
                                        $sTMP=$C->CONTATTO_UTILE;
                                        //TEST EMAIL
                                        if(strpos($C->CONTATTO_UTILE,"@")){
                                            $sTMP="<a href=\"mailto:$C->CONTATTO_UTILE\">$C->CONTATTO_UTILE</a>";
                                        }
                                        //TEST SITO
                                        if(filter_var($sTMP, FILTER_VALIDATE_URL) !== false){
                                            $testoLink =  preg_replace( "#^[^:/.]*[:/]+#i", "", $sTMP );
                                            $sTMP="<a target=\"_blank\" href=\"$C->CONTATTO_UTILE\">$testoLink</a>";
                                        }
                                        echo $sTMP;
                                        ?>
                                    </div>
                                    <div class="row small">
                                        <?=$C->DESCRIZIONE?>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?}
                    if($contaContatti>0){
                        //chiusura div row precedente
                        ?>
                        </div>
                        <?
                    }?>
                </div>
            </div>
        </div>
    </div>
    <? } ?>
</div>
