<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');


    function get_parametro_hi($nome_parametro,$default_value=""){
        $ci = &get_instance();
        $all_config_obj = isset($ci->datiSito["all_config_obj"])?$ci->datiSito["all_config_obj"]:new stdClass();
        $hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
        $retval = isset($hiPar->{$nome_parametro})?$hiPar->{$nome_parametro}:$default_value;
        return $retval;
    }


?>
