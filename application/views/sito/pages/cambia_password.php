<? 
$all_config_obj = $sito['all_config_obj'];
$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();

$logo= isset($hiPar->link_logo)?$hiPar->link_logo:"";  
$titolo= isset($hiPar->titolo)?$hiPar->titolo:"";  
$sottotitolo= isset($hiPar->sottotitolo)?$hiPar->sottotitolo:"";  
?>
<div class="container-fluid h-100 homepage">
    <div class="d-flex justify-content-center h-90" >
        <div class="d-flex flex-column logincard rounded">
        <div class="logincard-header pt-2">
                <? if($logo){ ?>
                <span class="d-flex justify-content-center align-middle main-font py-2">
                    <img class="img-fluid mr-4" src="<?=$sito['logo']?>" height="60">
                </span>
                <? }else{ ?>
                <h3 class="d-flex justify-content-center align-middle main-font">
                       <?=$titolo?>
                </h3>
                <!--<h5 class="d-flex justify-content-center align-middle main-font">
                       <?=$sottotitolo?>
                </h5>-->
                <? } ?>
            </div>      
            <div class="collapse show" id="container-login">
                <div class="logincard-header">
                    <h3 class="d-flex justify-content-center main-font">Cambio Password</h3>
                </div>
                <div class="logincard-body px-4">
                    <div class="input-group form-group">
                      
                    <div class="input-group-prepend">
                        <input id="changeForm-token" type="hidden" class="form-control" name="changeForm-token" 
                                    value="<?php echo (isset($page_data['token_verifica'])?$page_data['token_verifica']:"")?>" >
                        
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="nuova password" id="changeForm-pass">
                        
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-redo"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="ripeti nuova password" id="changeForm-pass2">
                    </div>
                    <div class="d-flex justify-content-center form-group">
                        <input type="button" value="Conferma" class="btn login_btn" onclick="doCambiaPassword()">
                    </div>
                        <div class="alert alert-info alert-info-custom collapse" role="alert" id="changeForm-msg"></div>
                        <div class="spinner-border text-primary float-center collapse" id="changeForm-loading" role="status">
                        
                    </div>
                </div>
                <div class="logincard-footer">
                    <div class="d-flex justify-content-center">
                        <a href="/" >Vai a Home Page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function doCambiaPassword(){
    var prefisso="changeForm";
    var new_password = $("#"+prefisso+"-pass").val();
    var new_password_repeat = $("#" + prefisso + "-pass2").val();
    var token_verifica = $("#"+prefisso+"-token").val();

    if(new_password!=new_password_repeat){
        $("#" + prefisso + "-msg").html("<strong>Oops!</strong>[Nuova password] non coincide con [Ripeti Nuova password]");
        $("#" + prefisso + "-msg").show();
        $("#" + prefisso + "-loading").hide();
        return false;
    }

    $("#"+prefisso+"-msg").hide();
    $("#"+prefisso+"-loading").show();
    $.ajax({
        type: "POST",
        url: "/login/execcambiopassword",
        data: {token_verifica:token_verifica,new_password:new_password,new_password_repeat:new_password_repeat},
        success: function(response)
        {
            var res = eval('('+response+')');
            if(res.success){
                $("#"+prefisso+"-msg").html("<strong>Success!</strong> la password è stata cambiata. "+ res.message);
                $("#"+prefisso+"-msg").show();
                $("#"+prefisso+"-loading").hide();
            }
            else{
                $("#"+prefisso+"-msg").html("<strong>Spiacenti!</strong>"+ res.message);
                $("#"+prefisso+"-msg").show();
                $("#"+prefisso+"-loading").hide();
            }
        },
        error: function()
        {
            $("#"+prefisso+"-msg").html("<strong>Errore!</strong>");
            $("#"+prefisso+"-msg").show();
            $("#"+prefisso+"-loading").hide();
        }
    });
}
</script>        