<?php

require_once "Base_rest_model.php";

class Sinistri_model extends Base_rest_model
{
    const SIN_GAR_INFORTUNIO="INF";
    const SIN_GAR_DANNO="DAN";

    const SIN_RUOLO_ALUNNO="ALUNNO";
    const SIN_RUOLO_ALUNNO_H="ALUNNO_H";
    
    const SIN_RUOLO_DOCENTE="DOCENTE";
    const SIN_RUOLO_DOCENTE_SOS="DOCENTE_SOS";
    
    const SIN_RUOLO_ATA="ATA";
    const SIN_RUOLO_TERZO="TERZO";
    const SIN_RUOLO_ALTRO="ALTRO";

    const SIN_RUOLO_GENITORE="GENITORE";

    public function costanti(){
		$c = array();
		$c['SIN_GAR_INFORTUNIO']= self::SIN_GAR_INFORTUNIO;
		$c['SIN_GAR_DANNO']= self::SIN_GAR_DANNO;
		return $c;
    }
    
    public function get_sinistri($params=array()){
        $ret = $this->CH->get_sinistri($params);
        $ELENCO = $this->CH->extract_data($ret);
        return $ELENCO;
    }
    public function get_sinistro($p){
        $ret = $this->CH->get_sinistri($p);
		$OBJ = $this->CH->extract_first_data($ret);
        return $OBJ;
    }

    public function get_sinistro_vuoto($params = array())
    {
        
        $id_polizza = isset($params['ID_POLIZZA'])?$params['ID_POLIZZA']:0;
      
        $POL = new stdClass();
        $this->load->model("Polizze_model");
        $PMOD = new Polizze_model($this->CH);
        $p=array();
        if($id_polizza==0){
            $p['SOLO_VIVE']=1;
        }
        else{
            $p['ID_POLIZZA']=$id_polizza;
        }
        $ret_polizze = $PMOD->get_polizze($p);
        if(!empty($ret_polizze)){
            $POL = $ret_polizze[0];
            $id_polizza = $POL->ID_POLIZZA;
        }
       
        $vuoto = new stdClass();
		$vuoto->ID_SINISTRO = 0;
        $vuoto->ID_POLIZZA =$id_polizza;
        $vuoto->NUMERO_POLIZZA =isset($POL->NUMERO_POLIZZA)?$POL->NUMERO_POLIZZA:"";
        
        $vuoto->NUMERO_SINISTRO ="";
        $vuoto->DATA_SINISTRO ="";//date("Y-m-d");
        $vuoto->DATA_DENUNCIA =date("Y-m-d");
        $vuoto->ESERCIZIO =date("Y");
        $vuoto->LUOGO_ACCADIMENTO ="";
        $vuoto->MACRO_AVVENIMENTO ="";
        $vuoto->DINAMICA ="";
        $vuoto->COD_GARANZIA_SINISTRO ="";//self::SIN_GAR_DANNO;

        $key="DANNEGGIATO";
        $vuoto->{"PROGRESSIVO_SOGGETTO_$key"}=0;
        $vuoto->{"ABBR_TIPO_SOGGETTO_$key"}="ALUNNO";
        $vuoto->{"NOMINATIVO_$key"}="";
        $vuoto->{"CODICEFISCALE_$key"}="";
        $vuoto->{"TEL_CASA_$key"}="";
        $vuoto->{"EMAIL_$key"}="";
        $key="GENITORE";
        $vuoto->{"PROGRESSIVO_SOGGETTO_$key"}=0;
        $vuoto->{"ABBR_TIPO_SOGGETTO_$key"}="GENITORE";
        $vuoto->{"NOMINATIVO_$key"}="";
        $vuoto->{"CODICEFISCALE_$key"}="";
        $vuoto->{"TEL_CASA_$key"}="";
        $vuoto->{"EMAIL_$key"}="";
        


		$vuoto->valid = false;
        $vuoto->messages=array();
        if($id_polizza==0){
			$vuoto->messages["POLIZZA"]="Nessuna polizza attiva per comunicazione evento";
		}
        

        return $vuoto;
    }
    
    public function put_sinistro($sinistro){
        $ret = $this->CH->put_sinistro($sinistro);
        return $ret;
    }
    
    public function put_sinistro_soggetto($soggetto){
        $ret = $this->CH->put_sinistro_soggetto($soggetto);
        return $ret;
    }

    public function put_sinistro_allegato($file_info){
        $ret = $this->CH->put_sinistro_allegato($file_info);
        return $ret;
    }
    
    
    public function stampa($sinistro){
        $ret = $this->CH->stampa_sinistro($sinistro);
        return $ret;
    }
}
