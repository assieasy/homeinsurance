<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	
	function __construct()
    {
		parent::__construct();
	}
	
	public function index()
	{
		var_dump(__METHOD__);
	}
	
	public function autenticazione()
	{
		$dati = array();
		$dati['username'] = $_POST['username'];
		$dati['password'] = $_POST['password'];
		if(filter_var($_POST['username'], FILTER_VALIDATE_EMAIL)){
			$dati['email'] = $_POST['username'];
		}
		$ret = $this->assi->login($dati);
		
		if(isset($ret) && $ret->success){
			$assi_token = $ret->data->TOKEN;

			//CALCOLO LA DURATA DEL COOKIE IN BASE A expire_date DEL TOKEN
			$durata_cookie = $this->durata_cookie_secs;
			$scadenza_token = isset($ret->token_status->expire_date)?$ret->token_status->expire_date:"";
			if($scadenza_token){
				$AA = strtotime($scadenza_token);
				$durata_cookie = $AA - time();
			}
			if(!($durata_cookie>0)){
				$durata_cookie = $this->durata_cookie_secs;
			}
			$this->load->helper("cookie");
			set_cookie("assi_token",$assi_token, $durata_cookie);
			$anag = $this->assi->get_utente();
			$anag->assi_token = $assi_token;
		}
		echo json_encode($ret);
	}
	public function logout()
	{
		$this->load->helper("cookie");
	    $assi_token = delete_cookie("assi_token");
		//$this->session->unset_userdata("assi_token");
		header('Location: /');
	}

	// ********** GESTIONE CAMBIO PASSWORD ********


	/**
	 * Riceve email in post.
	 * Richiede ad assieasy di inviare una mail con link per cambio password
	 *
	 * @var	array 
	 * @return	json
	 */
	public function mailcambiopassword()
	{
		$dati = array();
		$dati['email'] = $_POST['email'];
		$dati['callback_url'] = $this->datiSito['SITE_URL']."/login/cambiopassword";
		
		$datiSito =$this->datiSito;
		$sTMP = isset($datiSito['titolo'])?$datiSito['titolo']:"HomeInsurance";
		$dati['mail_object']=$sTMP." - Recupero password";
		$dati['mail_body']=$this->load->view('mail/recupero_password','',TRUE);
		$ret = $this->assi->mailcambiopassword($dati);
		echo json_encode($ret);
	}

	/**
	 * Riceve email nell'url il token per la verifica.
	 * espone la pagina di cambio password
	 *
	 * @var	array 
	 * @return	view
	 */
	public function cambiopassword($token_verifica){
		$page_config=array();
		$page_config['titolo']="Utente - Cambio Password";
		
		$page_data = array();
		$page_data['token_verifica']=urldecode($token_verifica);
		$this->show("cambia_password",$page_config,$page_data);
	}
	
	/**
	 * Riceve token_verifica e new_password in post.
	 * Richiede ad assieasy di aggiornare la password
	 *
	 * @var	array 
	 * @return	json
	 */
	public function execcambiopassword(){
		$dati=array(); 
		$dati['token_verifica']=$_POST['token_verifica'];
		$dati['new_password']=$_POST['new_password'];
		$dati['new_password_repeat']=$_POST['new_password_repeat'];
		
		if($dati['new_password']!=$dati['new_password_repeat']){
			$ret = array();
			$ret['success']=false;
			$ret['message']="[Nuova password] non coincide con [Ripeti Nuova password]";
		}
		else{
			$ret = $this->assi->execcambiopassword($dati);
		}
		
		echo json_encode($ret);
	}

	/**
	 * Riceve new_password e new_password_repeat in post.
	 * Richiede ad assieasy di aggiornare la password
	 *
	 * @var	array 
	 * @return	json
	 */
	public function updatepassword(){
		$dati=array(); 
		$dati['new_password']=$_POST['new_password'];
		$dati['new_password_repeat']=$_POST['new_password_repeat'];
		$ret = $this->assi->updatepassword($dati);
		echo json_encode($ret);
	}


	/**
	 * Riceve id nell'url il token per la verifica.
	 * espone la pagina di cambio password
	 *
	 * @var	array 
	 * @return	view
	 */
	public function registrazione($chiave_registrazione){

		if(!empty($this->utente)){
			//SE SONO GIA' LOGGATO TI PORTO ALLA HOME PAGE
			header('Location: /');
		}
		
		$page_config=array();
		$page_config['titolo']="Utente - Registrazione";
		
		$p = array();
		$p['chiave_registrazione']=urldecode($chiave_registrazione);
		$ret = $this->assi->ask_for_new_user($p);
		
		
		$page_data['procedi'] = isset($ret->success)?$ret->success:FALSE;
		$page_data['message'] = "";
		if($page_data['procedi']){
			$new_user_info = $this->assi->extract_data($ret);
		}
		else{
			$new_user_info = new stdClass();
			$page_data['message'] =  isset($ret->message)?$ret->message:"Errore di registrazione";
		}

		$page_data['new_user_info'] = $new_user_info;
		$this->show("registra_utente",$page_config,$page_data);
	}

}
