 <?
    $all_config_obj = $sito['all_config_obj'];
    $hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
    $vedi_premio_auto_rin = isset($hiPar->vedi_premio_auto_rin)?$hiPar->vedi_premio_auto_rin:0;  
    $vedi_premio_auto_rin = ($vedi_premio_auto_rin==1);
?>
<div class="container-fluid maincontainer pb-4">
    <?
    $pagina_origine=isset($page_data['pagina_origine'])?$page_data['pagina_origine']:"";
    $config=array(
        "border_class"=>"border-scadenze",
        "fa_icon"=>"calendar-check",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > Bonifici</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);

     ?>
    <!--
    <div class="row pt-1">
        <div class="ml-auto">
            <div class="btn-group card-dettaglio mr-4" role="toolbar"  aria-label="switch-visione">
                <a role="button" class="btn disabled btn-loading" href="/titoli/<?=$pagina_origine=="pagamenti"?"pagamenti":"scadenze"?>/?TIPO_ELENCO=TABELLA"><i class="fa fa-list" aria-hidden="true"></i></a>
                <a role="button" class="btn btn-loading" href="/titoli/<?=$pagina_origine=="pagamenti"?"pagamenti":"scadenze"?>/?TIPO_ELENCO=CARD"><i class="fa fa-table" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    -->
    <form id="form-sel-bonifici" method="post">  

    <input type="hidden" name="scadenze_selezionate" id="scadenze_selezionate" value="">

    <?foreach ($page_data['bonifici'] as $BONIFICO) {
        ?>
            <div class="row py-2">
                <div class="col">
                    <!-- CARD BONIFICO -->
                    <div class="card card-dettaglio " >
                        <div class="card-header">
                            <h5><i class="fa fa-university mr-2" aria-hidden="true"></i>Paga con un Bonifico
                                <? 
                                $polizze = "";
                                $conta_polizze = 0;
                                foreach ($BONIFICO->scadenze as $scad) {
                                    $conta_polizze++;
                                    $polizze .= ($polizze=="")?"":" ";
                                    $polizze .= $scad->NUMERO_POLIZZA;
                                }
                                if($conta_polizze>1){
                                    echo "le polizze ".$polizze;
                                }
                                else{
                                    echo "la polizza ".$polizze;
                                }
                                ?>
                            
                            </h5>
                        </div>
                        <div class="card-body justify-content-center">
                            <dl class="row">
                            <? foreach ($BONIFICO->canale->parametri as $key => $p) {
                                if($p->VALORE!=""){
                                ?>
                                    <dt class="col-3" ><?=$p->DESCRIZIONE?></dt>
                                    <dd class="col-9" >
                                        <? if($p->CHIAVE_PARAMETRO=="IBAN"){ ?>
                                            <span class="copy-target copy-remove-spaces"><?=$p->VALORE?></span>
                                            <button class="btn copy-btn ml-2" title="Copia"><i class="fa fa-copy mr-1" ></i></button>
                                        <?}
                                        else {?>
                                            <span><?=$p->VALORE?></span>
                                        <?}?>
                                        
                                    </dd>
                            <?  } 
                            }?>
                            <?  $causale = $BONIFICO->CAUSALE;
                                $importo = "&euro; ".number_format($BONIFICO->PREMIO_TOTALE,2,",","");
                            ?>
                                <dt class="col-3">Causale</dt>
                                <dd class="col-9"><span class="copy-target"><?=$causale?></span>
                                    <button class="btn copy-btn ml-2" title="Copia"><i class="fa fa-copy mr-1"></i></button>
                                </dd>
                                <dt class="col-3">Importo</dt>
                                <dd class="col-9"><?=$importo?></dd>
                            </dl>
                        </div>
                        
                    </div>
                    <!-- CARD BONIFICO FINE-->
                </div>
            </div>
    <?}?>
    <div class="row py-2">
        <div class="col">
            <div class="card card-dettaglio " >
                <? if(empty($page_data['titoli'])){?>
                    <div class="card-header">
                    <h5>Nessuna scadenza</h5>
                        Non ci risultano polizze in arretrato pagabili attraverso bonifico.
                    </div>
                <?} 
                else {
                ?>

                <div class="card-header">
                    <h5>Hai una o pi&ugrave; scadenze?</h5>
                    Scegli quali pagare e ricalcola per fare un solo bonifico.
                </div>
                <div class="card-body justify-content-center">
                
                    <div class="table-responsive col">
                        <table class="table table-sm table-hover small">
                        <thead>
                            <tr>
                            <th scope="col">Sel.</th>
                            <th scope="col">Compagnia</th>
                            <th scope="col">Numero Polizza</th>
                            <th scope="col">Prodotto</th>
                            <!-- <th scope="col">Tipo Polizza</th> -->
                            <th scope="col">Effetto</th>
                            <th scope="col">Stato</th>
                            <th scope="col">Premio</th>
                           
                            </tr>
                        </thead>
                        <tbody>
                        <?  foreach($page_data['titoli'] as $key => $TIT) {
                                $dtEff = new DateTime($TIT->DATA_EFFETTO);
                                ?>
                                <tr>
                                <td>
                                    <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="<?=$TIT->ID_TITOLO?>" 
                                            name="chkSCADENZA_<?=$TIT->ID_TITOLO?>" id="chkSCADENZA_<?=$TIT->ID_TITOLO?>"
                                            <?=($TIT->SELECTED==1?"checked":"")?>
                                            >
                                    </div>
                                    </div>
                                </td>
                                <td><?=$TIT->DESC_COMPAGNIA?></td>
                                <td><a class="btn-loading" href="/polizze/polizza/<?=$TIT->ID_POLIZZA?>" nowrap><i class="fa fa-id-card mr-1" aria-hidden="true"></i><?=$TIT->NUMERO_POLIZZA?></a></td>
                                <td><?=$TIT->DESC_PRODOTTO?></td>
                                <!-- <td><?=$TIT->COD_RAMO?></td> -->
                                <td><a class="btn-loading" href="/titoli/titolo/<?=$TIT->ID_TITOLO?>" nowrap><i class="fa fa-calendar-check mr-1" aria-hidden="true"></i><u><?=$dtEff->format('d/m/Y')?></u></a></td>
                                <td><?
                                $sTMP = $TIT->DESC_STATO_TITOLO;
                                if($TIT->STATO_TITOLO==10){
                                    $sTMP = "Incassato da contabilizzare";
                                }
                                echo $sTMP?></td>
                                <?  $importo = "";
                                    if(in_array("scadenze.premio",$funzioni_abilitate) || $TIT->STATO_TITOLO>2 ) {
                                            //SE ARRETRATO FACCIO VEDERE IMPORTO SOLO SE "scadenze.premio" E' ABILITATA
                                        //$importo = "&euro; ".number_format($TIT->PREMIO_LORDO_DA_DIREZIONE,2,",",".");
                                        if($TIT->IS_SCADENZA_ANNUALITA==1 && $TIT->TIPO_RAMO=="A" && !$vedi_premio_auto_rin){
                                            $importo = $TIT->DESC_SCADENZA_ANNUALITA;
                                        }
                                        else{
                                            $importo = "&euro; ".number_format($TIT->PREMIO_TOTALE,2,",",".");
                                        
                                        }
                                    } 
                                ?>
                                <td nowrap><?=$importo?></td>
                               
                                </tr>
                                
                        <?  }?>
                        </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body justify-content-center text-center">
                    <a href="#" 
                        onclick="printBonifici()" 
                        loadingSecondi="40" 
                        class="d-flex justify-content-center btn btn-primary btn-loading">Ricalcola gli estremi del bonifico</a>
                </div>
                <?}
                ?>
            </div>
        </div>
    </div>
    </form>
   <!-- <div class="card card-dettaglio " >
        <div class="card-body justify-content-center text-center">
            <a href="#" 
                onclick="printBonifici()" 
                loadingSecondi="40" 
                class="d-flex justify-content-center btn btn-primary btn-loading">Visualizza le Coordinate Bancarie</a>
        </div>
        
    </div>-->
</div>

<script>
/*************** funzioni init ***********/
pageinitfunctions = pageinitfunctions?pageinitfunctions:[];
/*************** funzioni init ***********/

function printBonifici( ){
    
    var frmName = "form-sel-bonifici";
    var scadenze_selezionate = "";
    $("#" + frmName + " input[type=checkbox]:checked").each(function(index){
        var input = $(this);
       if(scadenze_selezionate!=""){
            scadenze_selezionate+=",";
        }
        scadenze_selezionate+=input.attr('value');
    });
   
    $("#" + frmName + " #scadenze_selezionate").val(scadenze_selezionate);
    //if(scadenze_selezionate!=""){
        $('#'+frmName).attr("action","/bonifici");
        $('#'+frmName).attr("target","");
        $('#'+frmName).submit();
    /*}
    else{
        stop_spinner();
    }*/
    
    return false;
}

</script>

