<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Custom confguration options
 */

$config['assieasy_url'] = "https://futura.assieasy.com/assieasy/";
$config['rest_url'] = $config['assieasy_url']."clienti/";
$config['chiave_hi'] = 'ASSIEASY';
$config['sito'] = array("titolo"=>"Futura scuole"
                        ,"descrizione"=> "Futura scuole per gestire l'assicurazione scolastica"
                        ,"autore"=> "Save S.r.l."
                        ,"assieasy_url"=>$config['assieasy_url'] 
                        ,"rest_url"=>$config['rest_url'] 
                        ,"chiave_hi"=>$config['chiave_hi'] 
                        );
