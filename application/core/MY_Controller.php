<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * MY_Controller estensione di CI_Controller , 
 * le classi che estendono MY_Controller hanno a disposizione
 * la proprieta $SessionUser 
 * alla login
 * 
 *
 */
class MY_Controller extends CI_Controller {
    /**
     * @var stdClass
     */
    public $utente;
    public $datiSito = array();
    public $msg_auenticazione_errata = "";
    /**
     * @var Data_assi_model
     */ 
    protected $assi;
    /**
     * @var int
     */
    protected $durata_cookie_secs = 365*24*60*60;
	
	function __construct()
    {
        parent::__construct();
        $this->load->helper("sito");
        //cerco in db chi è il cliente in base all'url chiamante
        $this->load->model("Assi_switch_model");
        $STCH = new Assi_switch_model();
        $chiave = isset($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:"";
        $assieasy_config = $STCH->find_config_da_url($chiave);
        if(empty($assieasy_config)){
            echo "configurazione non corretta";
            die();
        }

        //chiedo a assieasy le configurazioni del sito .....
        $this->load->helper("cookie");
	    $assi_token = get_cookie("assi_token");
       
        $this->load->model("Data_assi_model");
        $this->assi = new Data_assi_model();
        $this->assi->set_assi_url($assieasy_config['ASSI_URL']);
        
        $this->datiSito = $assieasy_config;
        $ret = $this->assi->get_configurazioni_sito(); 
        foreach($ret as $key => $val){
            $this->datiSito[$key]=$val;
        }
        $this->assi->set_token($assi_token);
       
      
        $this->utente = $this->assi->utente();
        if(empty($this->utente)){
            $this->utente = "";
            $this->msg_auenticazione_errata =  $this->assi->get_msg_auenticazione_errata();
        }

    }

    protected function get_tipo_elenco(){
        $this->load->helper("cookie");
		$tipo_elenco = isset($_POST['TIPO_ELENCO'])?$_POST['TIPO_ELENCO']:"";
        $tipo_elenco = isset($_GET['TIPO_ELENCO'])?$_GET['TIPO_ELENCO']:get_cookie("TIPO_ELENCO");
        set_cookie("TIPO_ELENCO",$tipo_elenco, $this->durata_cookie_secs);
        return $tipo_elenco;
    }
    protected function controllo_privacy(){

        $ok = true;
        if(empty($this->utente)){
            //se non sono loggato mi va bene....
            return $ok;
        }
        

        $privacy_ok=($this->utente->PRIVACY>0);
        $privacy_revocata=($this->utente->PRIVACY_REVOCA>0);
        if(!$privacy_ok){
            $ok = false;
            //echo "Privacy non convermata!!!!";die();
        }
        if($privacy_revocata){
            //echo "Privacy revocata!!!!";die();
            $ok = false;
        }

        return $ok;
    }
    
    protected function build_page_data($page_config=array(),$dati=array()){
        $pagedata = array();
		$pagedata['title']=isset($page_config['titolo'])?$page_config['titolo']:$this->datiSito['titolo'];
		$pagedata['sito']=$this->datiSito; 
        $pagedata['utente']=$this->utente;
        $pagedata['funzioni_abilitate']=$this->funzioni_abilitate();
        foreach($page_config as $key=> $value){
            $pagedata[$key] = $value;
        }
        $pagedata['page_data']=$dati;

        return $pagedata;
    }
    public function load_view($page="home_page",$page_config=array(),$dati=array())
	{
        $pagedata = $this->build_page_data($page_config,$dati);
        $shtml = $this->load->view($page,$pagedata,true);
        return $shtml;
    }


    public function show($page="home_page",$page_config=array(),$dati=array())
	{
        $pagedata = $this->build_page_data($page_config,$dati);
 
        $this->load_header_views($pagedata);
        $this->load->view('sito/pages/'.$page,$pagedata);
        $this->load_footer_views($pagedata);
    }
    
    protected function load_header_views($pagedata){
        $this->load->view('sito/common/headers',$pagedata);
        if(!empty($this->utente)){
            $this->load->view('sito/common/navbar',$pagedata);
        }
    }
    protected function load_footer_views($pagedata){
        $TMP=isset($pagedata['footer_templates'])?$pagedata['footer_templates']:array();
        foreach($TMP as $tpl){
            $this->load->view("sito/common/$tpl",$pagedata);
        }
        $this->load->view('sito/common/footer_include',$pagedata);
        $this->load->view('sito/common/a2hs_script',$pagedata);
        $this->load->view('sito/common/footer_end',$pagedata);
        
    }

    protected function is_funzione_abilitata($funzione){
        $funzione = strtolower($funzione);
        $f = $this->funzioni_abilitate();        
        $ok = in_array($funzione,$f);
        
        return $ok;
    }
    protected function funzioni_abilitate(){
        $retF = array();
        if(!empty($this->utente)){
            $abilitazioni = $this->utente->abilitazioni;
            foreach($abilitazioni as $rec){
                if($rec->ABILITATA>0){
                    $retF[]=strtolower($rec->FUNZIONE);
                }
            }
            
        }
        return $retF;
    }

    protected function get_parametro_hi($nome_parametro){
        return get_parametro_hi($nome_parametro);
    }
}