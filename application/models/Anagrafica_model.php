<?php

require_once "Base_rest_model.php";

class Anagrafica_model extends Base_rest_model
{
    const CONTATTI_ULTILI_CONTESTO_MAIN = 1;
    const CONTATTI_ULTILI_CONTESTO_DETTAGLIO = 2;
    const CONTATTI_ULTILI_CONTESTO_BENVENUTO = 3;
    const CONTATTI_ULTILI_PREVIEV_CONTESTO_BENVENUTO = 4;
    const CONTATTI_ULTILI_FOOTER_CUSTOM = 5;
    
    public function get_anagrafica($params=array()){
		$ret = $this->CH->get_anagrafica_privacy($params);
		$r = $this->CH->extract_first_data($ret);
        return $r;
    }
    public function get_privacy($params=array()){
        $ret = $this->CH->get_anagrafica_privacy($params);
		$r = $this->CH->extract_first_data($ret);
        return $r;
    }

    public function get_contatti_utili($params=array()){
        $ret = $this->CH->get_contatti_utili($params);
        $r = $this->CH->extract_data($ret);
        return $r;
    }

    public function put_privacy($params=array()){
       
        $ret = $this->CH->put_anagrafica_privacy($params);
        return $ret;
    }
   
    public function update_anagrafica_hi($params=array()){
       
        $ret = $this->CH->update_anagrafica_hi($params);
        return $ret;
    }
    
}
