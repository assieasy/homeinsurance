<? 
$all_config_obj = $sito['all_config_obj'];
$hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();

$logo= isset($hiPar->link_logo_nav)?$hiPar->link_logo_nav:"";
        
?>
<nav class="navbar navbar-expand-lg navbar-hi shadow">
   <div class="container main-font">
      <a class="navbar-brand btn-loading" href="/">
        <?if($logo!="") { ?>
          <img src="<?=$logo?>" height="30" class="d-inline-block align-top" alt="">
        <?}else{?>
          <i class="fa fa-home fa" aria-hidden="true"></i>
        <?}?>
        <?=(isset($sito['titolo'])?$sito['titolo']:"Home Insurance")?>
      </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon">
          <i class="fas fa-bars" style="font-size:28px;"></i>
          </span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link btn-loading" href="/">Home
                <span class="sr-only">(current)</span>
              </a>
        </li>
        <li class="nav-item">
          <a class="nav-link btn-loading" href="/polizze/polizze" >Polizze</a>
        </li>
        <? if(in_array("scadenze",$funzioni_abilitate)){?>
        <li class="nav-item">
          <a class="nav-link btn-loading" href="/titoli/scadenze" >Scadenze</a>
        </li>
        <?}?>
        <? if(in_array("pagamenti",$funzioni_abilitate) ){?>
        <li class="nav-item">
          <a class="nav-link btn-loading" href="/titoli/pagamenti" >Pagamenti</a>
        </li>
        <?}?>
        <? if(in_array("sinistri",$funzioni_abilitate)){?>
        <li class="nav-item">
          <a class="nav-link btn-loading" href="/sinistri/sinistri" >Sinistri</a>
        </li>
        <?}?>

       
          
        <!--
        <li class="nav-item">
          <a class="nav-link" href="/sinistri/sinistri">Sinistri</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/documenti/documenti">Documenti</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/preventivi/regolazione">Regolazione</a>
        </li>
        -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarProfilo" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Profilo
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarProfilo">
                <a class="dropdown-item btn-loading" href="/utente/account">Il mio account</a>
               <!-- <a class="dropdown-item" href="/utente/anagrafica">Anagrafica</a> -->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item btn-loading"  href="/login/logout">Esci</a>
            </div>
        </li>
        <li class="nav-item">
          <a class="nav-link btn-loading"  href="/login/logout">Esci</a>
        </li>
      
        <?
        $fbook= isset($hiPar->social_facebook)?$hiPar->social_facebook:"";
        $lkin= isset($hiPar->social_linkedin)?$hiPar->social_linkedin:"";
        $twit= isset($hiPar->social_twitter)?$hiPar->social_twitter:"";

        if($fbook!="" || $lkin!=""){
        ?>
        <li class="nav-item">
        <ul class="navbar-nav flex-row">
          <?
          if($fbook!=""){
          ?>
            <li class="nav-item"><a class="nav-link px-2" target="_blank" href="<?=$fbook?>"><i class="fab fa-facebook"></i></a></li>
          <?}?>

          <?
          if($lkin!=""){
          ?>
            <li class="nav-item"><a class="nav-link px-2" target="_blank" href="<?=$lkin?>"><i class="fab fa-linkedin"></i></a></li>
          <?}?> 
          
          <?
          if($twit!=""){
          ?>
            <li class="nav-item"><a class="nav-link px-2" target="_blank" href="<?=$twit?>"><i class="fab fa-twitter"></i></a></li>
          <?}?> 
           
        </ul>
        </li>
        <?}?>
      </ul>
    </div>
  </div>
</nav>
