<?php

require_once "Assi_rest_model.php";

class Base_rest_model extends CI_Model
{
    /*
        @var Assi_rest_model $CH 
        e' il canale di comunicazione con Assieasy
        quando vengono istanziate le classi di tipo "Base_rest_model"
        deve essere passato il canale ( $this->assi )

        esempio nel controller welcome.php :
        $this->load->model("Polizze_model");
        $PMOD = new Polizze_model($this->assi);
        
        ..  oppure si puo' chamare successivamente il metodo init:
        $this->load->model("Polizze_model","poli");
        $this->poli->init($this->assi);
    */

    /**
     * @var Data_assi_model
     */   
    protected $CH;
   
    function __construct(Data_assi_model $rest_channel=null)
    {
        parent::__construct();
        $this->init($rest_channel);
    }
    
    function init(Data_assi_model $rest_channel=null)
    {
        $this->CH = $rest_channel;
    }

    protected function validate_channel(){
        if(!$this->CH){
            $parent = get_called_class();
            throw new \Exception($parent." - ".__METHOD__." - rest_channel non inizializzato nel model");
        }
    }
    function debug()
    {
        $this->validate_channel();
        $this->CH->debug();
    }
    function last_log()
    {
        $this->validate_channel();
        return $this->CH->last_log();
    }
    function get_last_response()
    {
        $this->validate_channel();
        return $this->CH->get_last_response();
    }
    
    protected function get_allegati($params){
        $retAllegati = array();
        if(isset($params["files"])){
            foreach($params["files"] as $file_up){
                $TMP=array();
                $tmp_file_path = isset($file_up['tmp_name'])?$file_up['tmp_name']:"";
                if($tmp_file_path!= "" && is_file($tmp_file_path)){
                    $TMP['FILE_NAME']= $file_up['name'];
                    $TMP['FILE_CONTENT']=base64_encode(file_get_contents($file_up['tmp_name']));
                    $TMP['DESC_TIPO_DOCUMENTO']= "";
                    $TMP['TITOLO']= "";
                    $TMP['VISIBILE_HOME_INS']= 1;
                    $TMP['info']=  $file_up;
                    
                }

                if(!empty($TMP)){
                    $retAllegati[]= $TMP;
                }
            }
        }
       return $retAllegati;
    }
}
