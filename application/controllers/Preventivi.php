<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preventivi extends MY_Controller {
	protected $PVMOD;
	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		if(!$this->controllo_privacy()){
			header('Location: /privacy');
		}
		$this->load->model("Preventivi_model");
		$this->PVMOD = new Preventivi_model($this->assi);
	}
	public function index()
	{
		$this->variazioni();
	}
	public function variazioni()
	{
		$page_config=array();
		$page_config['titolo']="Utente";
		$page_data = array();

		//cerco se ho una regolazione in sospeso....
		$this->load->model("Preventivi_model");
		$PREVMOD = new Preventivi_model($this->assi);
		$page_data['const']= $PREVMOD->costanti();
		$page_data['regolazione']= $PREVMOD->get_regolazione();

		$consenti_nuova_appendice = TRUE;
		//cerco se ho una appendice in sospeso....
		$APPENDICI= $PREVMOD->get_appendici_in_sospeso();
		foreach($APPENDICI as $app_in_sospeso){
			if($app_in_sospeso->STATO_PREVENTIVO == Preventivi_model::PREVENTIVO_IN_COMPILAZIONE){
				$consenti_nuova_appendice = FALSE;
			}
		}
		$page_data['appendici_aperte']= $APPENDICI;
		if($consenti_nuova_appendice){
			$page_data['nuova_appendice']= $PREVMOD->get_appendice_vuota();
		}

		//storico regolazioni 
		$p=array();
		$p['limit']=50;//ULTIMI 50
		$p['sorts']=array();
		$p['sorts'][]=array("column"=>"DATA_RIFERIMENTO","order"=>"DESC");
		$p['sorts'][]=array("column"=>"PROGRESSIVO","order"=>"DESC");
		$p['SOLO_VIVE']=1; // SOLO_VIVE = filtro su preventivi non abbinati o abbinati a polizze vive
		$REGOLAZIONI= $PREVMOD->get_regolazioni_tutte($p);
		$page_data['regolazioni_tutte']= $REGOLAZIONI;

		//storico appendici 
		$p=array();
		$p['limit']=50;//ULTIMI 50
		$p['sorts']=array();
		$p['sorts'][]=array("column"=>"DATA_RIFERIMENTO","order"=>"DESC");
		$p['sorts'][]=array("column"=>"PROGRESSIVO","order"=>"DESC");
		$p['SOLO_VIVE']=1; // SOLO_VIVE = filtro su preventivi non abbinati o abbinati a polizze vive
		$APPENDICI= $PREVMOD->get_appendici_tutte($p);
		$page_data['appendici_tutte']= $APPENDICI;

		
		$this->show("user_variazioni",$page_config,$page_data);
	}
	
	public function regolazione()
	{	
		$azione = isset($_POST['AZIONE'])?$_POST['AZIONE']:"";
		$progressivo_regolazione = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:0;

		switch($azione){
			case "ADDTESTA":
				$this->add_testa();
			break;
			case "SALVA":
				$this->write_regolazione();
			break;
			case "CONFERMA":
				$this->write_regolazione();
				$this->conferma_regolazione();
			break;
			case "STAMPA":
				$this->stampa_regolazione();
				die();
			break;
		}

		
		$page_config=array();
		$page_config['titolo']="Regolazione";
		$page_config['js_files']=array();
		$page_config['js_files'][]="jquery.tabledit/jquery.tabledit.mod.js";
		//$page_config['js_files'][]="user_regolazione.js";
		$page_config['css_files']=array();
		
		

		$page_data = array();
		$page_data['const']= $this->PVMOD->costanti();
		$page_data['regolazione']= $this->PVMOD->get_regolazione($progressivo_regolazione);

		$this->show("user_regolazione",$page_config,$page_data);
	}
	
	protected function add_testa($progressivo_appendice=0)
	{
		$input = filter_input_array(INPUT_POST);

		if($progressivo_appendice>0){
			$input['PROGRESSIVO'] = $progressivo_appendice;
		}

		$testa =array();
		$testa['AZIONE']= "INSERT";
		$testa['PROG_PREVENTIVO']= $input['PROGRESSIVO'];
		$testa['PROG_TESTA']= 0;
		$testa['COGNOME']= $input['TESTA_COGNOME'];
		$testa['NOME']= $input['TESTA_NOME'];
		$testa['CODICEFISCALE']= $input['TESTA_CODICEFISCALE'];
		$testa['RUOLO']= $input['TESTA_RUOLO'];

		$ret = $this->PVMOD->write_testa($testa);
	}
	public function testa()
	{	
		$input = filter_input_array(INPUT_POST);
		$input['AZIONE']="";
		switch($input['action']){
			case "edit":
				$input['AZIONE']= "UPDATE";
				break;
			case "insert":
				$input['AZIONE']= "INSERT";
				break;
			case "delete":
				$input['AZIONE']= "DELETE";
				break;
			
		}
		$ret = $this->PVMOD->write_testa($input);

		header('Content-Type: application/json');
		echo json_encode($input );
	}

	public function teste_json()
	{	
		
		$p = array();
		$p['CODIFICA']=Preventivi_model::CODIFICA_PREVENTIVI;
		$StatiPreventivoFiltro = Preventivi_model::PREVENTIVO_IN_COMPILAZIONE;
		$StatiPreventivoFiltro .= ",".Preventivi_model::PREVENTIVO_INVIATO;
		$p['STATO_PREVENTIVO']=$StatiPreventivoFiltro;

		$regolazione =  $this->PVMOD->get_regolazione($p);
		$teste = $regolazione->teste;
		$ret=array();
		$ret['data']=$teste ;
		$ret['itemsCount']=count($teste) ;
		header('Content-Type: application/json');

		echo json_encode($teste );
	}

	protected function delete_regolazione($progressivo_appendice=0){
		//e' una cancellazione virtuale.... in pratica lo metto in uno stato
		// che l'utente non vede
		$regolazione = array();
		$regolazione['PROGRESSIVO'] = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:$progressivo_appendice;
		$regolazione['STATO_PREVENTIVO'] = Preventivi_model::APPENDICE_CANCELLATA_DA_UTENTE;
		$ret = $this->PVMOD->cambia_stato_preventivo($regolazione);
		return $ret;
	}
	protected function conferma_regolazione($progressivo_appendice=0){
		$regolazione = array();
		$regolazione['PROGRESSIVO'] = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:$progressivo_appendice;
		$regolazione['STATO_PREVENTIVO'] = Preventivi_model::PREVENTIVO_INVIATO;
		$ret = $this->PVMOD->cambia_stato_preventivo($regolazione);
		return $ret;
	}
	protected function stampa_regolazione($progressivo_appendice=0){
		$regolazione = array();
		$regolazione['PROGRESSIVO'] = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:$progressivo_appendice;
		$sFile = "";
		if($regolazione['PROGRESSIVO']>0){
			$ret = $this->PVMOD->stampa_regolazione($regolazione);
			if($ret){
				$sFile = base64_decode($ret->file_content);
			}
		}
		$out_file_name = "regolazione.pdf";
		header("Content-type:application/pdf");
		header("Content-Disposition:attachment;filename=$out_file_name");
		echo $sFile;
		die();
	}
	protected function write_regolazione(){
		$regolazione = $this->post_to_regolazione();
		$ret = $this->PVMOD->put_regolazione($regolazione);
		return $ret;
	}

	protected function post_to_regolazione(){
		if(empty($_POST)) {
			return array();
		}
		$regolazione = array();
		$regolazione['PROGRESSIVO'] = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:0;
		$regolazione['CIG'] = isset($_POST['CIG'])?$_POST['CIG']:"";
		$regolazione['RAMO']= Preventivi_model::RAMO_GLOBALE_SCUOLA;
		$dd=array();
		$dd['DS'] = isset($_POST['DS'])?$_POST['DS']:"";
		$dd['DSGA'] = isset($_POST['DSGA'])?$_POST['DSGA']:"";
		$dd['N_ALUNNI'] = isset($_POST['N_ALUNNI'])?$_POST['N_ALUNNI']:0;
		$dd['N_ALUNNI_H'] = isset($_POST['N_ALUNNI_H'])?$_POST['N_ALUNNI_H']:0;
		$dd['N_ALUNNI_PAG'] = isset($_POST['N_ALUNNI_PAG'])?$_POST['N_ALUNNI_PAG']:0;
		$dd['N_ALUNNI_TOLL'] = isset($_POST['N_ALUNNI_TOLL'])?$_POST['N_ALUNNI_TOLL']:0;
		$dd['OPE_ORG'] = isset($_POST['OPE_ORG'])?$_POST['OPE_ORG']:0;
		$dd['OPE_PAG'] = isset($_POST['OPE_PAG'])?$_POST['OPE_PAG']:0;
		$regolazione['dati_dinamici']=$dd;

		$teste = array();

		$strTeste=isset($_POST['TESTE'])?$_POST['TESTE']:"";

		$idxColumns=array("COGNOME"=>1,"NOME"=>2,"CODICEFISCALE"=>3,"RUOLO"=>4);
		foreach(preg_split("/((\r?\n)|(\r\n?))/", $strTeste) as $line){
			if($line!=""){
				$valori = explode("#",$line);
				$testa = array();
				foreach($idxColumns as $colonna=>$indice){
					$testa[$colonna] = isset($valori[$indice])?$valori[$indice]:"";
				}
				if($testa["COGNOME"]!="" && $testa["CODICEFISCALE"]!="" ){
					$teste[]=$testa;
				}
			}
			
		} 
		$regolazione['teste']=$teste;

		return $regolazione;
	}

	public function appendice($progressivo_appendice="")
	{	

		$progressivo_appendice = $progressivo_appendice>0?$progressivo_appendice:0;
		$progressivo_appendice = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:$progressivo_appendice;
		$azione = isset($_POST['AZIONE'])?$_POST['AZIONE']:"";
	
		$write_config = array();
		$write_config['PROGRESSIVO_APPENDICE']=$progressivo_appendice;
		$write_config['AZIONE']=$azione;
		
		switch($azione){
			case "ADDTESTA":
				if($progressivo_appendice<=0){
					$ret = $this->write_appendice($write_config);	
					if($progressivo_appendice==0){
						$progressivo_appendice = $ret->PROGRESSIVO;
					}
				}
				$this->add_testa($progressivo_appendice);
			break;
			case "SALVA":
				$ret = $this->write_appendice($write_config);
				if($progressivo_appendice==0){
					$progressivo_appendice = $ret->PROGRESSIVO;
				}
			break;
			case "DELETE":
				$this->delete_appendice($write_config);
				$progressivo_appendice=0;
			break;
			case "CONFERMA":
				$this->write_appendice($write_config);
				$this->conferma_appendice($progressivo_appendice);
			break;
			case "STAMPA":
				$this->stampa_regolazione($progressivo_appendice);
				die();
			break;
		}
		
		$page_config=array();
		$page_config['titolo']="Appendice";
		$page_config['js_files']=array();
		$page_config['js_files'][]="jquery.tabledit/jquery.tabledit.mod.js";
		$page_config['css_files']=array();
		
		$page_data = array();
		$page_data['const']= $this->PVMOD->costanti();
		if($progressivo_appendice==0){
			$page_data['appendice']= $this->PVMOD->get_appendice_vuota($progressivo_appendice);
		}
		else{
			$page_data['appendice']= $this->PVMOD->get_appendice($progressivo_appendice);
		}
		
		$this->show("user_appendice",$page_config,$page_data);
	}

	protected function write_appendice($params=array()){
		$appendice = $this->post_to_appendice();
		foreach($params as $key=>$val){
			$appendice[$key] = $val;
		}
	
		$ret = $this->PVMOD->put_appendice($appendice);
		$retData= array();
		if($ret->success){
			$retData = $ret->data;
		}
		return $retData;
	}
	protected function delete_appendice($write_config=array()){
		
		/*
		//e' una cancellazione virtuale.... in pratica lo metto in uno stato
		// che l'utente non vede
		$regolazione = array();
		$regolazione['PROGRESSIVO'] = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:$progressivo_appendice;
		$regolazione['STATO_PREVENTIVO'] = Preventivi_model::APPENDICE_CANCELLATA_DA_UTENTE;
		$ret = $this->PVMOD->cambia_stato_preventivo($regolazione);
		return $ret;
		*/

		// faccio cancellazione fiscica come richiesto da Scuolafutura
		$write_config['AZIONE']="DELETE";
		return $this->write_appendice($write_config);
	}
	protected function conferma_appendice($progressivo_appendice=0){
		$regolazione = array();
		$regolazione['PROGRESSIVO'] = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:$progressivo_appendice;
		$regolazione['STATO_PREVENTIVO'] = Preventivi_model::APPENDICE_INVIATA;
		$ret = $this->PVMOD->cambia_stato_preventivo($regolazione);
		return $ret;
	}
	
	protected function post_to_appendice(){
		if(empty($_POST)) {
			return array();
		}
		$regolazione = array();
		$regolazione['PROGRESSIVO'] = isset($_POST['PROGRESSIVO'])?$_POST['PROGRESSIVO']:0;
		$regolazione['CODIFICA'] = isset($_POST['CODIFICA'])?$_POST['CODIFICA']: Preventivi_model::CODIFICA_PREVENTIVI_APPENDICI;
		$regolazione['STATO_PREVENTIVO'] = isset($_POST['STATO_PREVENTIVO'])?$_POST['STATO_PREVENTIVO']: Preventivi_model::APPENDICE_IN_COMPILAZIONE;
		$regolazione['RAMO']= Preventivi_model::RAMO_GLOBALE_SCUOLA;
		$dd=array();
		$regolazione['dati_dinamici']=$dd;

		$teste = array();

		$strTeste=isset($_POST['TESTE'])?$_POST['TESTE']:"";

		$idxColumns=array("COGNOME"=>1,"NOME"=>2,"CODICEFISCALE"=>3,"RUOLO"=>4);
		foreach(preg_split("/((\r?\n)|(\r\n?))/", $strTeste) as $line){
			if($line!=""){
				$valori = explode("#",$line);
				$testa = array();
				foreach($idxColumns as $colonna=>$indice){
					$testa[$colonna] = isset($valori[$indice])?$valori[$indice]:"";
				}
				if($testa["COGNOME"]!="" && $testa["CODICEFISCALE"]!="" ){
					$teste[]=$testa;
				}
			}
			
		} 
		$regolazione['teste']=$teste;

		return $regolazione;
	}


}
