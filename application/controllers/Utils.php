<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utils extends MY_Controller {
	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
	}
	public function index()
	{
		header('Location: /');
	}

	public function contatti_preview()
	{
		$funzioni_abilitate = $this->funzioni_abilitate();
		if(!in_array("contatti.preview",$funzioni_abilitate)){
			header('Location: /');
		}
		$page_config=array();
		$page_config['titolo']="Comunicazioni";

		$page_data = array();
		

		$tips=array();
		$sHTML = '<div class="card card-dettaglio border-dettaglio">';
		$sHTML .= PHP_EOL.' <span><i class="fa fa-clock fa-2x mr-1"></i>Gli orari della nostra agenzia sono 9:00 - 18:00 da lunedì a venerdì</span>';
		$sHTML .= PHP_EOL.'</div>';
		$tips[]=array("title"=>"Orari di Apertura (semplice)",
					"html"=>$sHTML);
		
		
		$sHTML = '<div class="card card-dettaglio border-dettaglio">';
		$sHTML .= PHP_EOL.' <span>Nell\' augurati buone feste ti ricodiamo che la nostra agenzia rester&agrave; chiusa dal giorno XX al giorno YY!';
		$sHTML .= PHP_EOL.'  <i class="fa fa-snowflake fa-2x ml-1"></i>';
		$sHTML .= PHP_EOL.' <span>';
		$sHTML .= PHP_EOL.'</div>';
	    $tips[]=array("title"=>"Buone feste ( in tinta )",
					"html"=>$sHTML);

		$sHTML = '<div class="card card-dettaglio border-dettaglio">';
		$sHTML .= PHP_EOL.' <div class="card-header">';
		$sHTML .= PHP_EOL.'  <i class="fa fa-gift fa-2x mr-2"></i>';
		$sHTML .= PHP_EOL.'  #NOMINATIVO!! <br>Ti abbiamo riservato un\'offerta da non credere. Vai a vederla su ';
		$sHTML .= PHP_EOL.'  <a href="https://www.assieasy.com/" target="_blank"><u>assieasy.com</u></a>';
		$sHTML .= PHP_EOL.' </div>';
		$sHTML .= PHP_EOL.'</div>';
					
		$tips[]=array("title"=>"Offerta con nome Utente (Evidente)",
					"html"=>$sHTML);

	
		$sHTML = '<div class="d-flex justify-content-center align-items-end" ';
		$sHTML .= PHP_EOL.' style="height:200pt;';
		$sHTML .= PHP_EOL.' background: url(\'https://www.touringclub.it/sites/default/files/styles/gallery_full/public/immagini_georiferite/piazza_notturno_chiara_03-2011_foto_c._vassalli.jpg\')';
		$sHTML .= PHP_EOL.' repeat-x center center;background-size: cover;">';
		$sHTML .= PHP_EOL.' <div class="card-dettaglio mb-3">';
		$sHTML .= PHP_EOL.'  <a class="btn" href="https://www.assieasy.com" target="_blank">assieasy.com</a>';
		$sHTML .= PHP_EOL.' </div>';
		$sHTML .= PHP_EOL.'</div>';
		
		$tips[]=array("title"=>"Immagine",
					"html"=>$sHTML);
		
		$sHTML = '<!-- *** SMARTPHONE *** -->';
		$sHTML .= PHP_EOL.'<div class="d-sm-none">';
		$sHTML .= PHP_EOL.' <div class="d-flex justify-content-center align-items-end"';
		$sHTML .= PHP_EOL.' style="height:150pt;background: url(\'https://www.assinews.it/wp-content/uploads/2017/09/eHealth_-324x160.jpg\') ';
		$sHTML .= PHP_EOL.' repeat-x top left;background-size: cover;">';
		$sHTML .= PHP_EOL.'  <div class="card-dettaglio m-3">';
		$sHTML .= PHP_EOL.'   <a class="btn" href="https://www.assieasy.com" target="_blank">assieasy.com</a>';
		$sHTML .= PHP_EOL.'  </div>';
		$sHTML .= PHP_EOL.' </div>';
		$sHTML .= PHP_EOL.'</div>';
		$sHTML .= PHP_EOL.'<!-- *** DESKTOP *** -->';
		$sHTML .= PHP_EOL.'<div class="d-none d-sm-block">';
		$sHTML .= PHP_EOL.' <div class="d-flex justify-content-end align-items-end " ';
		$sHTML .= PHP_EOL.' style="height:150pt;background: url(\'https://www.forbesindia.com/media/images/2019/Mar/img_113881_digital_health_startups.jpg\')';
		$sHTML .= PHP_EOL.' repeat-x center center;background-size: cover;">';
		$sHTML .= PHP_EOL.'  <div class="card-dettaglio m-3">';
		$sHTML .= PHP_EOL.'   <a class="btn" href="https://www.assieasy.com" target="_blank">assieasy.com</a>';
		$sHTML .= PHP_EOL.'  </div>';
		$sHTML .= PHP_EOL.' </div>';
		$sHTML .= PHP_EOL.'</div>';
	
		$tips[]=array("title"=>"Immagine Doppia Responsive",
					"html"=>$sHTML);

		$sHTML = '<!-- *** CAROSELLO *** -->';
		$sHTML .= PHP_EOL.'<div id="carouselExampleControls" class="carousel slide w-100" data-ride="carousel">';
		$sHTML .= PHP_EOL.' <div class="carousel-inner" role="listbox">';
		$sHTML .= PHP_EOL.'  <div class="carousel-item active">';
		$sHTML .= PHP_EOL.'   <div class="img"><img class="d-block img-fluid" src="https://www.ilmiocondominio.org/wp-content/uploads/2017/05/Assicurazione-globale-fabbricato.jpg" alt="Assicura la tua Casa"></div>';
		$sHTML .= PHP_EOL.'   <div class="carousel-caption d-none d-md-block">';
		$sHTML .= PHP_EOL.'    <div class="card-dettaglio"><a class="btn " href="http://assieasy.com" target="_blank" >Assicura la tua Casa</a></div>';
		$sHTML .= PHP_EOL.'   </div>';
		$sHTML .= PHP_EOL.'  </div>';
		$sHTML .= PHP_EOL.'  <div class="carousel-item">';
		$sHTML .= PHP_EOL.'   <div class="img"><img class="d-block img-fluid" src="https://pila.it/wp-content/uploads/2017/11/Header-sconti-e-agevolazioni-1600x500px.jpg" alt="Assicura la tua Vita"></div>';
		$sHTML .= PHP_EOL.'   <div class="carousel-caption d-none d-md-block">';
		$sHTML .= PHP_EOL.'    <div class="card-dettaglio"><a class="btn " href="http://assieasy.com" target="_blank" >Assicura la tua Vita</a></div>';
		$sHTML .= PHP_EOL.'   </div>';
		$sHTML .= PHP_EOL.'  </div>';
		$sHTML .= PHP_EOL.'  <div class="carousel-item">';
		$sHTML .= PHP_EOL.'   <div class="img"><img class="d-block img-fluid" src="https://www.myusa.it/wp-content/uploads/2019/10/sposarsi-las-vegas-1600x500.jpg" alt="Assicura la tua Auto"></div>';
		$sHTML .= PHP_EOL.'    <div class="carousel-caption d-none d-md-block">';
		$sHTML .= PHP_EOL.'    <div class="card-dettaglio"><a class="btn " href="http://assieasy.com" target="_blank" >Assicura la tua Auto</a></div>';
		$sHTML .= PHP_EOL.'   </div>';
		$sHTML .= PHP_EOL.'  </div>';
		$sHTML .= PHP_EOL.' </div>';
		$sHTML .= PHP_EOL.' <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">';
		$sHTML .= PHP_EOL.'  <span class="carousel-control-prev-icon" aria-hidden="true"></span>';
		$sHTML .= PHP_EOL.'  <span class="sr-only">Previous</span>';
		$sHTML .= PHP_EOL.' </a>';
		$sHTML .= PHP_EOL.' <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">';
		$sHTML .= PHP_EOL.'  <span class="carousel-control-next-icon" aria-hidden="true"></span>';
		$sHTML .= PHP_EOL.'  <span class="sr-only">Next</span>';
		$sHTML .= PHP_EOL.' </a>';
		$sHTML .= PHP_EOL.'</div>';

		$tips[]=array("title"=>"Carosello immagini",
		"html"=>$sHTML);
					

		$page_data['tips']=$tips;
		$this->show("utils_contatti_preview",$page_config,$page_data);
	}
	
}
