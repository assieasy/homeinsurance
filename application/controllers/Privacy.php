<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy extends MY_Controller {
	/**
     * @var Anagrafica_model
     */ 
	protected $PMOD; 

	function __construct()
    {
		parent::__construct();
		if(empty($this->utente)){
			header('Location: /');
		}
		$this->load->model("Anagrafica_model");
		$this->PMOD = new Anagrafica_model($this->assi);
	}
	public function index()
	{
		$this->privacy();
	}

	public function privacy()
	{
		$azione = isset($_POST['AZIONE'])?$_POST['AZIONE']:"";
		$errori_sinistro = array();
		switch($azione){
			case "SALVA":
				$ret = $this->write_privacy();
			break;
		}
		$page_config=array();
		$page_config['titolo']="Privacy";
		$page_data = array();
		

		$mie_privacy = array();
		
		$p=array();
		$anag = $this->PMOD->get_privacy($p);
		$page_data['anagrafica_privacy']= $anag;
		$this->show("user_privacy",$page_config,$page_data);
	}

	protected function write_privacy()
	{
		$privacy = $this->post_to_privacy();
		if(empty($privacy["errori"])){
			$ret = $this->PMOD->put_privacy($privacy);
			$this->mail_conferma_privacy();

			header('Location: /');
			die();
		}
	}

	protected function mail_conferma_privacy()
	{
		$page_config=array();
		$page_config['titolo']="Privacy";

		
		
		$dati = array();
		$anag = $this->PMOD->get_privacy();
		$dati['anagrafica_privacy']= $anag;

		$sTMP = isset($this->datiSito['SITE_URL'])?$this->datiSito['SITE_URL']:"";
		$dati['SITE_URL'] = $sTMP;
		$corpo_mail =$this->load_view('mail/conferma_privacy',$page_config,$dati);
		
		
		
		$params_mail=array();
		$sTMP = isset($this->datiSito['titolo'])?$this->datiSito['titolo']:"Home Insurance";
		$params_mail['OGGETTO_MAIL']=$sTMP." - Conferma Privacy";
		$params_mail['CORPO_MAIL']=$corpo_mail;
		$params_mail['PROGRAMMA']="HOMEINSURANCE";
		//$params_mail['IGNORA_PRIVACY']=1;
		$ret = $this->assi->mail_a_cliente($params_mail);
		//echo $this->assi->get_last_response();die();
		
	}

	protected function post_to_privacy(){
		if(empty($_POST)) {
			return array();
		}
		
		$retVal = array();
		$retVal['ORIGINE_CONSENSO']="HOMEINSURANCE";
		$retVal['ELENCO_PRIVACY']= array();
		foreach($_POST as $key=>$value){
			$retVal[$key]=$value;
			if(preg_match('/^PRIVACY_\d{1,}$/i',$key)){
				$num = str_replace("PRIVACY_","",strtoupper($key));
				$retVal['ELENCO_PRIVACY'][]=array("NUMERO_PRIVACY"=>$num,"CONSENSO"=>$value);
			}
		}
		$retVal["errori"]=array();
		
		return $retVal;
	}
	
}
