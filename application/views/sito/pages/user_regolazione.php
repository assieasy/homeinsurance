<div class="container-fluid maincontainer pb-4">
    <? 
    $COSTANTI = isset($page_data['const'])?$page_data['const']:array();
    $REGOLAZIONE = isset($page_data['regolazione'])?$page_data['regolazione']:array();

    $consenti_modifiche = FALSE;
    $consenti_stampa = FALSE;
    
    if(!empty($REGOLAZIONE)){
        if($REGOLAZIONE->STATO_PREVENTIVO==Preventivi_model::PREVENTIVO_IN_COMPILAZIONE){
            $consenti_modifiche =TRUE;
           
        }
        if($REGOLAZIONE->STATO_PREVENTIVO==Preventivi_model::PREVENTIVO_IN_COMPILAZIONE
            || $REGOLAZIONE->STATO_PREVENTIVO==Preventivi_model::PREVENTIVO_INVIATO){
            $consenti_stampa = ($REGOLAZIONE->PROGRESSIVO >0);
        }
    }

    ?>

    <? if(empty($REGOLAZIONE)){
        ?>
        <div class="container h-100">
            <div class="row pt-4">
                <div class="col">
                    <div class="alert alert-info alert-info-custom" role="alert">
                        Nessun Regolazione richiesta. 
                    </div>
                </div>
            </div>
        </div>
    <?}else{?>

    <div class="container">
        <div class="row pt-4" >
            <div class="col-12">
                <a href="/preventivi/variazioni" class="btn btn-primary">Torna alle Variazioni</a>
            </div>
        </div>
        <div class="row pt-4" id="main-row">
            <div class="col-12">
                <?if($REGOLAZIONE->PROGRESSIVO>0)
                    $dt = new DateTime($REGOLAZIONE->DATA_RIFERIMENTO);
                    {
                    ?>
                    <h5>Regolazione numero <?=$REGOLAZIONE->NUMERO_PREVENTIVO?> del <?=$dt->format('d/m/Y')?></h5>
                    <?
                    }
                ?>
            </div>
        </div>
        <form id="form-regolazione" method="POST">
            
        <div class="row pt-4" id="main-row">
            <div class="col">
                <input type="hidden" name="AZIONE" id="inputAZIONE" value="">
                <input type="hidden" name="PROGRESSIVO" id="inputPROGRESSIVO" value="<?=$REGOLAZIONE->PROGRESSIVO?>">
                <input type="hidden" name="TESTE" id="teste-hidden" value="">
                <? 
                $campi=array();
                $campi[] = array("CAMPO"=>"CIG","DESCRIZIONE"=>"Cig","TIPO"=>"STR");
                $campi[] = array("CAMPO"=>"DS","DESCRIZIONE"=>"Dirigente","TIPO"=>"STR");
                $campi[] = array("CAMPO"=>"DSGA","DESCRIZIONE"=>"DSGA","TIPO"=>"STR");
                foreach($campi as $campo){
                    ?>
                    <div class="form-group row">
                        <label for="input<?=$campo["CAMPO"]?>" class="col-sm-4 col-form-label col-form-label-sm"><?=$campo["DESCRIZIONE"]?></label>
                        <div class="col-8">
                            <input type="text" 
                                class="form-control form-control-sm" 
                                name="<?=$campo["CAMPO"]?>" 
                                id="input<?=$campo["CAMPO"]?>" 
                                placeholder="<?=$campo["DESCRIZIONE"]?>" 
                                value="<?=$REGOLAZIONE->{$campo["CAMPO"]}?>"
                                required
                                <?=$consenti_modifiche?"":"readonly" ?> 
                                >
                        </div>
                    </div>
                    <?
                    
                    if(isset($REGOLAZIONE->messages->{$campo["CAMPO"]})){
                        ?>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert" ><?=$REGOLAZIONE->messages->{$campo["CAMPO"]}?></div>
                            </div>
                        </div>
                    <?}?>
                    <?
                }
                $campi=array();
                $campi[] = array("CAMPO"=>"N_ALUNNI","DESCRIZIONE"=>"Numero Alunni totale iscritti e frequentanti","TIPO"=>"INT");
                $campi[] = array("CAMPO"=>"N_ALUNNI_H","DESCRIZIONE"=>"Numero Alunni diversamente abili","TIPO"=>"INT");
                $campi[] = array("CAMPO"=>"N_ALUNNI_TOLL",
                                "DESCRIZIONE"=>"Numero Alunni su cui applicare la tolleranza (iscritti – gratuiti)",
                                "TIPO"=>"INT",
                                "OPTIONS"=>"readonly");
                $campi[] = array("CAMPO"=>"N_ALUNNI_PAG",
                                "DESCRIZIONE"=>"Numero Alunni paganti (tolleranza max. 10%)","TIPO"=>"INT");
                $campi[] = array("CAMPO"=>"OPE_ORG","DESCRIZIONE"=>"Operatori scolastici in organico (docenti e ATA)","TIPO"=>"INT");
                $campi[] = array("CAMPO"=>"OPE_PAG","DESCRIZIONE"=>"Operatori scolastici paganti (come da elenco nominativo allegato)","TIPO"=>"INT");
                foreach($campi as $campo){
                    ?>
                    <div class="form-group row">
                        <div class="col-4">
                            <div class="input-group form-group">
                                <?
                                if(isset($REGOLAZIONE->messages->{$campo["CAMPO"]})){
                                    ?>
                                     <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-exclamation-triangle" data-toggle="tooltip" data-placement="top" title="<?=$REGOLAZIONE->messages->{$campo["CAMPO"]}?>"></i></i></span>
                                    </div>

                                    
                                <?}?>
                               
                                <input type="number" 
                                    class="form-control form-control-sm" 
                                    name="<?=$campo["CAMPO"]?>" 
                                    id="input<?=$campo["CAMPO"]?>" 
                                    placeholder="0" 
                                    required
                                    value="<?=$REGOLAZIONE->{$campo["CAMPO"]}?>"
                                    <?=$consenti_modifiche?"":"readonly" ?> 
                                    <?=isset($campo["OPTIONS"])?$campo["OPTIONS"]:""?>
                                    >
                            </div>
                        </div>
                        <label for="input<?=$campo["CAMPO"]?>" class="col-sm-8 col-form-label  col-form-label-sm"><?=$campo["DESCRIZIONE"]?></label>
                        
                    </div>
                    <? if(isset($REGOLAZIONE->messages->{$campo["CAMPO"]})){ ?>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert" ><?=$REGOLAZIONE->messages->{$campo["CAMPO"]}?></div>
                            </div>
                        </div>
                    <?}?>
                    
                    <?
                }
                ?>
               
            </div>
        </div>
        <div class="row justify-content-center" id="add-user-row">
            <? if($consenti_stampa){?>    
                <div class="col-4 justify-content-center">
                    <button type="button" class="btn btn-primary" href="#" onclick="stampaPreventivo('STAMPA')" >Stampa</button>
                </div>
            <?}?>
            <? if($consenti_modifiche){ ?>
                <div class="col-4 justify-content-center">
                    <button type="button" <?=!$REGOLAZIONE->valid?"disabled":""?> class="btn btn-primary" href="#" onclick="salvaPreventivo('CONFERMA','main-row')" >Conferma e Invia</button>
                </div>
                <div class="col-4 justify-content-center">
                    <button type="button" class="btn btn-primary" href="#" onclick="salvaPreventivo('SALVA','main-row')" >Salva</button>
                </div>
            <?}?>
            
           <!-- <div class="form-group row justify-content-md-center <?=$REGOLAZIONE->valid?"collapse":""?>">
                <?foreach($REGOLAZIONE->messages as $msg){ ?>
                <div class="alert alert-info alert-info-custom" role="alert" id="forgotForm-msg"><?=$msg?></div>
                <?}?>
            </div>-->
        </div>
        <hr>
        <div class="form-group row <?=$consenti_modifiche?"":"collapse" ?>" id="add-user-row">
            <div class="col-2">
                <label class="col-form-label  col-form-label-sm">Nuovo Operatore:</label>
            </div>
            <div class="col-2">
                <input type="text" 
                                class="form-control form-control-sm" 
                                name="TESTA_COGNOME" 
                                id="inputTESTA_COGNOME" 
                                placeholder="Cognome" 
                                value=""
                                required
                                >
            </div>
            <div class="col-2">
                <input type="text" 
                                class="form-control form-control-sm" 
                                name="TESTA_NOME" 
                                id="inputTESTA_NOME" 
                                placeholder="Nome" 
                                value=""
                                required
                                >
            </div>
            <div class="col-2">
                <input type="text" 
                                class="form-control form-control-sm" 
                                name="TESTA_CODICEFISCALE" 
                                id="inputTESTA_CODICEFISCALE" 
                                placeholder="Codice Fiscale" 
                                value=""
                                required
                                >
            </div>
            <div class="col-2">
                <select class="form-control  form-control-sm" 
                                name="TESTA_RUOLO" 
                                id="inputTESTA_RUOLO" 
                                placeholder="Ruolo" 
                                >
                    <option>Dirigente Scolastico</option>
                    <option>DSGA</option>
                    <option>Docente</option>
                    <option>Assistente Amministrativo</option>
                    <option>Assistente Tecnico</option>
                    <option>Collaboratore Scolastico</option>
                    <option>Altro</option>
                    </select>

            </div>
            <div class="col-2">
                <button type="button" class="btn btn-primary" href="#" onclick="salvaPreventivo('ADDTESTA','add-user-row')" >Aggiungi</button>
            </div>
        </div>
        </form>
        
        <div class="row pt-4" id="tab-users-row">
            <div class="col">
            <table  class="table table-striped table-bordered"  id="tabella-regolazione-teste">
                <thead>
                    <tr>
                    <th>#</th>
                    <th class="collapse">Prog</th>
                    <th>Cognome</th>
                    <th>Nome</th>
                    <th>Codice Fiscale</th>
                    <th>Ruolo</th>
                    </tr>
                </thead>
                <tbody>
                <?
                $conta_teste = 0;
                foreach($REGOLAZIONE->teste as $testa){
                     $conta_teste++;
                    ?>
                    <tr>
                    <?if($testa->message==""){?>
                        <td class="noedit"><div class="alert alert-sm alert-primary" role="alert"><?=$conta_teste?></div></td>
                    <?
                        }else{
                    ?>
                        <td class="noedit">
                            <div class="alert alert-sm alert-danger " role="alert">
                            <i class="fas fa-exclamation-triangle" data-toggle="tooltip" data-placement="top" title="<?=$testa->message?>"></i>
                            </div>
                        </td>
                    <?
                    }
                    ?>
                    <td class="collapse"><?=$testa->PROG_TESTA?></td>
                    
                    <td><?=$testa->COGNOME?></td>
                    <td><?=$testa->NOME?></td>
                    <td><?=$testa->CODICEFISCALE?></td>
                    <td><?=$testa->RUOLO?></td>
                    </tr>
                    <?
                }
                ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <? } ?>
</div>


<script>
/*************** funzioni init ***********/
pageinitfunctions = pageinitfunctions?pageinitfunctions:[];
<? if($consenti_modifiche){ ?>
    pageinitfunctions.push({name:'init_somme'});
    pageinitfunctions.push({name:'init_tabella_teste'});
    pageinitfunctions.push({name:'totalizza'});
<? } ?>

/*************** funzioni init ***********/


function init_somme(parametro1, parametro2) {
    //GESTIONE SOMME
    $('#inputN_ALUNNI, #inputN_ALUNNI_H, #inputN_ALUNNI_PAG').change(function () {
        totalizza();
    });
}

function init_tabella_teste() {
    //gestione tabela teste
    $('#tabella-regolazione-teste').Tabledit({
    lang : 'it',
    url: '/preventivi/testa',
    columns: {    
        identifier: [1, 'PROG_TESTA','hidden'],
        editable: [
        [2, 'COGNOME', 'input'],
        [3, 'NOME', 'input'],
        [4, 'CODICEFISCALE', 'input'],
        [5, 'RUOLO', 'select', 
            '{'+
            '"Dirigente Scolastico":"Dirigente Scolastico"'+
            ',"DSGA":"DSGA"'+
            ',"Docente":"Docente"'+
            ',"Assistente Amministrativo":"Assistente Amministrativo"'+
            ',"Assistente Tecnico":"Assistente Tecnico"'+
            ',"Collaboratore Scolastico":"Collaboratore Scolastico"'+
            ',"Altro":"Altro"'+
            '}'
            ]
        ]
    },
    onAjax: function(action, serialize) {
        serialize += '&PROG_PREVENTIVO=' +  $('#inputPROGRESSIVO').val();
        return serialize;
    },
    onSuccess: function(data, textStatus, jqXHR) {
        salvaPreventivo("","tab-users-row");
    },
    onFail: function(jqXHR, textStatus, errorThrown) {
        console.log('onFail(jqXHR, textStatus, errorThrown)');
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
    }
    });
}


function totalizza(){
  var n_alunni = 0;
  var n_alunni_h = 0;
  var n_alunni_pag = 0;
  var s ="";

  s = $('#inputN_ALUNNI').val();
  if(s){
    n_alunni = parseInt(s);
  }
  s = $('#inputN_ALUNNI_H').val();
  if(s){
    n_alunni_h = parseInt(s);
  }
  s = $('#inputN_ALUNNI_PAG').val();
  if(s){
    n_alunni_pag = parseInt(s);
  }

  var tot_alunni_tolleranza = n_alunni-n_alunni_h;
  $('#inputN_ALUNNI').val(n_alunni);
  $('#inputN_ALUNNI_H').val(n_alunni_h);
  $('#inputN_ALUNNI_TOLL').val(tot_alunni_tolleranza);
}



function salvaPreventivo( azione, ancora){
  switch(azione){
    case "ADDTESTA": 
        if(!insTestaOk()){
          return false;
        }
        $('form').attr("action","/preventivi/regolazione#add-user-row");
        break;
    default:
        $('form').attr("action","/preventivi/regolazione#main-row");
        break;
  }
  if(ancora){
    $('form').attr("action","/preventivi/regolazione#"+ancora);
  }
    $('#inputAZIONE').val(azione);
    $('form').attr("target","");
    $('form').submit();
    return false;
}

function insTestaOk(){
  var ok = true;
  var campi=['TESTA_COGNOME','TESTA_NOME','TESTA_CODICEFISCALE','TESTA_RUOLO'];
  campi.forEach( function(campo) {
    var s = $('#input'+campo).val();
    if(!s){
      ok = false;
    }
  });
  
  return ok;
}

function stampaPreventivo( azione){
  $('#inputAZIONE').val(azione);
  $('form').attr("target","_blank");
  $('form').submit();
  return false;
}
</script>
