<?  $REGOLAZIONE = isset($page_data['regolazione'])?$page_data['regolazione']:array();
    $POLIZZE = isset($page_data['polizze'])?$page_data['polizze']:array(); 
    $APPENDICI_APERTE = isset($page_data['appendici_aperte'])?$page_data['appendici_aperte']:array();
    $APPENDICE_NUOVA = isset($page_data['nuova_appendice'])?$page_data['nuova_appendice']:array();
    $SINISTRI = isset($page_data['sinistri'])?$page_data['sinistri']:array(); 
    
    $SCADENZE_SCADUTE = isset($page_data['scadenze_scadute'])?$page_data['scadenze_scadute']:array(); 

    $CONTATTI_UTILI = isset($page_data['contatti_utili'])?$page_data['contatti_utili']:array(); 
  
    $all_config_obj = $sito['all_config_obj'];
    $hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
    
    $link_pubblicita= isset($hiPar->link_pubblicita)?$hiPar->link_pubblicita:"";
    $link_pubblicita_href= isset($hiPar->link_pubblicita_href)?$hiPar->link_pubblicita_href:"";

    $quante_card = 1;
    if(in_array("scadenze",$funzioni_abilitate)){
        $quante_card++;
    }
    if(in_array("sinistri",$funzioni_abilitate)){
        $quante_card++;
    }
?>

<div class="container-fluid maincontainer pb-4 ">
    <? if($link_pubblicita!="") { 
        if($link_pubblicita_href!=""){ ?>
            <a href="<?=$link_pubblicita_href?>" target="_blank">
        <?}?>
        <div class="card main-pubblicita h-20 w-100 mt-2 p-0"
             style="background: url('<?=$link_pubblicita?>') repeat-x top left;background-size: cover;">
        </div>
        <? if($link_pubblicita_href!=""){ ?>
            </a>
        <?}?>
    <? } ?>

    <?
    $CONTATTI_TMP = array();
    foreach($CONTATTI_UTILI as $MSG){
        if($MSG->CONTESTO == Anagrafica_model::CONTATTI_ULTILI_CONTESTO_BENVENUTO){
            $CONTATTI_TMP[]= $MSG;
        }
    }
    ?>
    <div class="row pt-2">
        <div class="col">       
        <?
        if(!empty($CONTATTI_TMP)){
            foreach($CONTATTI_TMP as $MSG){
                    ?>
                        <div><?=$MSG->DESCRIZIONE?></div>
                    <?
            }
        }else{?>
            <div>Area riservata di <?=$utente->NOMINATIVO?>, Buongiorno!</div>
        <?}?>
        </div>
    </div>
    <?
    if(in_array("contatti.preview",$funzioni_abilitate)){
        $CONTATTI_TMP = array();
        foreach($CONTATTI_UTILI as $MSG){
            if($MSG->CONTESTO == Anagrafica_model::CONTATTI_ULTILI_PREVIEV_CONTESTO_BENVENUTO){
                $CONTATTI_TMP[]= $MSG;
            }
        }
        if(!empty($CONTATTI_TMP)){
            ?>
            <div class="row pt-2">
            <div class="col">
                <div class="small italic">***Inizio Preview*** <a href="/utils/contatti_preview" target="_blank">Suggerimenti</a></div>
                <?
                foreach($CONTATTI_TMP as $MSG){
                    ?>
                        <div><?=$MSG->DESCRIZIONE?></div>
                    <?
                }
                ?>
                <div class="small italic">***Fine Preview***</div>
            </div>
            </div>
            <?
        }
    }
    ?>

    <? if($sito["a2hs"]!="") {?>            
        <div class="row pt-2">
            <div class="col card-dettaglio justify-content-left mr-2">
                   <div class="card-a2hs text-right">
                        <button class="btn a2hs-button">+ Aggiungi il link al tuo desktop</button>
                    </div>
            </div>
        </div>
    <? } ?>

    <div class="row pt-2">
        <div class="col-md-<?=(12/$quante_card)?> d-flex align-items-stretch pt-2">
            <div class="card card-dettaglio border border-3 w-100 border-dettaglio border-polizze" >
            <div class="card-body">
                <span class="card-title main-font font-weight-bold">
                <i class="fa fa-id-card fa-2x icona" aria-hidden="true"></i> POLIZZE</span>
                <!--
                    <p class="card-text">In questa sezione puoi visualizzare le tue polizze e scaricare i relativi documenti.</p>
                -->
            </div>
            <div class="card-footer">
                <a href="/polizze/polizze" loadingSecondi="40" class="form-control form-control-sm btn btn-primary btn-loading">Consulta</a>
            </div>
            </div>
        </div>
        <? if(in_array("scadenze",$funzioni_abilitate)){?>
        <div class="col-md-<?=(12/$quante_card)?> d-flex align-items-stretch pt-2">
            <div class="card card-dettaglio border border-3 w-100 border-dettaglio border-scadenze" >
                <div class="card-body">
                    <span class="card-title main-font font-weight-bold">
                    <i class="fa fa-calendar-check fa-2x icona" aria-hidden="true"></i> SCADENZE
                    </span>
                    <? 
                        if(in_array("scadenze.msg",$funzioni_abilitate) ){
                            if(!empty($SCADENZE_SCADUTE)){ ?>
                                <div class="alert alert-info alert-info-custom">
                                <span >Sono presenti polizze in scadenza</span>
                                </div>
                            <?}else{?>
                                <div class="alert alert-info alert-info-custom">
                                <span >Nessuna scadenza nel periodo</span>
                                </div>
                            <?}
                        }
                    ?>  
                    <!--
                        <p class="card-text">In queste sezione è possibile visualizzare le scadenze.</p>
                    -->
                </div>
                <div class="card-footer">
                    <a href="/titoli/scadenze" class="form-control form-control-sm btn btn-primary  btn-loading">Consulta</a>
                </div>
            </div>
        </div>
        <?}?>
        <? if(in_array("sinistri",$funzioni_abilitate)){?>
        <div class="col-md-<?=(12/$quante_card)?> d-flex align-items-stretch pt-2">
            <div class="card card-dettaglio border border-3 w-100 border-dettaglio border-sinistri" >
                <div class="card-body">
                    <span class="card-title main-font font-weight-bold">
                    <i class="fa fa-user-injured fa-2x icona" aria-hidden="true"></i> SINISTRI</span>
                    <!--
                        <p class="card-text">In queste sezione è possibile visualizzare i sinistri.</p>
                    -->
                </div>
                <div class="card-footer">
                    <a href="/sinistri/sinistri" class="form-control form-control-sm btn btn-primary btn-loading" >Consulta</a>
                </div>
            </div>
        </div>
        <?}?>
    </div>
    <?if(in_array("messaggi",$funzioni_abilitate) ){ ?>
        <div class="row py-2">
            <div class="col-12">
                <div class="card card-dettaglio border-0 border-dettaglio m-0 p-0 " >
                        <div class="card-body justify-content-center text-center">
                        <h5> <a href="/messaggi/agenzia" class="btn btn-primary btn-loading" ><i class="fa fa-comment-dots mr-1" aria-hidden="true"></i>Scrivi un messaggio / Invia Documento</a></h5>
                        </div>
                </div>
            </div>
        </div>
       
    <?}?>
    <?  $CONTATTI_TMP = array();
        foreach($CONTATTI_UTILI as $MSG){
            if($MSG->CONTESTO == Anagrafica_model::CONTATTI_ULTILI_CONTESTO_MAIN){
                $CONTATTI_TMP[]= $MSG;
            }
        }
        if(!empty($CONTATTI_TMP)) { ?>
        <div class="row pt-2">
        <div class="col">
            <div class="card w-100" >
                <div class="card-header text-center"><b>Contatti Utili</b></div>
                <div class="card-body">
                    <?


                    $aCapo = true;
                    $contaContatti=0;
                    $quanti_per_riga = 3;
                    $i = count($CONTATTI_TMP);
                    if($i<$quanti_per_riga){
                        $quanti_per_riga = $i;
                    }
                    if($i % 4 == 0){
                        $quanti_per_riga = 4;
                    }
                    
                    foreach($CONTATTI_TMP as $C){
                        if($aCapo){
                            if($contaContatti>0){
                                //chiusura div row precedente
                                ?>
                                </div><!-- chiusura div row precedente -->
                                <?
                            }
                            ?>
                                <div class="row"> <!--div row -->
                            <?
                        }
                        $contaContatti++;
                        $aCapo = (($contaContatti % $quanti_per_riga)==0);
                        ?>
                        <div class="col-md-<?=(12/$quanti_per_riga)?> d-flex align-items-stretch pt-2">
                            <div class="card card-dettaglio w-100" >
                                <div class="card-header">
                                    <strong>
                                    <?=$C->TITOLO?>
                                    </strong>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <?
                                        $sTMP=$C->CONTATTO_UTILE;
                                        //TEST EMAIL
                                        if(strpos($C->CONTATTO_UTILE,"@")){
                                            $sTMP="<a href=\"mailto:$C->CONTATTO_UTILE\">$C->CONTATTO_UTILE</a>";
                                        }
                                        //TEST SITO
                                        if(filter_var($sTMP, FILTER_VALIDATE_URL) !== false){
                                            $testoLink =  preg_replace( "#^[^:/.]*[:/]+#i", "", $sTMP );
                                            $sTMP="<a target=\"_blank\" href=\"$C->CONTATTO_UTILE\">$testoLink</a>";
                                        }
                                        echo $sTMP;
                                        ?>
                                    </div>
                                    <div class="row small">
                                        <?=$C->DESCRIZIONE?>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?}
                    if($contaContatti>0){
                        //chiusura div row precedente
                        ?>
                        </div>
                        <?
                    }?>
                </div>
            </div>
        </div>
    </div>
    <? } ?>
</div>