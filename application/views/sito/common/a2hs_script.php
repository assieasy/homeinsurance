<? if($sito["a2hs"]!=""){ ?>
       
<script>
// Register service worker to control making site work offline

if('serviceWorker' in navigator) {
  navigator.serviceWorker
           .register('/sw.js')
           .then(function() { console.log('Service Worker Registered'); });
}

// Code to handle install prompt on desktop

let deferredPrompt;
const a2hsBtn = document.querySelector('.a2hs-button');
const a2hsCard = document.querySelector('.card-a2hs');

if(a2hsBtn){
  a2hsBtn.style.display = 'none';
}
if(a2hsCard){
  a2hsCard.style.display = 'none';
}
window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  if(a2hsBtn){
    // Update UI to notify the user they can add to home screen
    a2hsBtn.style.display = 'block';
    if(a2hsCard){
      a2hsCard.style.display = 'block';
    }
   
    a2hsBtn.addEventListener('click', (e) => {
      // hide our user interface that shows our A2HS button
      a2hsBtn.style.display = 'none';
      if(a2hsCard){
        a2hsCard.style.display = 'none';
      }
      // Show the prompt
      deferredPrompt.prompt();
      // Wait for the user to respond to the prompt
      deferredPrompt.userChoice.then((choiceResult) => {
          if (choiceResult.outcome === 'accepted') {
            console.log('User accepted the A2HS prompt');
          } else {
            console.log('User dismissed the A2HS prompt');
          }
          deferredPrompt = null;
        });
    });
  }
});

</script>
<? } ?>