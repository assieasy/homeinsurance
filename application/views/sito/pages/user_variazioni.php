<?  $COSTANTI = isset($page_data['const'])?$page_data['const']:array();
    $REGOLAZIONE = isset($page_data['regolazione'])?$page_data['regolazione']:array();
    $POLIZZE = isset($page_data['polizze'])?$page_data['polizze']:array(); 
    $APPENDICI_APERTE = isset($page_data['appendici_aperte'])?$page_data['appendici_aperte']:array();

    $APPENDICE_NUOVA = isset($page_data['nuova_appendice'])?$page_data['nuova_appendice']:array();

    $APPENDICI_TUTTE = isset($page_data['appendici_tutte'])?$page_data['appendici_tutte']:array();
    $REGOLAZIONI_TUTTE = isset($page_data['regolazioni_tutte'])?$page_data['regolazioni_tutte']:array();

?>

<div class="container-fluid h-100 maincontainer pb-4">
    <div class="row pt-5">
        <div class="col-md-12">
            <div class="card border border-3 border-scadenze" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-11">
                            <h5 class="main-font">
                            <i class="fa fa-question fa-2x" aria-hidden="true"></i> Regolazioni e Integrazioni
                            <? if(!empty($REGOLAZIONE)){  ?>
                                <a href="#" role="button" 
                                    onclick="gotoRegolazione(<?=$REGOLAZIONE->PROGRESSIVO?>)" class="alert alert-danger">Hai una Regolazione da 
                                                <?=($REGOLAZIONE->STATO_PREVENTIVO==Preventivi_model::PREVENTIVO_IN_COMPILAZIONE)?"confrmare":"inviare" ?>
                                </a>
                            <? }?>
                        </div>
                        <div class="col-md-1 align-self-end">
                            <h5 class="main-font">
                                <a class="align-self-end" href="/"><i class="fa fa-home fa-2x" aria-hidden="true"></i></a>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--
    <? if(!empty($APPENDICI_APERTE)){ ?>
        <div class="row p-4">    
        <? foreach($APPENDICI_APERTE as $APPENDICE){ ?>
            <div class="col-md-4">
                <div class="card" >
                <div class="card-body">
                    <h5 class="card-title">
                    <i class="fa fa-question fa-2x" aria-hidden="true"></i> Integrazione N. <?=$APPENDICE->PROGRESSIVO?> </h5>
                    <p class="card-text">
                        <a role="button" href="#" onclick="gotoAppendice(<?=$APPENDICE->PROGRESSIVO?>)" class="alert alert-danger" >
                        Hai una Integrazione da 
                            <?=($APPENDICE->STATO_PREVENTIVO==$COSTANTI['PREVENTIVO_IN_COMPILAZIONE'])?"confrmare":"inviare" ?>
                    
                        </a>
                    </p>
                </div>
                </div>
            </div>
        <? } ?>
        </div> 
    <? }?>
        -->
    <? if(!empty($REGOLAZIONI_TUTTE)){?>
    <div class="row">
        <div class="col-12">    
            <h5>Regolazioni</h5>
        </div>
    </div>
    <div class="row p-4">
        <div class="col-12">    
            <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">Data</th>
                <th scope="col">Numero</th>
                <th scope="col">Stato</th>
                <th scope="col">Teste</th>
                </tr>
            </thead>
            <tbody>
            <?  foreach($REGOLAZIONI_TUTTE as $key => $app)
                {
                        $dtapp = new DateTime($app->DATA_RIFERIMENTO);
                        ?>
                        <tr>
                        <td><?=$dtapp->format('d/m/Y') ?></td>
                        <td><a href="#" onclick="gotoRegolazione(<?=$app->PROGRESSIVO ?>)"><?=$app->NUMERO_PREVENTIVO?></a></td>
                        <?  $classe_msg="";
                            $testo_msg="";
                            switch($app->STATO_PREVENTIVO) {
                                case Preventivi_model::PREVENTIVO_IN_COMPILAZIONE:
                                    $classe_msg="danger";
                                    $testo_msg = "Regolazione da confrmare";
                                break;
                                case Preventivi_model::PREVENTIVO_INVIATO:
                                    $classe_msg="danger";
                                    $testo_msg = "Regolazione da inviare";
                                break;
                                default:
                                    $classe_msg="info";
                                    $testo_msg = "Regolazione registrata";
                                break;
                            }
                        ?>
                        <td>
                            <a role="button" href="#" onclick="gotoRegolazione(<?=$app->PROGRESSIVO?>)" 
                                class="alert alert-<?=$classe_msg?>" >
                                <?=$testo_msg ?>
                            </a>
                        </td>
                        <td><?=count($app->teste)?></td>
                        </tr>
            <?  } ?>
            </tbody>
            </table>
        </div>
    </div>
    <? } ?>


    <div class="row">
        <? if(!empty($APPENDICI_TUTTE)){?>
            <div class="col-md-6">    
                <h5>Elenco Integrazioni</h5>
            </div>
        <?}?>
        <? if(!empty($APPENDICE_NUOVA)){ ?>
            <div class="col-md-6">
                <a href="#" role="button" onclick="gotoAppendice(<?=$APPENDICE_NUOVA->PROGRESSIVO?>)" class="btn btn-primary">Richiedi una nuova Integrazione di polizza</a>
            </div>
        <? } ?>
    </div>

    <? if(!empty($APPENDICI_TUTTE)){?>
    <div class="row p-4">
        <div class="col-12">    
            <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">Data</th>
                <th scope="col">Numero</th>
                <th scope="col">Stato</th>
                <th scope="col">Teste</th>
                </tr>
            </thead>
            <tbody>
            <?  foreach($APPENDICI_TUTTE as $key => $app)
                {
                        $dtapp = new DateTime($app->DATA_RIFERIMENTO);
                        ?>
                        <tr>
                        <td><?=$dtapp->format('d/m/Y') ?></td>
                        <td><a href="#" onclick="gotoAppendice(<?=$app->PROGRESSIVO ?>)"><?=$app->NUMERO_PREVENTIVO?></a></td>
                        <?  $classe_msg="";
                            $testo_msg="";
                            switch($app->STATO_PREVENTIVO) {
                                case Preventivi_model::APPENDICE_IN_COMPILAZIONE:
                                    $classe_msg="danger";
                                    $testo_msg = "Integrazione da confrmare";
                                break;
                                case Preventivi_model::APPENDICE_INVIATA:
                                    $classe_msg="danger";
                                    $testo_msg = "Integrazione da inviare";
                                break;
                                default:
                                    $classe_msg="info";
                                    $testo_msg = "Integrazione registrata";
                                break;
                            }
                        ?>
                        <td>
                            <a role="button" href="#" onclick="gotoAppendice(<?=$app->PROGRESSIVO?>)" 
                                class="alert alert-<?=$classe_msg?>" >
                                <?=$testo_msg ?>
                            </a>
                        </td>
                        <td><?=count($app->teste)?></td>
                        </tr>
            <?  } ?>
            </tbody>
            </table>
        </div>
    </div>
    <? } ?>
</div>
<form id="form-goto-appendice" method="POST">
    <input type="hidden" name="PROGRESSIVO" id="inputPROGRESSIVOAPPENDICE" >
</form>
<script>
function gotoAppendice( progressivo ){
    var frmName="form-goto-appendice";
    console.log(progressivo);
    console.log($('#'+frmName+' #inputPROGRESSIVOAPPENDICE'));
    $('#'+frmName+' #inputPROGRESSIVOAPPENDICE').val(progressivo);
   
    $('#'+frmName).attr("action","/preventivi/appendice");
    $('#'+frmName).attr("target","");
    $('#'+frmName).submit();
    return false;
}
function gotoRegolazione( progressivo ){
    var frmName="form-goto-appendice";
    console.log(progressivo);
    console.log($('#'+frmName+' #inputPROGRESSIVOAPPENDICE'));
    $('#'+frmName+' #inputPROGRESSIVOAPPENDICE').val(progressivo);
   
    $('#'+frmName).attr("action","/preventivi/regolazione");
    $('#'+frmName).attr("target","");
    $('#'+frmName).submit();
    return false;
}
</script>