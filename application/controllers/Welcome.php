<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	function __construct()
    {	
		parent::__construct();
		if(!$this->controllo_privacy()){
			header('Location: /privacy');
		}
	}

	public function index()
	{
		if(empty($this->utente)){
			$page_config=array();
			$page_config['titolo']="Accesso";
			$page_config['footer_templates']=array("footer_unlogged");
			
			$page_data = array();
			$page_data['msg_auenticazione_errata'] = '';
			if(strpos(strtolower($this->msg_auenticazione_errata),"offline")){
				$page_data['msg_auenticazione_errata'] = 'Servizio momentaneamente Offline,<br>riprova pi&ugrave tardi';
			}
			$this->show("home_page",$page_config,$page_data);
		}
		else{
			$this->home();
		}
	}

	public function cookies()
	{
		$page_config=array();
		$page_config['titolo']="Cookies";
		$page_config['footer_templates']=array("footer_unlogged");
		
		$page_data=array();
		$this->show("info_cookies",$page_config,$page_data);
		
	}
	public function home()
	{
		$page_config=array();
		$page_config['titolo']="Home";
		$page_config['footer_templates']=array("footer_logged");
		$page_data = array();


		$this->load->model("Anagrafica_model");
		$AMOD = new Anagrafica_model($this->assi);
		$p=array();
		//$p['CONTESTO']=Anagrafica_model::CONTATTI_ULTILI_CONTESTO_BENVENUTO;
		$page_data['contatti_utili']= $AMOD->get_contatti_utili($p);

		if($this->is_funzione_abilitata("scadenze")){
			$this->load->model("Titoli_model");
			$TMOD = new Titoli_model($this->assi);
			$p=array();
			$page_data['scadenze_scadute']= $TMOD->get_scadenze($p);
		}
		$this->show("user_main",$page_config,$page_data);
		
	}

}
