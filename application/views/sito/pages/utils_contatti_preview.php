<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-account",
        "fa_icon"=>"lightbulb",
        "titolo"=>'<a  class="btn-loading" href="/">Home</a> > Comunicazioni Suggerimenti</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
    <?foreach($page_data['tips'] as $tip){?>
    <div class="row pt-3">
        <div class="col">
            <div ><h3><?=$tip['title']?></h3></div>
            <div ><i>Aspetto:</i></div>
            <div class="m-2"><?=$tip['html']?></div>
            <div ><i>Codice Sorgente:</i></div>
            <div class="border border-3 p-1">
                <pre class="small"><?=htmlentities($tip['html'])?>
                </pre>
            </div>
            <div ><hr></div>
        </div>
    </div>    
    <?}?>
</div>
