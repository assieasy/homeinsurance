<?php

require_once "Base_rest_model.php";

class Preventivi_model extends Base_rest_model
{
	const CODIFICA_PREVENTIVI="SCUOLAFUTURA";
	const CODIFICA_PREVENTIVI_APPENDICI ="SCUOLAFUTURAAPPENDICE";
	const PREVENTIVO_IN_COMPILAZIONE="1"; //ATTESA DATI REGOLAZIONE
	const PREVENTIVO_INVIATO="11"; // POLIZZA DA EMETTERE
	const PREVENTIVO_STORICO="2"; // POLIZZA EMESSA

	const APPENDICE_IN_COMPILAZIONE="15"; //ATTESA DATI APPENDICE
	const APPENDICE_INVIATA="12"; //APPENDICE DA EMETTERE
	const APPENDICE_CANCELLATA_DA_UTENTE ="14";
	const APPENDICE_STORICA="13"; // APPENDICE EMESSA

	const RAMO_GLOBALE_SCUOLA="1"; 
	
	

	
	public function costanti(){
		$c = array();
		$c['PREVENTIVO_IN_COMPILAZIONE']= self::PREVENTIVO_IN_COMPILAZIONE;
		$c['PREVENTIVO_INVIATO']= self::PREVENTIVO_INVIATO;
		$c['CODIFICA_PREVENTIVI']= self::CODIFICA_PREVENTIVI;

		$c['APPENDICE_IN_COMPILAZIONE']= self::APPENDICE_IN_COMPILAZIONE;
		$c['APPENDICE_INVIATA']= self::APPENDICE_INVIATA;
		$c['CODIFICA_PREVENTIVI_APPENDICI']= self::CODIFICA_PREVENTIVI_APPENDICI;
		
		return $c;
	}
	protected function pr_get_regolazioni($params = array()){
        $p = array();
		foreach($params as $key=>$value){
			$p[$key]= $value;
		}
		$p['CODIFICA']=self::CODIFICA_PREVENTIVI;
		$ret = $this->CH->get_regolazione($p);
	    return $ret;
	}
	
    public function get_regolazione($progressivo_regolazione = 0){
		$p = array();
		$p['PROGRESSIVO']=$progressivo_regolazione;
		if($progressivo_regolazione==0){
			//filtro solo tra le aperte
			$StatiPreventivoFiltro = self::PREVENTIVO_IN_COMPILAZIONE;
			$StatiPreventivoFiltro .= ",".self::PREVENTIVO_INVIATO;
			$p['STATO_PREVENTIVO']=$StatiPreventivoFiltro;
		}
		$ret = $this->pr_get_regolazioni($p);
        $regolazione=  $this->CH->extract_first_data($ret);
        return $regolazione;
	}
	
	public function get_regolazioni_aperte($params = array()){
        $p = array();
		foreach($params as $key=>$value){
			$p[$key]= $value;
		}
		$StatiPreventivoFiltro = self::PREVENTIVO_IN_COMPILAZIONE;
		$StatiPreventivoFiltro .= ",".self::PREVENTIVO_INVIATO;
		$p['STATO_PREVENTIVO']=$StatiPreventivoFiltro;

		$ret = $this->pr_get_regolazioni($p);
        $regolazioni = $this->CH->extract_data($ret);		
        return $regolazioni;
	}
	
	public function get_regolazioni_tutte($params = array()){
		$p = array();
		foreach($params as $key=>$value){
			$p[$key]= $value;
		}
		$StatiPreventivoFiltro = self::PREVENTIVO_IN_COMPILAZIONE;
		$StatiPreventivoFiltro .= ",".self::PREVENTIVO_INVIATO;
		$StatiPreventivoFiltro .= ",".self::PREVENTIVO_STORICO;
		$p['STATO_PREVENTIVO']=$StatiPreventivoFiltro;

		$ret = $this->pr_get_regolazioni($p);
		$regolazioni = $this->CH->extract_data($ret);	
        return $regolazioni;
	}
    public function write_testa($testa){
		$ret = $this->CH->put_preventivo_testa($testa);
		return $ret;
	}
    
    public function cambia_stato_preventivo($regolazione){
		$ret = $this->CH->cambia_stato_preventivo($regolazione);
		return $ret;
    }
    public function stampa_regolazione($regolazione){
		$ret = $this->CH->stampa_regolazione($regolazione);
		return $ret;
    }
    public function put_regolazione($regolazione){
		$ret = $this->CH->put_preventivo($regolazione);
		return $ret;
	}

	//APPENDICE
	public function put_appendice($regolazione){
		$ret = $this->CH->put_preventivo($regolazione);
		return $ret;
	}
	public function get_appendice($progressivo_appendice=0){
       	$appendici = $this->get_appendici();
		$ret_appendice = null;
		foreach($appendici as $app){
			if($progressivo_appendice==0 || $app->PROGRESSIVO == $progressivo_appendice){
				$ret_appendice = $app;
				break;
			}
		}
        return $ret_appendice;
	}
	//APPENDICE
	public function get_appendici_in_sospeso($params = array()){
		$p = array();
		foreach($params as $key=>$value){
			$p[$key]= $value;
		}
		$StatiPreventivoFiltro = self::APPENDICE_IN_COMPILAZIONE;
		$StatiPreventivoFiltro .= ",".self::APPENDICE_INVIATA;
		$p['STATO_PREVENTIVO']=$StatiPreventivoFiltro;
		
        return $this->get_appendici($p);
	}
	public function get_appendici_storiche($params = array()){
		$p = array();
		foreach($params as $key=>$value){
			$p[$key]= $value;
		}
		$StatiPreventivoFiltro = self::APPENDICE_STORICA;
		$p['STATO_PREVENTIVO']=$StatiPreventivoFiltro;
		
        return $this->get_appendici($p);
	}
	public function get_appendici_tutte($params = array()){
		$p = array();
		foreach($params as $key=>$value){
			$p[$key]= $value;
		}
		$StatiPreventivoFiltro = self::APPENDICE_IN_COMPILAZIONE;
		$StatiPreventivoFiltro .= ",".self::APPENDICE_INVIATA;
		$StatiPreventivoFiltro .= ",".self::APPENDICE_STORICA;
		$p['STATO_PREVENTIVO']=$StatiPreventivoFiltro;
		
        return $this->get_appendici($p);
	}

	protected function get_appendici($params = array()){
		$p = array();
		foreach($params as $key=>$value){
			$p[$key]= $value;
		}
		$p['CODIFICA']=self::CODIFICA_PREVENTIVI_APPENDICI;
		$ret = $this->CH->get_regolazione($p);
		$appendici = $this->CH->extract_data($ret);		
        return $appendici;
	}

	public function get_appendice_vuota($params = array())
    {
		$pvuoto = new stdClass();
		$pvuoto->PROGRESSIVO = 0;
		
		$pvuoto->CODIFICA =self::CODIFICA_PREVENTIVI_APPENDICI;
		$pvuoto->STATO_PREVENTIVO =self::APPENDICE_IN_COMPILAZIONE;
		$pvuoto->NUMERO_PREVENTIVO = 0;
		$pvuoto->DATA_RIFERIMENTO = date("Y-m-d");
		$pvuoto->valid = false;
		$pvuoto->teste=array();
		$pvuoto->messages=array();

        return $pvuoto;
	}
	
}
