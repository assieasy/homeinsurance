    <? 
    $all_config_obj = $sito['all_config_obj'];
    $hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
    $vedi_premio_auto_rin = isset($hiPar->vedi_premio_auto_rin)?$hiPar->vedi_premio_auto_rin:0;  
    $vedi_premio_auto_rin = ($vedi_premio_auto_rin==1);

                                    
    
    $TIT = isset($page_data['scadenza'])?$page_data['scadenza']:array();
    $nascondi_premio = FALSE;
    if( $TIT->STATO_TITOLO==1 
            && $TIT->IS_SCADENZA_ANNUALITA==1 
            && $TIT->TIPO_RAMO=="A" 
            && !$vedi_premio_auto_rin){
        $nascondi_premio = TRUE;
    }
    
    $POLIZZA = isset($TIT->polizza)?$TIT->polizza:array();
    $dtScadenza = new DateTime($TIT->DATA_EFFETTO);
    $dtEff = new DateTime($TIT->DATA_EFFETTO);

    $sLinkPolizza = '<a class="btn-loading" href="/polizze/polizza/'.$POLIZZA->ID_POLIZZA.'"><u>'.$POLIZZA->NUMERO_POLIZZA.'</u></a>';
    $show_contraente = false;
    
    ?>
<div class="container-fluid maincontainer pb-4">
    <?
    if($TIT->STATO_TITOLO==1){
       $link_testata =  '<a class="btn-loading" href="/titoli/scadenze">Scadenze</a> > Scadenza del '.$dtEff->format('d/m/Y').' > Polizza '.$sLinkPolizza;
    }
    else{
        $link_testata =  '<a class="btn-loading" href="/titoli/pagamenti">Scadenze</a> > Scadenza del '.$dtEff->format('d/m/Y').' > Polizza '.$sLinkPolizza;
    }

    $config=array(
        "border_class"=>"border-scadenze",
        "fa_icon"=>"calendar-check",
        "titolo"=>$link_testata 
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
    <div class="row pt-3">
        <div class="col-md-6 py-2">
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD SCADENZA -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-id-card mr-1" aria-hidden="true"></i> Scadenza</h5>
                        </div>
                        <div class="card-body">
                            <dl class="">
                                <? $valori=array();
                                $valori[]=array("label"=>"Compagnia","valore"=>$POLIZZA->DESC_COMPAGNIA);

                                $valori[]=array("label"=>"Numero Polizza","valore"=>$sLinkPolizza);
                                if($POLIZZA->TARGA!=""){
                                    $valori[]=array("label"=>"Targa","valore"=>$POLIZZA->TARGA);
                                }
                                $valori[]=array("label"=>"Ramo","valore"=>$POLIZZA->DESC_RAMO);
                                $valori[]=array("label"=>"Prodotto","valore"=>$POLIZZA->DESC_PRODOTTO);
                                $valori[]=array("label"=>"Frazionamento","valore"=>$POLIZZA->FRAZIONAMENTO);

                                $valori[]=array("label"=>"Data Scadenza","valore"=>$dtEff->format('d/m/Y'));
                                
                                if(in_array("scadenze.premio",$funzioni_abilitate) || $TIT->STATO_TITOLO>2 ) {
                                    //SE ARRETRATO FACCIO VEDERE IMPORTO SOLO SE "scadenze.premio" E' ABILITATA
                                    //$sTMP = "&euro; ".number_format($TIT->PREMIO_LORDO_DA_DIREZIONE,2,",",".");
                                    if($nascondi_premio){
                                        $sTMP = $TIT->DESC_SCADENZA_ANNUALITA;
                                        $valori[]=array("label"=>"Tipo scadenza","valore"=>$sTMP);
                                    }
                                    else{
                                        $sTMP = "&euro; ".number_format($TIT->PREMIO_TOTALE,2,",",".");
                                        $valori[]=array("label"=>"Importo","valore"=>$sTMP);
                                    }  
                                }
                               /* $valori[]=array("label"=>"Data Decorrenza","valore"=>$dtEff->format('d/m/Y'));
                                $valori[]=array("label"=>"Data Scadenza","valore"=>$dtScadenza->format('d/m/Y'));
                                $valori[]=array("label"=>"Stato","valore"=>$POLIZZA->DESC_STATO_POLIZZA);
                                */
                                foreach($valori as $valore){
                                    ?>
                                    <dt ><?=$valore["label"]?></dt>
                                    <dd ><?=$valore["valore"]?></dd>
                                    <?
                                }
                                ?>
                                
                            </dl> 
                        </div>
                    </div>
                    <!-- CARD SCADENZA FINE-->
                </div>
            </div>
        </div>

        <div class="col-md-6 py-2">
            <?if($TIT->STATO_TITOLO==1 && in_array("scadenze.paga",$funzioni_abilitate) && !$nascondi_premio) { ?>

                <? $BONIFICO = isset($TIT->canale_bonifico)?$TIT->canale_bonifico:"";
                if($BONIFICO!=""){?>
                    <div class="row py-2">
                        <div class="col-12">
                            <!-- CARD BONIFICO -->

                            <div class="card card-dettaglio " >
                                <div class="card-header">
                                    <h5><i class="fa fa-university mr-2" aria-hidden="true"></i>Vuoi pagare con Bonifico?</h5>
                                </div>
                                <div class="card-body justify-content-center text-center">
                                   <!-- <dl>
                                    <?  $iban = "";
                                        foreach ($BONIFICO->parametri as $key => $p) {
                                            if($p->CHIAVE_PARAMETRO=="IBAN"){
                                                $iban = $p->VALORE;
                                            } 
                                        }
                                    ?>
                                        <dt >IBAN</dt>
                                        <dd ><?=$iban?></dd>
                                    </dl>
                                    -->
                                    <a href="/bonifici/bonifico/<?=$TIT->ID_TITOLO?>" 
                                        loadingSecondi="40" 
                                        class="d-flex justify-content-center btn btn-primary btn-loading">Visualizza le Coordinate Bancarie</a>
                                </div>
                                
                            </div>
                            <!-- CARD BONIFICO FINE-->
                        </div>
                    </div>
                <?}?>

                <?
                $NEXI = isset($TIT->canale_nexi)?$TIT->canale_nexi:"";
                if($NEXI!=""){?>
                    <div class="row py-2">
                        <div class="col-12">
                            <!-- CARD BONIFICO -->

                            <div class="card card-dettaglio " >
                                <div class="card-header">
                                    <h5><i class="fa fa-university mr-2" aria-hidden="true"></i>Vuoi pagare con Nexi?</h5>
                                </div>
                                <div class="card-body justify-content-center text-center">
                                    <a href="/titoli/nexi/<?=$TIT->ID_TITOLO?>" 
                                        loadingSecondi="40" 
                                        class="d-flex justify-content-center btn btn-primary btn-loading">
                                        <img src="https://ecommerce.nexi.it/sites/default/files/Nexi_logo_3.png"/></a>
                                </div>
                                
                            </div>
                            <!-- CARD BONIFICO FINE-->
                        </div>
                    </div>
                <?}?>
            <?}?>
            <?if(!empty($TIT->documenti)) {?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD DOCUMENTI -->
                    <div class="card card-dettaglio " >
                        <div class="card-header">
                        <h5><i class="fa fa-file-alt mr-1" aria-hidden="true"></i> Archivio Documenti</h5>
                        </div>
                        <div class="card-body">
                            <ul>
                                <?  foreach($TIT->documenti as $doc) {?>
                                        <li>
                                            <?=$doc->DESC_TIPO_DOCUMENTO?>:
                                            <a target="_blank" href="<?=$sito['ASSI_URL_PUBBLICO']."/".$doc->URL_DOCUMENTO."/download"?>" >
                                            <i class="fa fa-<?=$this->load->view("sito/common/icona_file_fa",array("filename"=>$doc->NOME_FILE),TRUE);?> fa-2x"></i>
                                            <?=($doc->TITOLO!=""?$doc->TITOLO:$doc->NOME_FILE)?>
                                            </a>
                                        </li>
                                <?  }?>
                            </ul> 
                        </div>
                    </div>
                    <!-- CARD DOCUMENTI FINE-->
                </div>
            </div>
            <?}?>
            <?if(!empty($TIT->sottotitoli)){?>
            <div class="row py-2">
                <div class="col-12">
                    <!-- CARD SOTTOTITOLI -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-book-open mr-2" aria-hidden="true"></i>Dettaglio Garanzie</h5>
                        </div>
                        <div class="card-body">
                            <ul class="">
                                <?
                                    foreach($TIT->sottotitoli as $stit){
                                    ?>
                                    <li ><?=$stit->DES_GARANZIA?></li>
                                    <?
                                }
                                ?>
                            </ul> 
                        </div>
                    </div>
                    <!-- CARD SOTTOTITOLI FINE-->
                </div>
            </div>
            <?}?>
        </div>
    </div>    
</div>