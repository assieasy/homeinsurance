<?php

require_once "Base_rest_model.php";

class Documenti_model extends Base_rest_model
{
    public function get_documenti($params=array()){
        $ret = $this->CH->get_documenti($params);
        $elenco = $this->CH->extract_data($ret);
        return $elenco;
    }
   
}
