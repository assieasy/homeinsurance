<? 
    $progressivo_messaggio = isset($page_data['PROGRESSIVO_MESSAGGIO'])?$page_data['PROGRESSIVO_MESSAGGIO']:0;
    $errori = isset($page_data['errori'])?$page_data['errori']:array();
    
    $POL = isset($page_data['DATI_POLIZZA'])?$page_data['DATI_POLIZZA']:array();
    $SIN = isset($page_data['DATI_SINISTRO'])?$page_data['DATI_SINISTRO']:array();
    $TIT = isset($page_data['DATI_TITOLO'])?$page_data['DATI_TITOLO']:array();

    $oggetto_ricavato = ""; 
    $link_menu = ""; 
    $id_polizza = 0; 
    $id_titolo = 0; 
    $id_sinistro = 0; 
    
    if(!empty($SIN)){
        $id_polizza = $SIN->ID_POLIZZA; 
        $id_sinistro = $SIN->ID_SINISTRO;
        if($oggetto_ricavato==""){
            $oggetto_ricavato = $SIN->NOMINATIVO." - Sinistro ".$SIN->NUMERO_SINISTRO." su Polizza ".$SIN->NUMERO_POLIZZA;
        }
        if($link_menu==""){
            $sLinkPolizza = '<a href="#" class="btn-loading" onclick="gotoPolizza('.$SIN->ID_POLIZZA.')"><u>'.$SIN->NUMERO_POLIZZA.'</u></a>';
            $sLinkSinistro = '<a href="#" class="btn-loading" onclick="gotoSinistro('.$SIN->ID_SINISTRO.')"><u>'.$SIN->NUMERO_SINISTRO.'</u></a>';
            $link_menu = " > Sinistro ".$sLinkSinistro." > Polizza ".$sLinkPolizza ;
        }
    }
    if(!empty($TIT)){
        $id_polizza = $TIT->ID_POLIZZA; 
        $id_titolo = $TIT->ID_SINISTRO;
        $dtEff = new DateTime($TIT->DATA_EFFETTO);
        if($oggetto_ricavato==""){
            $oggetto_ricavato = $TIT->NOMINATIVO." - Scadenza del ".$dtEff->format('d/m/Y')." su Polizza ".$TIT->NUMERO_POLIZZA;
        }
        if($link_menu==""){
            $sLinkPolizza = '<a href="#" class="btn-loading" onclick="gotoPolizza('.$TIT->ID_POLIZZA.')"><u>'.$TIT->NUMERO_POLIZZA.'</u></a>';
            $sLinkTitolo = '<a href="#" class="btn-loading" onclick="gotoTitolo('.$TIT->ID_TITOLO.')"><u>'.$dtEff->format('d/m/Y').'</u></a>';
            $link_menu = " > Scadenza del ".$sLinkTitolo." > Polizza ".$sLinkPolizza ;
        }
    }
    if(!empty($POL)){
        $id_polizza = $POL->ID_POLIZZA; 
        if($oggetto_ricavato==""){
            $oggetto_ricavato = $POL->NOMINATIVO." - Polizza ".$POL->NUMERO_POLIZZA;
        }
        if($link_menu==""){
            $sLinkPolizza = '<a href="#" class="btn-loading" onclick="gotoPolizza('.$POL->ID_POLIZZA.')"><u>'.$POL->NUMERO_POLIZZA.'</u></a>';
            $link_menu = " > Polizza ".$sLinkPolizza ;
        }
    }

    if( $oggetto_ricavato ==""){
        $oggetto_ricavato = $utente->NOMINATIVO;
    }
  

    $oggetto = isset($page_data['TITOLO'])?$page_data['TITOLO']:"";
    $messaggio = isset($page_data['MESSAGGIO'])?$page_data['MESSAGGIO']:"";
    $referente = isset($page_data['REFERENTE'])?$page_data['REFERENTE']:"";

?>
<div class="container-fluid maincontainer pb-4">
    <?
    $config=array(
        "border_class"=>"border-account",
        "fa_icon"=>"comment-dots",
        "titolo"=>'<a  class="btn-loading" href="/">Home</a> > Messaggio '.$link_menu.'</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);
    ?>
    <div class="row pt-3">
        <div class="col-md-12 py-2">
            <div class="row py-2">
                <div class="col-12">
                    <? if($progressivo_messaggio == 0){ ?>
                    <!-- CARD MESSAGGIO -->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-pen mr-1" aria-hidden="true"></i>Scrivi un messaggio / Invia Documento</h5>
                        </div>
                        <div class="card-body">
                          
                            <form id="form-messaggio" enctype="multipart/form-data" method="POST">
                                <input type="hidden" name="AZIONE" id="AZIONE" value="">
                                <input type="hidden" name="ID_POLIZZA" id="ID_POLIZZA" value="<?=$id_polizza?>">
                                <input type="hidden" name="ID_TITOLO" id="ID_TITOLO" value="<?=$id_titolo?>">
                                <input type="hidden" name="ID_SINISTRO" id="ID_SINISTRO" value="<?=$id_sinistro?>">
                                <div class="form-group">
                                    <label for="TITOLO" class="form-label form-label-sm">Oggetto</label>
                                    <input type="text" 
                                        class="form-control form-control-sm" 
                                        name="TITOLO" 
                                        id="TITOLO" 
                                        placeholder="oggetto del messaggio ..." 
                                        value="<?=$oggetto?>"
                                        >
                                </div>
                                <div class="form-group">
                                    <label for="MESSAGGIO" class="form-label form-label-sm">Messaggio</label>
                                    <textarea class="form-control" name="MESSAGGIO" id="MESSAGGIO" rows="3"><?=$messaggio?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="REFERENTE" class="form-label form-label-sm">Eventuale Referente</label>
                                    <input type="text" 
                                        class="form-control form-control-sm" 
                                        name="REFERENTE" 
                                        id="REFERENTE" 
                                        placeholder="Nome Referente" 
                                        value="<?=$referente?>"
                                        >
                                </div>
                                <? if(in_array("messaggi.allega",$funzioni_abilitate)){ ?>
                                    <div class="form-group">
                                        <label for="FILE_ALLEGATO" class="form-label form-label-sm">Se pensi sia utile allega un file</label>
                                        <div class="custom-file" id="customFile" lang="it">
                                            <input type="file" 
                                                name="FILE_ALLEGATO" 
                                                class="form-control-file alert alert-info alert-info-custom" 
                                                id="inputFILE_ALLEGATO">
                                        </div>
                                    </div>
                                <?}?>
                            </form>
                        </div>
                        <div class="card-footer">
                            <?
                            if(!empty($errori)){
                                foreach($errori as $ERR){
                            ?>
                                <div class="alert alert-danger" role="alert" ><?=$ERR?></div>
                   
                            <?  }
                            }?>
                            <button type="button" class="btn btn-primary" href="#" onclick="inviaMessaggio('SALVA')" >Invia Messaggio</button>
                        </div>
                    </div>
                    <!-- CARD MESSAGGIO FINE-->
                    <?} else {?>
                    <!-- CARD MSG INVIATO-->
                    <div class="card card-dettaglio" >
                        <div class="card-header">
                            <h5><i class="fa fa-pen mr-1" aria-hidden="true"></i> Messaggio Inviato all'agenzia!</h5>
                        </div>
                        <div class="card-body">
                            
                        </div>
                        <div class="card-footer">
                            <?
                            $strBtn = "";
                            if($strBtn == "" && $id_sinistro>0){
                                $strBtn= '<button type="button" class="btn btn-primary btn-loading" href="#" onclick="gotoSinistro('.$id_sinistro.')">TORNA</button>';
                            }
                            if($strBtn == "" && $id_titolo>0){
                                $strBtn= '<button type="button" class="btn btn-primary btn-loading" href="#" onclick="gotoTitolo('.$id_titolo.')">TORNA</button>';
                            }
                            if($strBtn == "" && $id_polizza>0){
                                $strBtn= '<button type="button" class="btn btn-primary btn-loading" href="#" onclick="gotoPolizza('.$id_polizza.')">TORNA</button>';
                            }
                            if($strBtn == ""){
                                $strBtn= '<button type="button" class="btn btn-primary btn-loading" href="#" onclick="gotoHome()">TORNA</button>';
                            }
                            echo $strBtn;
                            ?>
                            
                        </div>
                    </div>
                    <!-- CARD MSG INVIATO FINE-->
                    <?}?>
                </div>
            </div>
        </div>
    </div>  
    <?
    $ultimi_messaggi_inviati = isset($page_data['ultimi_messaggi_inviati'])?$page_data['ultimi_messaggi_inviati']:array();
    if(!empty($ultimi_messaggi_inviati)){?>
        <div class="row pt-3">
        <div class="col-12 card card-dettaglio" >
            <div class="card-header d-flex justify-content-center">
                Ultimi messaggi inviati...
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-hover small">
                    <thead>
                        <tr>
                        <th scope="col">Data</th>
                        <th scope="col">Oggetto</th>
                        <th scope="col">Testo</th>
                        <th scope="col" class="d-none d-md-block">Referente</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?  foreach($ultimi_messaggi_inviati as $msg) {
                            $dt = new DateTime($msg->DATA_RICEZIONE);
                            ?>
                            <tr>
                            <td><?=$dt->format('d/m/Y H:i')?></td>
                            <td><?=$msg->OGGETTO?></td>
                            <td><?=$msg->MESSAGGIO?></td>
                            <td class="d-none d-md-block"><?=$msg->REFERENTE?></td>

                            </tr>
                            
                    <?  }?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    <?}?>  
</div>


<form id="form-goto" method="POST">
    <input type="hidden" name="ID_POLIZZA" id="inputID_POLIZZA" >
    <input type="hidden" name="ID_TITOLO" id="inputID_TITOLO" >
    <input type="hidden" name="ID_SINISTRO" id="inputID_SINISTRO" >
    <input type="hidden" name="TIPO_ELENCO" id="inputTIPO_ELENCO" >
</form>

<script>
/*************** funzioni init ***********/
pageinitfunctions = pageinitfunctions?pageinitfunctions:[];
/*************** funzioni init ***********/

function inviaMessaggio( azione, ancora){
    var frmName="form-messaggio";
    switch(azione){
        default:
            if(!insMessageOk()){
            return false;
            }
            $('#'+frmName).attr("action","/messaggi/invia");
            break;
    }
    if(ancora){
        $('#'+frmName).attr("action","/messaggi/invia#"+ancora);
    }
    $('#AZIONE').val(azione);
    $('#'+frmName).attr("target","");

    start_spinner();
    $('#'+frmName).submit();

    return false;
}

function insMessageOk(){
  var ok = true;
  var campi=['MESSAGGIO','TITOLO'];
  campi.forEach( function(campo) {
    var s = $('#'+campo).val();
    if(!s){
      ok = false;
    }
  });
  
  return ok;
}

function gotoHome( ){
    var frmName="form-goto";
    $('#'+frmName).attr("action","/");
    $('#'+frmName).attr("target","");
    $('#'+frmName).submit();
    return false;
}


function gotoPolizza( id_polizza ){
    var frmName="form-goto";
    $('#'+frmName+' #inputID_POLIZZA').val(id_polizza);
   
    $('#'+frmName).attr("action","/polizze/polizza");
    $('#'+frmName).attr("target","");
    $('#'+frmName).submit();
    return false;
}
function gotoTitolo( id_titolo ){
    var frmName="form-goto";
    $('#'+frmName+' #inputID_TITOLO').val(id_titolo);
   
    $('#'+frmName).attr("action","/titoli/titolo");
    $('#'+frmName).attr("target","");
    $('#'+frmName).submit();
    return false;
}

function gotoSinistro( id_sinistro ){
    var frmName="form-goto";
    $('#'+frmName+' #inputID_SINISTRO').val(id_sinistro);
   
    $('#'+frmName).attr("action","/sinistri/sinistro");
    $('#'+frmName).attr("target","");
    $('#'+frmName).submit();
    return false;
}

function showElenco( tipoVisualizzazione){
    var frmName="form-goto";
    $('#'+frmName+' #inputTIPO_ELENCO').val(tipoVisualizzazione);
   
    $('#'+frmName).attr("action","/titoli/scadenze");
    $('#'+frmName).attr("target","");
    $('#'+frmName).submit();
    return false;
}

</script>

