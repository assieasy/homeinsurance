 <?
    $all_config_obj = $sito['all_config_obj'];
    $hiPar =  isset($all_config_obj->homeins_parametri)?$all_config_obj->homeins_parametri:new stdClass();
    $vedi_premio_auto_rin = isset($hiPar->vedi_premio_auto_rin)?$hiPar->vedi_premio_auto_rin:0;  
    $vedi_premio_auto_rin = ($vedi_premio_auto_rin==1);
?>
<div class="container-fluid maincontainer pb-4">
    <?
    $pagina_origine=isset($page_data['pagina_origine'])?$page_data['pagina_origine']:"";
    $config=array(
        "border_class"=>"border-scadenze",
        "fa_icon"=>"calendar-check",
        "titolo"=>'<a class="btn-loading" href="/">Home</a> > '.($pagina_origine=="pagamenti"?"Storico pagamenti":"Scadenze").'</h5>'
    );
    echo $this->load->view("sito/common/top_div",array("config"=>$config),TRUE);

     ?>
    <div class="row pt-1">
        <div class="ml-auto">
            <div class="btn-group card-dettaglio mr-4" role="toolbar"  aria-label="switch-visione">
                <a role="button" class="btn disabled btn-loading" href="/titoli/<?=$pagina_origine=="pagamenti"?"pagamenti":"scadenze"?>/?TIPO_ELENCO=TABELLA"><i class="fa fa-list" aria-hidden="true"></i></a>
                <a role="button" class="btn btn-loading" href="/titoli/<?=$pagina_origine=="pagamenti"?"pagamenti":"scadenze"?>/?TIPO_ELENCO=CARD"><i class="fa fa-table" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="row p-4">
        <div class="table-responsive col-12">
            <table class="table table-sm table-hover small">
            <thead>
                <tr>
                <th scope="col">Compagnia</th>
                <th scope="col">Numero Polizza</th>
                <th scope="col">Prodotto</th>
                <!-- <th scope="col">Tipo Polizza</th> -->
                <th scope="col">Effetto</th>
                <th scope="col">Stato</th>
                <th scope="col">Premio</th>
                </tr>
            </thead>
            <tbody>
            <?  foreach($page_data['titoli'] as $key => $TIT) {
                    $dtEff = new DateTime($TIT->DATA_EFFETTO);
                    ?>
                    <tr>
                    <td><?=$TIT->DESC_COMPAGNIA?></td>
                    <td><a class="btn-loading" href="/polizze/polizza/<?=$TIT->ID_POLIZZA?>" nowrap><i class="fa fa-id-card mr-1" aria-hidden="true"></i><?=$TIT->NUMERO_POLIZZA?></a></td>
                    <td><?=$TIT->DESC_PRODOTTO?></td>
                    <!-- <td><?=$TIT->COD_RAMO?></td> -->
                    <td><a class="btn-loading" href="/titoli/titolo/<?=$TIT->ID_TITOLO?>" nowrap><i class="fa fa-calendar-check mr-1" aria-hidden="true"></i><u><?=$dtEff->format('d/m/Y')?></u></a></td>
                    <td><?
                    $sTMP = $TIT->DESC_STATO_TITOLO;
                    if($TIT->STATO_TITOLO==10){
                        $sTMP = "Incassato da contabilizzare";
                    }
                    echo $sTMP?></td>
                    <?  $importo = "";
                        if(in_array("scadenze.premio",$funzioni_abilitate) || $TIT->STATO_TITOLO>2 ) {
                                //SE ARRETRATO FACCIO VEDERE IMPORTO SOLO SE "scadenze.premio" E' ABILITATA
                            //$importo = "&euro; ".number_format($TIT->PREMIO_LORDO_DA_DIREZIONE,2,",",".");
                            if($TIT->IS_SCADENZA_ANNUALITA==1 && $TIT->TIPO_RAMO=="A" && !$vedi_premio_auto_rin){
                                $importo = $TIT->DESC_SCADENZA_ANNUALITA;
                            }
                            else{
                                $importo = "&euro; ".number_format($TIT->PREMIO_TOTALE,2,",",".");
                            
                            }
                        } 
                    ?>
                     <td nowrap><?=$importo?></td>
                    </tr>
                    
            <?  }?>
            </tbody>
            </table>
        </div>
    </div>
</div>
