<?php

class Assi_rest_model extends CI_Model
{
    protected $token="";
    protected $chiave_hi="ASSIHI";
    protected $rest_url="";
    protected $out_msgs = array();
    
    protected $assieasy_url = "";
    protected $rets_url = "";
    protected $rest_params = array();
    protected $last_response = "";
    protected $debug_call = FALSE;
    
    function __construct()
    {
        parent::__construct();
        
    }

    function get_last_response(){
        return $this->last_response;
    }

    function finisce_con($stringa, $ricerca) {
        return substr_compare($stringa, $ricerca, -strlen($ricerca)) === 0;
    }
    public function set_assi_url($url){
        if($url!=""){
            if(!$this->finisce_con($url,"/")){
                $url.="/";
            }
        }
        $this->assieasy_url = $url;
        $this->rets_url = $url."clienti/";
        
    }
    
    public function debug(){
        $this->debug_call = TRUE;
    }
    public function set_token($token){
        $this->token = $token;
    }
    public function last_log(){
        return $this->out_msgs;
    }
    protected function normalizza_dir($d){
        $s = str_replace('\\', '/', $d);
        $s = rtrim($s, '/').'/';
        return $s;
    }

    protected function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {

        if ( is_object( $arrays ) ) {
            $arrays = get_object_vars( $arrays );
        }
    
        foreach ( $arrays AS $key => $value ) {
            $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
            if ( is_array( $value ) OR is_object( $value )  ) {
                $this->http_build_query_for_curl( $value, $new, $k );
            } else {
                $new[$k] = $value;
            }
        }
    }
    public function CallAPI($method, $base_url, $azione, $data = false)
    {
        $url = $base_url.$azione;

        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if (!$data){
                    $data=array();
                }
                if(!isset($data['url_origin'])){
                    $data['url_origin']=base_url();
                }
                if ($data)
                    $this->http_build_query_for_curl($data,$post_data);
                
                
                if($post_data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            case "JSON":
                curl_setopt($curl, CURLOPT_POST,           1 );
                curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($data) ); 
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
 
        $headers = array();
        if($this->chiave_hi!=""){
            $headers[] = "chiave-hi:  $this->chiave_hi";
        }
        if($this->token!=""){
            $headers[] = "token:  $this->token";
        }
        
        if(!empty($headers)){

            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        //curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
       // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
       // curl_setopt($curl, CURLOPT_UNRESTRICTED_AUTH, true);
       // curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        //IGNORA ERRORI DI CERTIFICATO
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
       
        $result = curl_exec($curl);
        if($this->debug_call){
          // echo $url; 
           //echo $result;
           $this->log_me("url");
           $this->log_me($url);
           $this->log_me("headers");
           $this->log_me($headers);
           $this->log_me("params");
           $this->log_me($data);
           $this->log_me("result");
           $this->log_me($result);
           $this->debug_call = FALSE;
        }
        if(curl_errno($curl)){
            $ret = new stdClass();
            $ret->success = FALSE;
            $ret->error_message = curl_error($curl);
            $ret->message = $ret->error_message;
            $ret->response = $result;
            //throw new Exception(json_encode($ret));
            return $ret;// $this->send_error('Request Error:' . curl_error($curl));
        }
        curl_close($curl);
       
        return $this->elaboraResponse($result);
    }
    
    protected function elaboraResponse($resp){
        $this->last_response = $resp;
        
        $objResp =  json_decode( $this->last_response);
        

        return $objResp;
    }

    protected function log_me( $s ){
        if($s!=""){
             $this->out_msgs['logs'][]=$s;
        }
    }
    public function extract_data( $rest_results ){
        $ret = array();
        if(!empty($rest_results)){
            $ret = isset($rest_results->data)?$rest_results->data:array();
        }
        return $ret;
    }
    public function extract_first_data( $rest_results ){
        $ret = array();
        $tmp = $this->extract_data($rest_results);
        if(!empty($tmp)){
            foreach($tmp as $record){
                $ret = $record;
            }
        }
        return $ret;
    }

    protected function add_params($params, $prev_params = array(),$overwrite = FALSE){
        $ret_params = $prev_params;
        foreach($params as $key => $value)   
        {
            if($overwrite){
                $ret_params[$key]= $value;
            }
            else{
                if(!isset($ret_params[$key])){
                    $ret_params[$key]= $value;
                }
            }
        }
        return $ret_params;
    }


    
    
}
